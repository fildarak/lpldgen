#!/bin/bash

# this script is used by the CI to build and install lpldgen
apt update
apt -y install meson
apt -y install git
apt -y install curl

curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs > rust.sh
sh ./rust.sh -y
source "$HOME/.cargo/env"

git submodule init
git submodule update
cargo build --release

mkdir bin
# Link or copy the binary where you want it...
cp -sfr target/release/lpldgen ./bin/

meson build -Dprefix=$PWD/pld
cd build && ninja install