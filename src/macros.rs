macro_rules! parser_error {
    ($input:expr, $($arg:tt)+) => {
        eprintln!("\n\x1b[31;1m*** PARSE ERROR ***\x1b[m\n");

        eprint!("\x1b[31m");
        eprint!($($arg)+);
        eprintln!("\x1b[m");

        eprintln!("\n\x1b[90mInput:\x1b[m\n{}", $input);

        eprintln!("\x1b[31mParsing failed.\x1b[m\n");
        std::process::exit(1);
    }
}

macro_rules! structural_error {
    ($($arg:tt)+) => {
        eprintln!("\n\x1b[31;1m*** STRUCTURAL ERROR ***\x1b[m\n");

        eprint!("\x1b[31m");
        eprint!($($arg)+);
        eprintln!("\x1b[m");

        eprintln!("\x1b[31mGeneration failed.\x1b[m\n");
        std::process::exit(1);
    }
}

macro_rules! config_error {
    ($($arg:tt)+) => {
        eprintln!("\n\x1b[31;1m*** CONFIGURATION ERROR ***\x1b[m\n");

        eprint!("\x1b[31m");
        eprint!($($arg)+);
        eprintln!("\x1b[m");

        std::process::exit(1);
    }
}

macro_rules! nom_failure_msg {
    ($input:expr, $msg:expr) => {
        nom::Err::Failure(crate::errors::SpecParseError::new($input, $msg))
    };
}
