use crate::errors::Error;
use std::cmp::Ordering;
use std::convert::TryFrom;
use std::fmt;
use std::fmt::{Display, Formatter};

/// CSP port newtype, used in MsgTag
#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Clone, Copy, Hash)]
pub struct CspPort(pub u8);

impl Display for CspPort {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.0)
    }
}

/// Message address, used to recognize a payload variant by csp_id
#[derive(Debug, PartialEq, Eq, Clone, Copy, Hash)]
pub enum MsgTag {
    /// Message with a known source address and port
    From(u8, CspPort),
    /// Message with a known destination address and port
    To(u8, CspPort),
    /// Template messages are only used for reuse or embedding in others
    Template,
}

impl MsgTag {
    fn get_addr(self) -> u8 {
        match self {
            MsgTag::From(a, _) | MsgTag::To(a, _) => a,
            MsgTag::Template => panic!("Trying to get address from a template message"),
        }
    }

    fn get_port(self) -> CspPort {
        match self {
            MsgTag::From(_, p) | MsgTag::To(_, p) => p,
            MsgTag::Template => panic!("Trying to get port from a template message"),
        }
    }

    fn is_from(self) -> bool {
        match self {
            MsgTag::From(_, _) => true,
            MsgTag::To(_, _) => false,
            MsgTag::Template => false,
        }
    }

    pub fn is_template(self) -> bool {
        match self {
            MsgTag::From(_, _) => false,
            MsgTag::To(_, _) => false,
            MsgTag::Template => true,
        }
    }
}

impl std::cmp::Ord for MsgTag {
    fn cmp(&self, other: &Self) -> Ordering {
        self.get_addr()
            .cmp(&other.get_addr())
            .then_with(|| self.get_port().cmp(&other.get_port()))
            .then_with(|| self.is_from().cmp(&other.is_from()))
    }
}

impl PartialOrd for MsgTag {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl Default for MsgTag {
    fn default() -> Self {
        MsgTag::Template
    }
}

impl Display for MsgTag {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match self {
            MsgTag::From(addr, CspPort(port)) => {
                write!(f, "from CSP address {}, port {}", addr, port)
            }
            MsgTag::To(addr, CspPort(port)) => write!(f, "to CSP address {}, port {}", addr, port),
            MsgTag::Template => f.write_str("payload template"),
        }
    }
}

/// Convert a MsgTag to a hexa number 0x<FromAddr><ToAddr><Port>, unused address is set to 0xFF
impl From<MsgTag> for u32 {
    fn from(tag: MsgTag) -> u32 {
        match tag {
            MsgTag::From(addr, CspPort(port)) => (addr as u32) << 16 | 0xFF00 | (port as u32),
            MsgTag::To(addr, CspPort(port)) => 0xFF_0000 | (addr as u32) << 8 | (port as u32),
            MsgTag::Template => panic!("cannot convert template message tag to u32"),
        }
    }
}

/// Convert a hexa number to a MsgTag struct
impl TryFrom<u32> for MsgTag {
    type Error = crate::errors::Error<'static>;

    fn try_from(num: u32) -> Result<MsgTag, Self::Error> {
        if num & 0xFF_0000 == 0xFF_0000 {
            let to = ((num >> 8) & 0xFF) as u8;
            if to == 0xFF {
                Err(Error::Message(
                    format!("Could not parse tag from {:#04x}", num).into(),
                ))
            } else {
                Ok(MsgTag::To(to, CspPort((num & 0xFF) as u8)))
            }
        } else if num & 0x00_FF00 == 0x00_FF00 {
            Ok(MsgTag::From(
                ((num >> 16) & 0xFF) as u8,
                CspPort((num & 0xFF) as u8),
            ))
        } else {
            Err(Error::Message(
                format!("Could not parse tag from {:#04x}", num).into(),
            ))
        }
    }
}

#[cfg(test)]
mod tests {
    use super::{CspPort, MsgTag};
    use std::convert::TryFrom;

    #[test]
    fn test_tag_to_u32() {
        assert_eq!(
            MsgTag::From(0x05, CspPort(0x3F)),
            MsgTag::try_from(0x05_FF3F).unwrap()
        );
        assert_eq!(
            MsgTag::From(0x7F, CspPort(0xFF)),
            MsgTag::try_from(0x7F_FFFF).unwrap()
        );
        assert_eq!(
            MsgTag::From(0x00, CspPort(0xFF)),
            MsgTag::try_from(0x00_FFFF).unwrap()
        );
        assert_eq!(
            MsgTag::To(0x7F, CspPort(0x00)),
            MsgTag::try_from(0xFF_7F00).unwrap()
        );
        assert_eq!(
            MsgTag::To(0x05, CspPort(0xFF)),
            MsgTag::try_from(0xFF_05FF).unwrap()
        );
    }
}
