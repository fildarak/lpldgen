use crate::spec::field::{FieldMeta, Type, TypeValue};
use crate::spec::Endian;

/// Simple field that can be inside a bitfield
#[derive(Debug, PartialEq, Clone)]
pub enum BitfieldScalar {
    /// Simple variable
    Simple {
        /// Field name, used in the output format
        name: String,
        /// Field data type
        dtype: Type,
        /// Metadata
        meta: FieldMeta,
    },
    /// Constant
    Const {
        /// Field value
        value: TypeValue,
        /// Metadata (this will be most likely endian for const)
        meta: FieldMeta,
    },
    /// Padding (zero constant)
    Pad,
}

impl BitfieldScalar {
    pub fn get_endian(&self) -> Option<Endian> {
        match self {
            BitfieldScalar::Simple { meta, .. } | BitfieldScalar::Const { meta, .. } => meta.endian,
            BitfieldScalar::Pad => None,
        }
    }

    pub fn get_name(&self) -> Option<&String> {
        match self {
            BitfieldScalar::Simple { name, .. } => Some(name),
            BitfieldScalar::Const { .. } | BitfieldScalar::Pad => None,
        }
    }

    #[must_use]
    pub fn with_name(self, new_name: String) -> Self {
        match self {
            BitfieldScalar::Simple {
                name: _,
                dtype,
                meta,
            } => BitfieldScalar::Simple {
                name: new_name,
                dtype,
                meta,
            },
            BitfieldScalar::Const { .. } => self,
            BitfieldScalar::Pad => self,
        }
    }
}

/// Field that can be inside bitfield (including array of fixed length)
#[derive(Debug, PartialEq, Clone)]
pub enum BitfieldEntryKind {
    /// Simple multi-bit field inside a bitfield element
    Scalar(BitfieldScalar),
    /// Array evenly dividing the space taken by itself among sub-elements, cannot be nested
    ScalarArray {
        /// Repeat
        count: usize,
        /// Array member field (with a name)
        field: BitfieldScalar,
    },
    /// Bitfield struct
    Struct {
        /// Name
        name: String,
        /// Members
        fields: Vec<BitfieldEntry>,
        // Struct meta (mainly descr)
        meta: FieldMeta,
    },
    /// Array of bitfield structs
    StructArray {
        /// Name
        name: String,
        /// Repeat
        count: usize,
        /// Members
        fields: Vec<BitfieldEntry>,
        // Struct meta (mainly descr)
        meta: FieldMeta,
    },
}

/// Entry in a bitfield
#[derive(Debug, PartialEq, Clone)]
pub struct BitfieldEntry {
    /// Data field in the entry
    pub field: BitfieldEntryKind,
    /// Bit size of the field
    pub bits: usize,
}

impl BitfieldEntry {
    pub fn has_variables(&self) -> bool {
        match &self.field {
            BitfieldEntryKind::Scalar(BitfieldScalar::Simple { .. }) => true,
            BitfieldEntryKind::ScalarArray {
                field: BitfieldScalar::Simple { .. },
                ..
            } => true,
            BitfieldEntryKind::Struct { fields, .. }
            | BitfieldEntryKind::StructArray { fields, .. } => {
                for f in fields {
                    if f.has_variables() {
                        return true;
                    }
                }
                false
            }
            _ => false,
        }
    }

    pub fn check_for_duplicate_fields(&self, names: &mut Vec<String>) -> Option<String> {
        // Named field starts its own scope for sub-fields;
        // Anonymous field shares scope with this
        match &self.field {
            BitfieldEntryKind::Scalar(field) | BitfieldEntryKind::ScalarArray { field, .. } => {
                match field {
                    BitfieldScalar::Simple { name, .. } => {
                        if names.contains(name) {
                            return Some(name.to_string());
                        } else {
                            names.push(name.clone());
                        }
                    }
                    BitfieldScalar::Const { .. } => {}
                    BitfieldScalar::Pad => {}
                }
            }
            BitfieldEntryKind::Struct { name, fields, .. }
            | BitfieldEntryKind::StructArray { name, fields, .. } => {
                if names.contains(name) {
                    return Some(name.to_string());
                } else {
                    names.push(name.clone());

                    let mut inner_names = vec![];
                    for field in fields {
                        let inner = field.check_for_duplicate_fields(&mut inner_names);
                        if inner.is_some() {
                            return inner;
                        }
                    }
                }
            }
        }

        None
    }
}
