use crate::codegen::bitfield_split::split_bitfield_to_bytes;
use crate::codegen::utils::ShiftLeftRight;
use crate::parser::expr;
use crate::spec::Endian;
mod bitfield;
mod types;

pub use types::{Type, TypeValue};

pub use bitfield::{BitfieldEntry, BitfieldEntryKind, BitfieldScalar};
use std::borrow::Cow;

/// Byte mask for message detection based on well-known content
#[derive(Debug, PartialEq, Eq, Ord, PartialOrd, Clone, Copy, Hash)]
pub struct ByteMask {
    /// byte index
    pub offset: usize,
    /// (field's value) & mask
    pub value: u8,
    /// byte mask
    pub mask: u8,
}

impl ByteMask {
    /// Create a new byte mask
    #[allow(unused)]
    pub fn new(offset: usize, value: u8, mask: u8) -> Self {
        Self {
            offset,
            value,
            mask,
        }
    }

    /// Adjust the offset field
    pub fn add_offset(&mut self, add: isize) {
        self.offset = ((self.offset as isize) + add) as usize;
    }

    /// Check if this mask matches other mask, except for the masked value
    pub fn position_matches(&self, other: &ByteMask) -> bool {
        self.offset == other.offset && self.mask == other.mask
    }

    /// Check if this mask matches other mask
    pub fn matches(&self, other: &ByteMask) -> bool {
        self.offset == other.offset
            && self.mask == other.mask
            && (self.mask & self.value) == (other.mask & other.value)
    }
}

/// Configurable bitfield item alignment
#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub enum BitfieldAlign {
    /// align right, towards LSB:
    /// ccBbbbAa, DdddddCc
    Right,
    /// align left, towards MSB:
    /// AaBbbbCc, ccDddddd
    Left,
}

impl Default for BitfieldAlign {
    fn default() -> Self {
        BitfieldAlign::Left
    }
}

/// Field metadata; extra information associated with a struct field
#[derive(Debug, Default, PartialEq, Clone)]
pub struct FieldMeta {
    /// Human-readable field label
    pub descr: Option<String>,
    /// Unit used to format the value after conversion
    pub unit: Option<String>,
    /// Some constants are exported as named defines for use in parser switches
    pub constant_name: Option<String>,
    /// Endian override
    pub endian: Option<Endian>,
    /// Bitfield alignment (used only inside bitfield)
    pub bitfield_align: Option<BitfieldAlign>,
    /// Formula that produces the encoded value from a raw input value
    /// (e.g. turn float °C to unsigned integer that goes in the payload).
    ///
    /// The raw input is represented as a variable called 'x'
    pub transform: Option<expr::Node>,
    /// Representation to use after converting
    pub repr: Option<Type>,
    /// Convert the value to string when building JSON
    pub json_sprintf: Option<String>,
    /// In spec struct, the "template" keyword sets this to true
    pub is_template: bool,
}

/// information for FieldMeta fields (used when parsing these one by one)
#[derive(Debug, PartialEq)]
pub enum FieldMetaOption {
    Descr(String),
    NamedConst(String),
    Unit(String),
    Repr(Type),
    Endian(Endian),
    BitfieldAlign(BitfieldAlign),
    Transform(expr::Node),
    JsonSprintf(String),
}

impl FieldMeta {
    /// Compose from a list of options
    pub fn from_options(options: &[FieldMetaOption]) -> Self {
        let mut object = Self::default();

        for option in options {
            match option {
                FieldMetaOption::Descr(s) => match object.descr.as_mut() {
                    None => object.descr = Some(s.to_owned()),
                    Some(d) => {
                        *d += "\n";
                        *d += s;
                    }
                },
                FieldMetaOption::NamedConst(s) => object.constant_name = Some(s.to_owned()),
                FieldMetaOption::Unit(s) => object.unit = Some(s.to_owned()),
                FieldMetaOption::Endian(e) => object.endian = Some(*e),
                FieldMetaOption::BitfieldAlign(a) => object.bitfield_align = Some(*a),
                FieldMetaOption::Transform(n) => object.transform = Some(n.clone()),
                FieldMetaOption::Repr(ty) => object.repr = Some(*ty),
                FieldMetaOption::JsonSprintf(fmt) => object.json_sprintf = Some(fmt.to_owned()),
            }
        }

        object
    }

    /// Generate a doxygen field comment.
    /// The comment is complete including the `/** */` markers and a trailing newline.
    pub fn to_field_comment(&self) -> Option<String> {
        let mut lines = vec![];
        if let Some(d) = &self.descr {
            lines.push(Cow::Borrowed(d));
        }
        if let Some(u) = &self.unit {
            lines.push(Cow::Owned(format!("Unit: {}", u)));
        }

        if lines.is_empty() {
            None
        } else {
            let mut buf = "/** ".to_string();

            for (n, line) in lines.iter().enumerate() {
                if n > 0 {
                    buf += "\n * ";
                }
                buf += &line.replace("\n", "\n * ");
            }
            buf += " */\n";
            Some(buf)
        }
    }

    /// Check if any conversions are attached
    pub fn has_conversions(&self) -> bool {
        /* unit is allowed, as it does not affect the data type or value */
        self.repr.is_some() || self.transform.is_some()
    }

    /// Check if this meta applies to a struct
    pub fn applies_to_struct(&self) -> bool {
        !self.has_conversions() && self.endian.is_none() && self.unit.is_none()
    }
}

/// Payload field specification
#[derive(Debug, PartialEq, Clone)]
pub enum Field {
    /// Named field of a primitive type
    Simple {
        /// Field name, used in the output format
        name: String,
        /// Field data type
        dtype: Type,
        /// Metadata
        meta: FieldMeta,
    },
    /// Constant value (e.g. for reserved bytes or bytes identifying a payload variant)
    Const {
        /// Field value
        value: TypeValue,
        /// Metadata (this will be most likely endian for const)
        meta: FieldMeta,
    },
    /// Padding / filler, an unnamed field of arbitrary length
    Pad(usize),
    /// Named bitfield (packed shorter fields)
    Bits {
        /// Field name, used in the output format
        name: Option<String>,
        /// Inner fields accompanied by their bit lengths.
        items: Vec<BitfieldEntry>,
        /// Metadata (endian override option)
        meta: FieldMeta,
    },
    /// Named struct
    Struct {
        /// Struct field name, used in the output format
        name: String,
        /// Fields inside the struct
        fields: Vec<Field>,
        /// Metadata (endian override, descr, etc)
        meta: FieldMeta,
    },
    /// Array of fixed length
    Array {
        /// Number of items
        count: usize,
        /// Field repeated in the array
        field: Box<Field>,
    },
    /// Tail-position array of dynamic size, determined from the payload length.
    /// This array is simply the remainder of the payload after all previous fields were parsed.
    /// It's illegal to follow this field by anything else in the struct, and it must be used in
    /// the top level of a payload definition, or at the end of the last struct or union.
    TailArray {
        /// Field repeated in the array
        field: Box<Field>,
    },
    /// Render a template in its place
    Use { template: String, meta: FieldMeta },
}

/// Get a constant matching mask from a vec of fields
pub fn fields_vec_to_const_mask(fields: &[Field], pld_endian: Endian) -> Option<Vec<ByteMask>> {
    let masks = fields
        .iter()
        // we can't check constants after a growable field (they don't have a fixed index)
        .take_while(|f| !f.is_growable())
        .map(|f| (f.byte_length(), f.get_constant_mask(pld_endian)))
        .fold((0usize, Vec::new()), |(offset, mut buf), (len, masks)| {
            if let Some(mut mm) = masks {
                mm.iter_mut().for_each(|m| m.add_offset(offset as isize));

                buf.extend(mm);
            }

            (offset + len, buf)
        })
        .1;

    if masks.is_empty() {
        None
    } else {
        Some(masks)
    }
}

impl Field {
    /// Get field name, if possible
    pub fn get_name(&self) -> Option<&str> {
        match self {
            Field::Simple { name, .. } | Field::Struct { name, .. } => Some(&name[..]),

            Field::Array { field, .. } | Field::TailArray { field, .. } => field.get_name(),

            Field::Bits { name: Some(n), .. } => Some(n.as_str()),

            _ => None,
        }
    }

    /// Get field meta, if any
    pub fn get_meta(&self) -> Option<&FieldMeta> {
        match self {
            Field::Simple { meta, .. }
            | Field::Const { meta, .. }
            | Field::Bits { meta, .. }
            | Field::Struct { meta, .. } => Some(meta),

            Field::Pad(_) => None,

            Field::Array { field, .. } | Field::TailArray { field } => field.get_meta(),
            Field::Use { meta, .. } => Some(meta),
        }
    }

    //noinspection ALL
    /// Get field meta, if any
    pub fn get_meta_mut(&mut self) -> Option<&mut FieldMeta> {
        match self {
            Field::Simple { meta, .. }
            | Field::Const { meta, .. }
            | Field::Bits { meta, .. }
            | Field::Struct { meta, .. } => Some(meta),

            Field::Pad(_) => None,

            Field::Array { field, .. } | Field::TailArray { field } => field.get_meta_mut(),
            Field::Use { meta, .. } => Some(meta),
        }
    }

    /// Get byte mask that can be used for matching
    pub fn get_constant_mask(&self, pld_endian: Endian) -> Option<Vec<ByteMask>> {
        match self {
            Field::Const {
                value,
                meta: FieldMeta { endian, .. },
            } => {
                if self.is_growable() {
                    // won't happen, this check is just for completeness
                    return None;
                }

                Some(
                    value
                        .to_bytes(endian.unwrap_or(pld_endian))
                        .into_iter()
                        .enumerate()
                        .map(|(i, b)| ByteMask {
                            offset: i,
                            value: b,
                            mask: 0xFF,
                        })
                        .collect(),
                )
            }
            Field::Bits {
                items,
                meta:
                    FieldMeta {
                        endian,
                        bitfield_align,
                        ..
                    },
                ..
            } => {
                let splitting = split_bitfield_to_bytes(
                    items,
                    endian.unwrap_or(pld_endian),
                    bitfield_align.unwrap_or_default(),
                );

                let combined: Vec<ByteMask> = splitting
                    .iter()
                    .map(|byte_chunks| {
                        let mut byte: u8 = 0;
                        let mut mask: u8 = 0;
                        for chunk in byte_chunks {
                            if let BitfieldScalar::Const { value, .. } = &chunk.entry {
                                let padded = value.to_u64();
                                byte |= (padded & chunk.mask).shift(chunk.rshift) as u8;
                                mask |= chunk.mask.shift(chunk.rshift) as u8;
                            }
                        }

                        (byte, mask)
                    })
                    .enumerate()
                    .filter_map(|(i, (p, m))| {
                        if m != 0 {
                            Some(ByteMask {
                                offset: i,
                                value: p,
                                mask: m,
                            })
                        } else {
                            None
                        }
                    })
                    .collect();

                if combined.is_empty() {
                    None
                } else {
                    Some(combined)
                }
            }
            Field::Struct {
                fields,
                meta: FieldMeta { endian, .. },
                ..
            } => fields_vec_to_const_mask(fields, endian.unwrap_or(pld_endian)),
            _ => None,
        }
    }

    /// Get constant value, if this field is a simple constant
    pub fn get_constant_value(&self) -> Option<TypeValue> {
        match self {
            Field::Const { value, .. } => Some(*value),
            _ => None,
        }
    }

    /// Get field's minimal byte length
    pub fn byte_length(&self) -> usize {
        match self {
            Field::Simple { dtype, .. } => dtype.byte_size(),
            Field::Const { value, .. } => Type::from(value).byte_size(),
            Field::Pad(len) => *len as usize,
            Field::Bits { items, .. } => {
                let mut bitcount = 0;
                for item in items {
                    bitcount += item.bits;
                }

                if bitcount % 8 != 0 {
                    panic!("Bitfield length not even: {:#?}", self);
                }

                bitcount as usize / 8
            }
            Field::Struct { fields, .. } => {
                let mut sum = 0;
                for f in fields {
                    sum += f.byte_length();
                }
                sum
            }
            Field::Array { count, field } => *count as usize * field.byte_length(),
            Field::TailArray { .. } => 0,
            Field::Use { .. } => panic!("'use' fields should be resolved by now!"),
        }
    }

    /// Check whether the field's byte_length() is fixed or can grow
    pub fn is_growable(&self) -> bool {
        match self {
            Field::Struct { fields, .. } => {
                for f in fields {
                    if f.is_growable() {
                        return true;
                    }
                }
                false
            }
            Field::Array { field, .. } => field.is_growable(),
            Field::TailArray { .. } => true,
            _ => false,
        }
    }

    /// Check if this field contains constants usable for pattern matching
    ///
    /// (bitfields may contain constants, but these are not matched)
    pub fn has_constants(&self) -> bool {
        match self {
            Field::Const { .. } => true,
            Field::Bits { items, .. } => {
                for item in items {
                    if let BitfieldEntry {
                        field: BitfieldEntryKind::Scalar(BitfieldScalar::Const { .. }),
                        ..
                    } = item
                    {
                        return true;
                    }
                }
                false
            }
            Field::Struct { fields, .. } => {
                for item in fields {
                    if item.has_constants() {
                        return true;
                    }
                }
                false
            }
            Field::Array { .. } | Field::TailArray { .. } => {
                // arrays can contain const fields, but they aren't
                // used for pattern matching
                false
            }
            Field::Use { .. } => panic!("'use' fields should be resolved by now!"),
            _ => false,
        }
    }

    /// Check if this field contains constants usable for pattern matching
    pub fn has_variables(&self) -> bool {
        match self {
            Field::Const { .. } => false,
            Field::Bits { items, .. } => {
                for item in items {
                    if item.has_variables() {
                        return true;
                    }
                }
                false
            }
            Field::Struct { fields, .. } => {
                for item in fields {
                    if item.has_variables() {
                        return true;
                    }
                }
                false
            }
            Field::Array { .. } | Field::TailArray { .. } => {
                // may or may not contain constants
                true
            }
            Field::Simple {
                name: _,
                dtype: _,
                meta: _,
            } => true,
            Field::Pad(_) => false,
            Field::Use { .. } => panic!("'use' fields should be resolved by now!"),
        }
    }

    pub fn check_for_duplicate_fields(&self, mut names: &mut Vec<String>) -> Option<String> {
        match self {
            Field::Simple { .. } => None,
            Field::Const { .. } => None,
            Field::Pad(_) => None,
            // these have sub-fields
            Field::Array { field, .. } => field.check_for_duplicate_fields(&mut vec![]),
            Field::TailArray { field } => field.check_for_duplicate_fields(&mut vec![]),
            Field::Struct { fields, .. } => {
                for field in fields {
                    // Named field starts its own scope for sub-fields;
                    // Anonymous field shares scope with this
                    match field.get_name() {
                        Some(n) => {
                            if names.contains(&n.to_string()) {
                                return Some(n.to_string());
                            } else {
                                names.push(n.to_string());

                                let inner = field.check_for_duplicate_fields(&mut vec![]);
                                if inner.is_some() {
                                    return inner;
                                }
                            }
                        }
                        None => {
                            let inner = field.check_for_duplicate_fields(&mut names);
                            if inner.is_some() {
                                return inner;
                            }
                        }
                    }
                }

                None
            }
            Field::Bits { items, .. } => {
                for field in items {
                    let inner = field.check_for_duplicate_fields(&mut names);
                    if inner.is_some() {
                        return inner;
                    }
                }

                None
            }
            Field::Use { .. } => panic!("'use' fields should be resolved by now!"),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::{
        BitfieldAlign, BitfieldEntry, BitfieldEntryKind, BitfieldScalar, Field, FieldMeta,
    };
    use super::{Type, TypeValue};
    use crate::spec::{ByteMask, Endian};

    #[test]
    fn test_fields_constant_mask() {
        // simple has no mask
        let f = Field::Simple {
            name: "".to_string(),
            dtype: Type::Bool,
            meta: Default::default(),
        };
        assert_eq!(None, f.get_constant_mask(Endian::Big));

        // pad has no mask
        let f = Field::Pad(2);
        assert_eq!(None, f.get_constant_mask(Endian::Big));

        // array has no mask
        let f: Field = Field::Array {
            count: 5,
            field: Box::new(Field::Const {
                value: TypeValue::U8(0xF3),
                meta: Default::default(),
            }),
        };
        assert_eq!(None, f.get_constant_mask(Endian::Big));

        // tail array has no mask
        let f = Field::TailArray {
            field: Box::new(Field::Const {
                value: TypeValue::U8(0xF3),
                meta: Default::default(),
            }),
        };
        assert_eq!(None, f.get_constant_mask(Endian::Big));

        // constant simple
        let f = Field::Const {
            value: TypeValue::U8(0xF3),
            meta: Default::default(),
        };
        assert_eq!(
            Some(vec![ByteMask::new(0, 0xF3, 0xFF),]),
            f.get_constant_mask(Endian::Big)
        );

        // constant u16
        let f = Field::Const {
            value: TypeValue::U16(0xF345),
            meta: Default::default(),
        };
        assert_eq!(
            Some(vec![
                ByteMask::new(0, 0xF3, 0xFF),
                ByteMask::new(1, 0x45, 0xFF),
            ]),
            f.get_constant_mask(Endian::Big)
        );
        assert_eq!(
            Some(vec![
                ByteMask::new(0, 0x45, 0xFF),
                ByteMask::new(1, 0xF3, 0xFF),
            ]),
            f.get_constant_mask(Endian::Little)
        );

        // struct with no fields
        let f = Field::Struct {
            name: "".to_string(),
            fields: vec![],
            meta: FieldMeta::default(),
        };
        assert_eq!(None, f.get_constant_mask(Endian::Big));

        // struct with non-mask fields
        let f = Field::Struct {
            name: "".to_string(),
            meta: FieldMeta::default(),
            fields: vec![
                Field::Simple {
                    name: "aaa".to_string(),
                    dtype: Type::Bool,
                    meta: Default::default(),
                },
                Field::Simple {
                    name: "bbb".to_string(),
                    dtype: Type::U32,
                    meta: Default::default(),
                },
            ],
        };
        assert_eq!(None, f.get_constant_mask(Endian::Big));

        // struct with mask fields
        let f = Field::Struct {
            name: "".to_string(),
            meta: FieldMeta::default(),
            fields: vec![
                Field::Simple {
                    name: "aaa".to_string(),
                    dtype: Type::Bool,
                    meta: Default::default(),
                },
                Field::Simple {
                    name: "bbb".to_string(),
                    dtype: Type::U32,
                    meta: Default::default(),
                },
                Field::Const {
                    value: TypeValue::U8(0xF3),
                    meta: Default::default(),
                },
                Field::Simple {
                    name: "ccc".to_string(),
                    dtype: Type::Bool,
                    meta: Default::default(),
                },
                Field::Const {
                    value: TypeValue::U32(0xcafebabe),
                    meta: Default::default(),
                },
            ],
        };
        // The struct specifies the byte order in the payload, first to last byte.
        // Endian only affects bytes in individual multi-byte fields.
        assert_eq!(
            Some(vec![
                ByteMask::new(5, 0xF3, 0xFF),
                ByteMask::new(7, 0xca, 0xFF),
                ByteMask::new(8, 0xfe, 0xFF),
                ByteMask::new(9, 0xba, 0xFF),
                ByteMask::new(10, 0xbe, 0xFF),
            ]),
            f.get_constant_mask(Endian::Big)
        );

        // bitfield with non-mask fields
        let f = Field::Bits {
            name: None,
            items: vec![BitfieldEntry {
                field: BitfieldEntryKind::Scalar(BitfieldScalar::Simple {
                    name: "aaa".to_string(),
                    dtype: Type::U8,
                    meta: Default::default(),
                }),
                bits: 5,
            }],
            meta: Default::default(),
        };
        assert_eq!(None, f.get_constant_mask(Endian::Big));

        // bitfield with mask fields
        let f = Field::Bits {
            name: None,
            items: vec![
                BitfieldEntry {
                    field: BitfieldEntryKind::Scalar(BitfieldScalar::Simple {
                        name: "aaa".to_string(),
                        dtype: Type::U8,
                        meta: Default::default(),
                    }),
                    bits: 5,
                },
                BitfieldEntry {
                    field: BitfieldEntryKind::Scalar(BitfieldScalar::Const {
                        value: TypeValue::U8(0b1101),
                        meta: Default::default(),
                    }),
                    bits: 4,
                },
                BitfieldEntry {
                    field: BitfieldEntryKind::Scalar(BitfieldScalar::Pad),
                    bits: 7,
                },
            ],
            meta: Default::default(),
        };

        assert_eq!(
            Some(vec![
                ByteMask::new(0, 0b0000_0110, 0b0000_0111),
                ByteMask::new(1, 0b1000_0000, 0b1000_0000)
            ]),
            f.get_constant_mask(Endian::Big)
        );
        // endian does not change bitfield bytes order
        assert_eq!(
            Some(vec![
                ByteMask::new(0, 0b0000_0110, 0b0000_0111),
                ByteMask::new(1, 0b1000_0000, 0b1000_0000)
            ]),
            f.get_constant_mask(Endian::Little)
        );

        // bitfield with mask fields, ALIGN RIGHT
        let f = Field::Bits {
            name: None,
            items: vec![
                BitfieldEntry {
                    field: BitfieldEntryKind::Scalar(BitfieldScalar::Simple {
                        name: "aaa".to_string(),
                        dtype: Type::U8,
                        meta: Default::default(),
                    }),
                    bits: 5,
                },
                BitfieldEntry {
                    field: BitfieldEntryKind::Scalar(BitfieldScalar::Const {
                        value: TypeValue::U8(0b1101),
                        meta: Default::default(),
                    }),
                    bits: 4,
                },
                BitfieldEntry {
                    field: BitfieldEntryKind::Scalar(BitfieldScalar::Pad),
                    bits: 7,
                },
            ],
            meta: FieldMeta {
                bitfield_align: Some(BitfieldAlign::Right),
                ..Default::default()
            },
        };

        // the field order is reversed from the previous sample
        assert_eq!(
            Some(vec![
                ByteMask::new(0, 0b1010_0000, 0b1110_0000),
                ByteMask::new(1, 0b0000_0001, 0b0000_0001)
            ]),
            f.get_constant_mask(Endian::Big)
        );

        // struct including bitfields
        let f = Field::Struct {
            name: "".to_string(),
            meta: FieldMeta::default(),
            fields: vec![
                Field::Simple {
                    name: "aaa".to_string(),
                    dtype: Type::Bool,
                    meta: Default::default(),
                },
                Field::Bits {
                    name: None,
                    items: vec![
                        BitfieldEntry {
                            field: BitfieldEntryKind::Scalar(BitfieldScalar::Simple {
                                name: "aaa".to_string(),
                                dtype: Type::U8,
                                meta: Default::default(),
                            }),
                            bits: 5,
                        },
                        BitfieldEntry {
                            field: BitfieldEntryKind::Scalar(BitfieldScalar::Const {
                                value: TypeValue::U8(0b1101),
                                meta: Default::default(),
                            }),
                            bits: 4,
                        },
                        BitfieldEntry {
                            field: BitfieldEntryKind::Scalar(BitfieldScalar::Pad),
                            bits: 7,
                        },
                    ],
                    meta: Default::default(),
                },
                Field::Const {
                    value: TypeValue::U32(0xcafebabe),
                    meta: Default::default(),
                },
                Field::Bits {
                    name: None,
                    items: vec![
                        BitfieldEntry {
                            field: BitfieldEntryKind::Scalar(BitfieldScalar::Const {
                                value: TypeValue::U16(0xC001),
                                meta: Default::default(),
                            }),
                            bits: 16,
                        },
                        BitfieldEntry {
                            field: BitfieldEntryKind::Scalar(BitfieldScalar::Const {
                                value: TypeValue::U16(0xD000),
                                meta: Default::default(),
                            }),
                            bits: 16,
                        },
                    ],
                    meta: Default::default(),
                },
            ],
        };
        // The struct specifies the byte order in the payload, first to last byte.
        // Endian only affects bytes in individual multi-byte fields.
        assert_eq!(
            Some(vec![
                ByteMask::new(1, 0b0000_0110, 0b0000_0111),
                ByteMask::new(2, 0b1000_0000, 0b1000_0000),
                ByteMask::new(3, 0xca, 0xFF),
                ByteMask::new(4, 0xfe, 0xFF),
                ByteMask::new(5, 0xba, 0xFF),
                ByteMask::new(6, 0xbe, 0xFF),
                ByteMask::new(7, 0xC0, 0xFF),
                ByteMask::new(8, 0x01, 0xFF),
                ByteMask::new(9, 0xD0, 0xFF),
                ByteMask::new(10, 0x00, 0xFF),
            ]),
            f.get_constant_mask(Endian::Big)
        );

        assert_eq!(
            Some(vec![
                ByteMask::new(1, 0b0000_0110, 0b0000_0111),
                ByteMask::new(2, 0b1000_0000, 0b1000_0000),
                ByteMask::new(3, 0xbe, 0xFF),
                ByteMask::new(4, 0xba, 0xFF),
                ByteMask::new(5, 0xfe, 0xFF),
                ByteMask::new(6, 0xca, 0xFF),
                ByteMask::new(7, 0x01, 0xFF), // multi-byte fields are swapped
                ByteMask::new(8, 0xC0, 0xFF),
                ByteMask::new(9, 0x00, 0xFF),
                ByteMask::new(10, 0xD0, 0xFF),
            ]),
            f.get_constant_mask(Endian::Little)
        );

        // mixed endian
        let f = Field::Struct {
            name: "".to_string(),
            meta: FieldMeta::default(),
            fields: vec![
                Field::Simple {
                    name: "aaa".to_string(),
                    dtype: Type::Bool,
                    meta: Default::default(),
                },
                Field::Bits {
                    name: None,
                    items: vec![
                        BitfieldEntry {
                            field: BitfieldEntryKind::Scalar(BitfieldScalar::Simple {
                                name: "aaa".to_string(),
                                dtype: Type::U8,
                                meta: Default::default(),
                            }),
                            bits: 5,
                        },
                        BitfieldEntry {
                            field: BitfieldEntryKind::Scalar(BitfieldScalar::Const {
                                value: TypeValue::U8(0b1101),
                                meta: Default::default(),
                            }),
                            bits: 4,
                        },
                        BitfieldEntry {
                            field: BitfieldEntryKind::Scalar(BitfieldScalar::Pad),
                            bits: 7,
                        },
                    ],
                    meta: FieldMeta {
                        descr: None,
                        unit: None,
                        constant_name: None,
                        endian: Some(Endian::Little),
                        bitfield_align: Some(BitfieldAlign::Left),
                        transform: None,
                        repr: None,
                        json_sprintf: None,
                        is_template: false,
                    },
                },
                Field::Const {
                    value: TypeValue::U32(0xcafebabe),
                    meta: Default::default(),
                },
            ],
        };

        assert_eq!(
            Some(vec![
                // this one field is now little endian
                ByteMask::new(1, 0b0000_0110, 0b0000_0111),
                ByteMask::new(2, 0b1000_0000, 0b1000_0000),
                // the rest is default (big) endian
                ByteMask::new(3, 0xca, 0xFF),
                ByteMask::new(4, 0xfe, 0xFF),
                ByteMask::new(5, 0xba, 0xFF),
                ByteMask::new(6, 0xbe, 0xFF),
            ]),
            f.get_constant_mask(Endian::Big)
        );
    }
}
