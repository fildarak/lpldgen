use crate::codegen::utils::Indent;
use crate::codegen::CodegenConfig;
use crate::modules::SpecModule;
use crate::spec::PayloadSpec;

/// Build test functions that look for constant payload bytes defined in the specification.
///
/// These are referenced in the payloads look-up table and help differentiate payloads with the same addressing
/// and length (or variable length, making length match unusable).
pub(crate) fn build_test_funcs(
    config: &CodegenConfig,
    module: &SpecModule,
    specs: &[PayloadSpec],
) -> String {
    let mut items = Vec::new();

    for spec in specs {
        if !spec.has_constants() {
            continue;
        }

        let masks = spec
            .get_constant_mask()
            .expect("Expected constant mask for a spec with constants");
        let mut tests = Vec::new();
        for m in masks {
            if m.mask != 0xFF {
                tests.push(format!(
                    "((bytes[{i}] & {m:#04x}) == {v:#04x})",
                    i = m.offset,
                    m = m.mask,
                    v = m.value
                ));
            } else {
                tests.push(format!(
                    "(bytes[{i}] == {v:#04x})",
                    i = m.offset,
                    v = m.value
                ));
            }
        }

        items.push(
            format!(
                r#"
                /** Test constants in a "{name}" payload */
                static bool {func}(const uint8_t * const bytes) {{
                    return {expr};
                }}
                "#,
                name = &spec.name,
                expr = tests.join(" &&\n").indent_inner(6),
                func = module.substitute_name(&config.tpl_test_fn, &spec.name),
            )
            .unindent(),
        );
    }

    items.join("\n\n")
}
