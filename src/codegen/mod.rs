use std::convert::From;
use std::fs::OpenOptions;
use std::io::Write;
use std::path::Path;

use ifmt::iformat;

pub mod bitfield_split;
mod config;
mod gen_export_json;
mod gen_pack;
mod gen_struct;
mod gen_tests;
mod gen_unpack;
mod packing_layout;
mod recog;
pub mod utils;

pub(crate) use config::Config as CodegenConfig;

use crate::errors::Error;
use crate::modules::SpecModule;
use crate::spec::{tag::CspPort, tag::MsgTag, PayloadSpec};
use utils::{AddSlashes, Indent};

/* c/h templates use /*{TOKEN}*/ to mark substitution sites */
const TEMPLATE_C: &str = include_str!("template/module.c");
const TEMPLATE_H: &str = include_str!("template/module.h");

const STATIC_FILES: &[(&str, &str)] = &[
    ("cJSON.h", include_str!("template/static/cJSON.h")),
    ("cJSON.c", include_str!("template/static/cJSON.c")),
    ("payloads.h", include_str!("template/static/payloads.h")),
    (
        "conf_payloads.h.in",
        include_str!("template/static/conf_payloads.h.in"),
    ),
    ("payloads.c", include_str!("template/static/payloads.c")),
    (
        "payloads_internal.h",
        include_str!("template/static/payloads_internal.h"),
    ),
    (
        "payloads_pretty_print.c",
        include_str!("template/static/payloads_pretty_print.c"),
    ),
];

pub const C_KEYWORDS: [&str; 35] = [
    "auto", "break", "case", "char", "const", "continue", "default", "do", "double", "else",
    "enum", "extern", "float", "for", "goto", "if", "int", "long", "register", "return", "short",
    "signed", "sizeof", "static", "struct", "switch", "typedef", "union", "unsigned", "void",
    "volatile", "while", // extras
    "bool", "NULL", "pld_id", // XXX this is configurable in config
];

struct LibraryBuilder<'a> {
    config: &'a CodegenConfig,
    module: SpecModule,
}

pub(crate) fn generate_static_code<P: AsRef<Path>, Q: AsRef<Path>>(
    config: &CodegenConfig,
    dest_c_h: (P, Q),
) -> Result<(), Error<'static>> {
    if config.generate_static {
        info!(
            "Generating static library files:\n  {}",
            STATIC_FILES
                .iter()
                .map(|(name, _bytes)| name)
                .copied()
                .collect::<Vec<_>>()
                .join("\n  ")
        );

        let (path_c, path_h) = (dest_c_h.0.as_ref(), dest_c_h.1.as_ref());

        let mut fopts = OpenOptions::new();
        fopts.create(true).write(true).truncate(true);

        for (name, chars) in STATIC_FILES {
            if name.ends_with(".h") || name.ends_with(".in") {
                fopts.open(path_h.join(name))?.write_all(chars.as_bytes())?;
            } else {
                fopts.open(path_c.join(name))?.write_all(chars.as_bytes())?;
            }
        }
    }

    Ok(())
}

pub(crate) fn generate_library_code<P: AsRef<Path>, Q: AsRef<Path>>(
    config: &CodegenConfig,
    module: SpecModule,
    dest_c_h: (P, Q),
    absolute_path: bool
) -> Result<(), Error<'static>> {
    let b = LibraryBuilder { config, module };

    for spec in &b.module.specs {
        if let Some(name) = spec.check_for_duplicate_fields() {
            return Err(Error::Message(
                format!("Duplicate field {} in payload {}", name, spec.name).into(),
            ));
        }
    }

    b.generate_library_code(dest_c_h, absolute_path)
}

impl LibraryBuilder<'_> {
    pub fn generate_library_code<P: AsRef<Path>, Q: AsRef<Path>>(
        &self,
        dest_c_h: (P, Q),
        absolute_path: bool
    ) -> Result<(), Error<'static>> {
        let specs = &self.module.specs;

        if specs.is_empty() {
            return Ok(());
        }

        recog::check_payloads_for_duplicates(specs); // will panic on error

        // Disabled to preserve payload order
        // specs.sort_by(|a, b| a.partial_cmp(b).unwrap_or(Ordering::Equal));

        let mut lib_c = TEMPLATE_C.to_string();
        let mut lib_h = TEMPLATE_H.to_string();

        let lib_c_name = self
            .module
            .substitute(&format!("{}.c", self.config.filename));
        let lib_h_name = self
            .module
            .substitute(&format!("{}.h", self.config.filename));

        let (path_c, path_h) = (dest_c_h.0.as_ref(), dest_c_h.1.as_ref());

        let filename_c = if absolute_path {
            path_c.to_path_buf()
        } else {
            path_c.join(lib_c_name.clone())
        };

        let filename_h = if absolute_path {
            path_h.to_path_buf()
        } else {
            path_h.join(lib_h_name.clone())
        };

        info!(
            "Generating module \"{}\":\n  SRC {}\n  HDR {}",
            self.module.opts.name,
            filename_c.display(),
            filename_h.display()
        );

        if absolute_path {
            if filename_c.as_os_str().is_empty() && filename_h.as_os_str().is_empty() {
                return Err(Error::from(
                    "Source file destination and header destination cannot be both empty.",
                ));
            }
        } else {
            if !path_c.exists() || !path_c.is_dir() {
                return Err(Error::from(
                    "Source file destination does not exist or is not a directory.",
                ));
            }

            if !path_h.exists() || !path_h.is_dir() {
                return Err(Error::from(
                    "Header destination does not exist or is not a directory.",
                ));
            }
        }

        let mut fopts = OpenOptions::new();
        fopts.create(true).write(true).truncate(true);

        /* Library header */
        if !filename_h.as_os_str().is_empty() {
            lib_h = lib_h.replace("$INCLUDE_GUARD", &self.module.substitute("PLD_%MODULE_H"));

            lib_h = lib_h.replace(
                "$MODULE_STRUCT_NAME",
                &self.module.substitute(&self.config.tpl_module_struct),
            );

            lib_h = lib_h.replace("$MODULE_NAME", &self.module.opts.name);
            lib_h = lib_h.replace("$VERSION", crate::VERSION);

            lib_h = lib_h.replace(
                "$PAYLOAD_ENUM_NAME",
                &self.module.substitute(&self.config.tpl_enum),
            );

            lib_h = lib_h.replace("$PAYLOAD_ENUM_ITEMS", &self.build_types_enum(specs));

            lib_h = lib_h.replace(
                "$PAYLOAD_STRUCTS",
                &gen_struct::build_payload_structs(self.config, &self.module, specs),
            );

            lib_h = lib_h.replace(
                "$PARSE_FUNCS",
                &gen_unpack::build_parser_protos(self.config, &self.module, specs),
            );

            lib_h = lib_h.replace(
                "$BUILD_FUNCS",
                &gen_pack::build_builder_protos(self.config, &self.module, specs),
            );

            lib_h = lib_h.replace(
                "$EXPORT_FUNCS",
                &gen_export_json::build_export_protos(self.config, &self.module, specs),
            );

            lib_h = lib_h.replace(
                "$SPEC_POINTERS",
                &self.build_ident_pointers(&self.module.specs, true),
            );

            fopts
                .open(filename_h)?
                .write_all(lib_h.as_bytes())?;
        }

        /* Library source */
        if !filename_c.as_os_str().is_empty() {
            lib_c = lib_c.replace(
                "$MODULE_STRUCT_NAME",
                &self.module.substitute(&self.config.tpl_module_struct),
            );

            lib_c = lib_c.replace("$MODULE_NAME", &self.module.opts.name);
            lib_c = lib_c.replace("$headername", &lib_h_name);
            lib_c = lib_c.replace("$VERSION", crate::VERSION);

            lib_c = lib_c.replace("$START_INDEX", {
                let mut val = self.module.opts.id_range.expect("id range required")[0];
                if val == 0 {
                    val = 1; // if a range starts with zero, it is internally changed to 1 (warning is printed in the content generation loop)
                }
                &val.to_string()
            });

            lib_c = lib_c.replace("$SPEC_COUNT", &self.module.specs.len().to_string());

            lib_c = lib_c.replace(
                "$TEST_FUNCS",
                &gen_tests::build_test_funcs(self.config, &self.module, specs),
            );

            lib_c = lib_c.replace(
                "$PARSE_FUNCS",
                &gen_unpack::build_parser_funcs(self.config, &self.module, specs),
            );

            lib_c = lib_c.replace(
                "$BUILD_FUNCS",
                &gen_pack::build_builder_funcs(self.config, &self.module, specs),
            );

            lib_c = lib_c.replace(
                "$EXPORT_FUNCS",
                &gen_export_json::build_export_funcs(self.config, &self.module, specs),
            );

            lib_c = lib_c.replace(
                "$SPEC_TABLE",
                &self.build_payload_lookup_table_items(specs),
            );

            lib_c = lib_c.replace(
                "$SPEC_POINTERS",
                &self.build_ident_pointers(&self.module.specs, false),
            );

            fopts
                .open(filename_c)?
                .write_all(lib_c.as_bytes())?;
        }

        Ok(())
    }

    /// Build the members of a payload types enum (name = value, ...)
    fn build_types_enum(&self, specs: &[PayloadSpec]) -> String {
        let mut items = Vec::new();
        let mut counter = self.module.opts.id_range.expect("ID range required!")[0];

        if counter == 0 {
            // 0 can be used to detect poorly initialized structs
            warn!("Payload ID 0 is reserved, starting from 1!");
            counter = 1;
        }

        let max = self.module.opts.id_range.expect("ID range required!")[1];

        if max < counter {
            structural_error!("Module {} has invalid ID range!", self.module.opts.name);
        }

        for spec in specs {
            if counter > max {
                structural_error!(
                    "Module {} has run out of assigned IDs!",
                    self.module.opts.name
                );
            }

            // also check name validity now
            if C_KEYWORDS.contains(&spec.name.as_str()) {
                structural_error!(
                    "Payload \"{}\" name is a C keyword. Please change the name.",
                    spec.name
                );
            }

            let name = self
                .module
                .substitute_name(&self.config.tpl_enum_item, &spec.name);
            items.push(format!("    {} = {}", name, counter));
            counter += 1;
        }

        items.join(",\n")
    }

    /// Build a look-up table for payload variants.
    /// This is a constant array walked over when a payload is received.
    fn build_payload_lookup_table_items(&self, specs: &[PayloadSpec]) -> String {
        let mut items = Vec::new();
        for spec in specs {
            // 0xFF is a token indicating the field is unused
            let mut src = 0xFF;
            let mut dst = 0xFF;
            let mut sport = 0xFF;
            let mut dport = 0xFF;

            match spec.tag {
                MsgTag::From(a, CspPort(p)) => {
                    src = a;
                    sport = p;
                }
                MsgTag::To(a, CspPort(p)) => {
                    dst = a;
                    dport = p;
                }
                MsgTag::Template => {
                    panic!("Template fields should be resolved by now");
                }
            }

            let mut tester_fn = "NULL".to_string();
            let fn_parse = self
                .module
                .substitute_name(&self.config.tpl_parse_fn, &spec.name);
            let fn_build = self
                .module
                .substitute_name(&self.config.tpl_build_fn, &spec.name);
            let fn_parse_static = self
                .module
                .substitute_name(&self.config.tpl_parse_static_fn, &spec.name);
            let fn_build_static = self
                .module
                .substitute_name(&self.config.tpl_build_static_fn, &spec.name);

            if spec.has_constants() {
                tester_fn = self
                    .module
                    .substitute_name(&self.config.tpl_test_fn, &spec.name);
            }

            let bin_size = spec.byte_length();
            let growable = spec.is_growable();
            let recognize = spec.recognize;

            let to_json_fn = self
                .module
                .substitute_name(&self.config.tpl_inner_export_fn, &spec.name);
            let pld_enum_item = self
                .module
                .substitute_name(&self.config.tpl_enum_item, &spec.name);

            let struct_name = self
                .module
                .substitute_name(&self.config.tpl_payload_struct, &spec.name);

            let (tail_size, tail_c_size) = if let Some((_, size)) = spec.tail_array_info() {
                (
                    size,
                    self.module.substitute_name(
                        &self.config.tpl_struct_tail_elem_c_size_const,
                        &spec.name,
                    ),
                )
            } else {
                (0, "0".to_string())
            };

            let name = format!("\"{}\"", struct_name);
            let descr = spec
                .descr
                .as_ref()
                .map(AddSlashes::add_slashes_and_quotes)
                .unwrap_or_else(|| "NULL".to_string());

            items.push(
                iformat!(
                    r##"
            {{
                .pld_id = {pld_enum_item},
                .address = {{
                    .src = {src},
                    .sport = {sport},
                    .dst = {dst},
                    .dport = {dport}
                }},
                .bin_size = {bin_size},
                .c_size = sizeof(struct {struct_name}),
                .growable = {growable},
                .tail_elem_bin_size = {tail_size},
                .tail_elem_c_size = {tail_c_size},
                .recognize = {recognize},
                .tester = {tester_fn},
            #if PLD_HAVE_ALLOC
                .parse = (fn_parse_t) {fn_parse},
                .build = (fn_build_t) {fn_build},
            #endif /* PLD_HAVE_ALLOC */
                .parse_static = (fn_parse_static_t) {fn_parse_static},
                .build_static = (fn_build_static_t) {fn_build_static},
                .name = {name},
            #if PLD_HAVE_EXPORT
                .descr = {descr},
                .data_to_json = (fn_data_to_json_t) {to_json_fn}
            #endif /* PLD_HAVE_EXPORT */
            }}
            "##
                )
                .unindent()
                .indent(1),
            );
        }

        items.join(",\n")
    }

    fn build_ident_pointers(&self, specs: &[PayloadSpec], prototypes: bool) -> String {
        let mut items = Vec::new();

        for (n, spec) in specs.iter().enumerate() {
            let varname = self
                .module
                .substitute_name(&self.config.tpl_spec_pointer, &spec.name);

            if prototypes {
                items.push(iformat!("extern const struct pld_spec * const {varname};"));
            } else {
                items.push(iformat!(
                    "const struct pld_spec * const {varname} = &payload_specs[{n}];"
                ));
            }
        }

        items.join("\n")
    }
}
