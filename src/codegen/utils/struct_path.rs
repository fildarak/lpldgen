use std::fmt::{self, Display};

/// Helper for accessing C struct members by name.
/// It supports nesting and array indexing
#[derive(Clone, Debug)]
pub struct StructPath {
    /// The current path string
    path: String,
    /// Suffix to add to the next child node name (array index)
    next_child_suffix: Option<String>,
    /// True if the root variable is a pointer (all lower are direct members)
    is_pointer: bool,
}

impl StructPath {
    /// Create a struct path pointing to the struct root
    pub fn new(name: impl Into<String>, is_pointer: bool) -> Self {
        Self {
            path: name.into(),
            next_child_suffix: None,
            is_pointer,
        }
    }

    /// Get path to a child member
    pub fn child(&self, name: &str) -> Self {
        let mut copy = self.clone();
        if !self.is_pointer {
            copy.path.push('.');
        } else if self.is_pointer {
            copy.path.push_str("->");
        }
        copy.is_pointer = false;
        copy.path.push_str(name);

        if let Some(s) = &copy.next_child_suffix {
            copy.path.push_str(s);
            copy.next_child_suffix = None;
        }

        copy
    }

    /// Tack something to the end of all direct descendant nodes (e.g. `i` for `[i]`)
    pub fn add_index_for_children(&self, what: &str) -> Self {
        let mut copy = self.clone();
        copy.next_child_suffix = Some(format!("[{}]", what));
        copy
    }
}

impl Display for StructPath {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.write_str(&self.path)?;
        Ok(())
    }
}
