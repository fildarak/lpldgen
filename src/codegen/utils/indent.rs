/// Indent and unindent a string
pub trait Indent {
    /// Remove leading indents
    fn unindent(&self) -> String;
    /// Indent lines
    fn indent(&self, count: usize) -> String;
    /// Indent second and later lines
    fn indent_inner(&self, count: usize) -> String;
}

impl Indent for &'_ str {
    fn unindent(&self) -> String {
        if self.trim().is_empty() {
            return String::new();
        }

        let mut ll = self.lines().skip_while(|l| l.trim().is_empty()).peekable();

        let next = ll.peek().unwrap();

        let space_count = next.chars().take_while(|c| *c == ' ').count();

        ll.map(|s| {
            let first_nonzero = s.chars().take_while(|c| *c == ' ').count();
            &s[first_nonzero.min(space_count)..]
        })
        .collect::<Vec<&str>>()
        .join("\n")
        .trim_end()
        .to_string()
    }

    fn indent(&self, count: usize) -> String {
        let indentbare = "    ".repeat(count);
        let indentstr = "\n".to_string() + &indentbare;
        let replaced = self.replace("\n", &indentstr);
        (indentbare + &replaced).trim_end().to_string()
    }

    fn indent_inner(&self, count: usize) -> String {
        let indentbare = "    ".repeat(count);
        let indentstr = "\n".to_string() + &indentbare;
        let replaced = self.replace("\n", &indentstr);
        replaced.trim_end().to_string()
    }
}

impl Indent for String {
    fn unindent(&self) -> String {
        self.as_str().unindent()
    }

    fn indent(&self, count: usize) -> String {
        self.as_str().indent(count)
    }

    fn indent_inner(&self, count: usize) -> String {
        self.as_str().indent_inner(count)
    }
}

impl Indent for &String {
    fn unindent(&self) -> String {
        self.as_str().unindent()
    }

    fn indent(&self, count: usize) -> String {
        self.as_str().indent(count)
    }

    fn indent_inner(&self, count: usize) -> String {
        self.as_str().indent_inner(count)
    }
}

#[cfg(test)]
mod tests {
    use super::Indent;

    #[test]
    fn test_indent_lines() {
        assert_eq!("", "".indent(10));
        assert_eq!("    aaa", "aaa".indent(1));
        assert_eq!("        aaa", "aaa".indent(2));
        assert_eq!("    foo\n      aaa\n    bb", "foo\n  aaa\nbb".indent(1));
    }

    #[test]
    fn test_unindent() {
        assert_eq!("", "".unindent(), "empty");
        assert_eq!("xx", "  xx".unindent(), "simple trim");
        assert_eq!(
            "xx\n  ff\naa",
            "  xx\n    ff\n  aa".unindent(),
            "multi line"
        );
        assert_eq!(
            "aa\nbb\ncc\n d",
            "    aa\nbb\n  cc\n     d".unindent(),
            "multi line, indents too short"
        );
        assert_eq!("xx", "  \n    xx".unindent(), "blank leading line");
        assert_eq!(
            "xx\n  ff",
            "  \n    xx\n      ff".unindent(),
            "blank leading line and multiline"
        );
        assert_eq!(
            "hello\n            hello\n        hello",
            r#"
            hello
                hello
            hello
            "#
            .unindent()
            .indent_inner(2)
        );

        assert_eq!(
            "for ({ivar} = 0; {ivar} < {count}; {ivar}++) {{\n    {inner}\n}}",
            r#"
                    for ({ivar} = 0; {ivar} < {count}; {ivar}++) {{
                        {inner}
                    }}
                    "#
            .unindent()
        );

        assert_eq!(
            "lpld_error_t {func}(const struct {struct} *data, uint8_t **output, size_t *len) {{
{code}
    return LPLD_OK;
}}",
            r#"
            lpld_error_t {func}(const struct {struct} *data, uint8_t **output, size_t *len) {{
            {code}
                return LPLD_OK;
            }}
            "#
            .unindent(),
            "blank leading line and multiline"
        );
    }
}
