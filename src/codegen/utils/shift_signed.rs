pub trait Cast<B: Copy> {
    fn cast(self) -> B;
}

impl Cast<isize> for i32 {
    fn cast(self) -> isize {
        self as isize
    }
}

impl Cast<isize> for i64 {
    fn cast(self) -> isize {
        self as isize
    }
}

impl Cast<isize> for u32 {
    fn cast(self) -> isize {
        self as isize
    }
}

impl Cast<isize> for u64 {
    fn cast(self) -> isize {
        self as isize
    }
}

impl Cast<isize> for usize {
    fn cast(self) -> isize {
        self as isize
    }
}

impl Cast<isize> for u8 {
    fn cast(self) -> isize {
        self as isize
    }
}

impl Cast<isize> for i8 {
    fn cast(self) -> isize {
        self as isize
    }
}

impl<T: Copy> Cast<T> for T {
    fn cast(self) -> T {
        self
    }
}

impl<T: Copy> Cast<T> for &T {
    fn cast(self) -> T {
        *self
    }
}

pub trait ShiftLeftRight {
    fn shift(self, shift: impl Cast<isize>) -> Self;
}

impl ShiftLeftRight for u64 {
    fn shift(self, right: impl Cast<isize>) -> Self {
        let s: isize = right.cast();
        if s >= 0 {
            self >> (s as u64)
        } else {
            self << (-s as u64)
        }
    }
}

impl ShiftLeftRight for u8 {
    fn shift(self, right: impl Cast<isize>) -> Self {
        let s: isize = right.cast();
        if s >= 0 {
            self >> (s as u8)
        } else {
            self << (-s as u8)
        }
    }
}

impl ShiftLeftRight for u16 {
    fn shift(self, right: impl Cast<isize>) -> Self {
        let s: isize = right.cast();
        if s >= 0 {
            self >> (s as u16)
        } else {
            self << (-s as u16)
        }
    }
}

impl ShiftLeftRight for u32 {
    fn shift(self, right: impl Cast<isize>) -> Self {
        let s: isize = right.cast();
        if s >= 0 {
            self >> (s as u32)
        } else {
            self << (-s as u32)
        }
    }
}

impl ShiftLeftRight for usize {
    fn shift(self, right: impl Cast<isize>) -> Self {
        let s: isize = right.cast();
        if s >= 0 {
            self >> (s as usize)
        } else {
            self << (-s as usize)
        }
    }
}

#[cfg(test)]
mod tests {
    use super::ShiftLeftRight;

    #[test]
    fn test_shifts() {
        assert_eq!(0xF0, 0x0Fu8.shift(-4));
        assert_eq!(0x01, 0x10u8.shift(4));
        assert_eq!(0x8000, 1u16.shift(-15));
        assert_eq!(0x8000, 1usize.shift(-15));
        assert_eq!(1, 0b1011u32.shift(3));
    }
}
