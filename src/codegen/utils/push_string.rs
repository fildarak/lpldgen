/// Extension to String to append owned String
pub trait PushString {
    /// Append a string, functionally identical to `push_str()`
    fn push_string(&mut self, s: String);
}

impl PushString for String {
    fn push_string(&mut self, s: String) {
        self.push_str(s.as_str())
    }
}
