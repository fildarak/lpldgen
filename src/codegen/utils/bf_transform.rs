use crate::spec::field::{BitfieldEntry, BitfieldEntryKind, BitfieldScalar, Field};

/// Convert bitfield scalar to normal field
fn bf_scalar_to_field(bfs: &BitfieldScalar) -> Field {
    match bfs {
        BitfieldScalar::Simple { name, dtype, meta } => Field::Simple {
            name: name.clone(),
            dtype: *dtype,
            meta: meta.clone(),
        },
        BitfieldScalar::Const { value, meta } => Field::Const {
            value: *value,
            meta: meta.clone(),
        },
        BitfieldScalar::Pad => Field::Pad(1),
    }
}

/// convert bitfield to equivalent normal field (for struct exporting)
pub fn bitfield_to_field(bf: &BitfieldEntry) -> Field {
    match &bf.field {
        BitfieldEntryKind::Scalar(x) => bf_scalar_to_field(x),
        BitfieldEntryKind::ScalarArray { count, field } => Field::Array {
            count: *count,
            field: Box::new(bf_scalar_to_field(field)),
        },
        BitfieldEntryKind::Struct { name, fields, meta } => Field::Struct {
            name: name.clone(),
            meta: meta.clone(),
            fields: fields.iter().map(bitfield_to_field).collect(),
        },
        BitfieldEntryKind::StructArray {
            name,
            count,
            fields,
            meta,
        } => Field::Array {
            count: *count,
            field: Box::new(Field::Struct {
                name: name.clone(),
                meta: meta.clone(),
                fields: fields.iter().map(bitfield_to_field).collect(),
            }),
        },
    }
}
