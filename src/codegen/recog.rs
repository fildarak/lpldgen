//! Payload recognition & checking for duplicates

use crate::spec::PayloadSpec;
use crate::spec::{tag::MsgTag, ByteMask};

use std::fmt::{self, Display, Formatter};

#[derive(Debug, PartialEq, Eq, Clone, Hash)]
struct PldPattern {
    addr: MsgTag,
    mask: Option<Vec<ByteMask>>,
}

impl From<&PayloadSpec> for PldPattern {
    fn from(spec: &PayloadSpec) -> Self {
        PldPattern {
            addr: spec.tag,
            mask: spec.get_constant_mask(),
        }
    }
}

enum Size {
    Fixed(usize),
    Growable(usize),
}

impl Size {
    fn new(len: usize, growable: bool) -> Self {
        if growable {
            Size::Growable(len)
        } else {
            Size::Fixed(len)
        }
    }

    fn len(&self) -> usize {
        *match self {
            Size::Fixed(s) => s,
            Size::Growable(s) => s,
        }
    }

    fn is_growable(&self) -> bool {
        match self {
            Size::Fixed(_) => false,
            Size::Growable(_) => true,
        }
    }
}

impl Display for Size {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.len())?;

        if self.is_growable() {
            write!(f, "+")?;
        }

        Ok(())
    }
}

/// Detect if any two payloads have conflicting recognition pattern (CSP address/port, constant fields, length)
pub fn check_payloads_for_duplicates(payloads: &[PayloadSpec]) {
    let mut map = Vec::<(PldPattern, String, Size)>::new();

    for pld in payloads {
        // Skip payloads excluded from recognition
        if !pld.recognize {
            continue;
        }

        let pat = PldPattern::from(pld);
        let sz = Size::new(pld.byte_length(), pld.is_growable());

        for (pat2, name2, sz2) in &map {
            if name2 == &pld.name {
                structural_error!("Multiple definitions for payload \"{}\"", name2);
            }

            if pat2.addr != pat.addr {
                // CSP addressing differs, collision not possible
                continue;
            }

            if
            // both growable, we can't tell them apart
            (sz.is_growable() && sz2.is_growable()) ||
                // one growable with minimal size < the other's fixed size, there's ambiguity
                (sz.is_growable() && !sz2.is_growable() && sz.len() < sz2.len()) ||
                (sz2.is_growable() && !sz.is_growable() && sz2.len() < sz.len()) ||
                // neither growable, but same size
                (!sz2.is_growable() && !sz.is_growable() && sz2.len() == sz.len())
            {
                let er_lengths = format!("compatible lengths {} vs {}", sz, sz2);

                // now we have the same size, check for patterns
                if pat2 == &pat {
                    structural_error!(
                        "Duplicate payloads \"{}\" and \"{}\": CSP match, {}, same constant masks",
                        pld.name,
                        name2,
                        er_lengths
                    );
                }

                // It's still possible that one has constants overlapping with the other's variable fields.
                // This can be a form of "specialization", but more likely it's simply a bug.

                if pat2.mask.as_ref().xor(pat.mask.as_ref()).is_some() {
                    // one has mask, the other doesn't
                    let tmp = if pat.mask.is_none() {
                        &pld.name
                    } else {
                        &name2
                    };

                    structural_error!("Duplicate payloads \"{}\" and \"{}\": CSP match, {}, constants overlap variables (\"{}\" has no constants)",
                           pld.name, name2, er_lengths, tmp);
                }

                if pat2.mask.is_some() {
                    let mut had_difference = false;

                    'bm2: for bm2 in pat2.mask.as_ref().unwrap() {
                        for bm in pat.mask.as_ref().unwrap() {
                            if bm.position_matches(bm2) && !bm.matches(bm2) {
                                had_difference = true;
                                break 'bm2;
                            }
                        }
                    }

                    if !had_difference {
                        structural_error!("Duplicate payloads \"{}\" and \"{}\": CSP match, {}, all constant bytes match or overlap variables",
                                       pld.name, name2, er_lengths);
                    }
                }
            }
        }

        map.push((pat, pld.name.clone(), sz));
    }
}
