#![allow(clippy::enum_variant_names)]

#[macro_use]
extern crate log;
#[macro_use]
extern crate smart_default;
#[macro_use]
extern crate serde_derive;

#[macro_use]
mod macros;

mod codegen;
mod errors;
mod modules;
mod parser;
mod spec;
mod utils;

use crate::codegen::CodegenConfig;
use crate::errors::Error;
use crate::modules::{ModuleLoader, SpecParseContext};
use crate::spec::PayloadSpec;

use std::collections::HashMap;
use std::env;
use std::fs::File;
use std::io::Read;
use std::path::{Path, PathBuf};
use std::str::FromStr;

use clappconfig::{clap, AppConfig};
use nom::error::convert_error;
use path_clean::PathClean;

const CONFIG_FILE: &str = "config.json";
const SOFTWARE_NAME: &str = "Payload Transcoder Generator";

#[derive(SmartDefault, Serialize, Deserialize, Debug)]
#[serde(deny_unknown_fields)]
#[serde(default)]
struct Config {
    /// Logging level to use (can be increased using -v flags)
    #[default = "debug"]
    logging: String,

    log_filters: HashMap<String, String>,

    /// Paths to module directories.
    /// Each listed directory must contain a "module.json" file.
    modules: Vec<String>,

    /// Variables (override variables defined in modules)
    variables: HashMap<String, u64>,

    /// Working directory, always replaced with the config file's parent folder path.
    #[serde(skip)]
    workdir: Option<PathBuf>,

    /// Where the include files are written
    #[default = ""]
    dest_inc: String,

    /// Where the .c files are written
    #[default = ""]
    dest_src: String,

    /// Where both .c and .h files are written (if set)
    #[default = ""]
    dest: String,

    #[default = false]
    single_output_file: bool,

    /// Code generator settings
    codegen: CodegenConfig,
}

impl AppConfig for Config {
    type Init = Config;

    fn logging(&self) -> &str {
        &self.logging
    }

    fn logging_mod_levels(&self) -> Option<&HashMap<String, String>> {
        Some(&self.log_filters)
    }

    fn add_args<'a: 'b, 'b>(clap: clap::App<'a, 'b>) -> clap::App<'a, 'b> {
        clap
            .arg(
                clap::Arg::with_name("output")
                    .short("o")
                    .long("dest")
                    .value_name("DIR")
                    .help("Sets a destination folder for both source and header files, absolute or relative to working directory")
                    .takes_value(true)
            )
            .arg(
                clap::Arg::with_name("output-src")
                    .long("dest-src")
                    .value_name("DIR")
                    .help("Sets a destination folder for generated source files, absolute or relative to working directory")
                    .takes_value(true)
            )
            .arg(
                clap::Arg::with_name("output-inc")
                    .long("dest-inc")
                    .value_name("DIR")
                    .help("Sets a destination folder for generated header files, absolute or relative to working directory")
                    .takes_value(true)
            )
            .arg(
                clap::Arg::with_name("output-file-inc")
                    .long("output-inc")
                    .value_name("FILE")
                    .help("Sets an output header file to be generated for single module")
                    .takes_value(true)
            )
            .arg(
                clap::Arg::with_name("output-file-src")
                    .long("output-src")
                    .value_name("FILE")
                    .help("Sets an output source file to be generated for single module")
                    .takes_value(true)
            )
    }

    fn on_config_file_found(&mut self, path: &PathBuf) {
        let workdir = path.parent().unwrap().to_owned();

        println!("Workdir: {}", workdir.display());

        self.workdir = Some(workdir);
    }

    fn configure(mut self, argv: &clap::ArgMatches) -> anyhow::Result<Self::Init> {
        // ensure we have some workdir
        if self.workdir.is_none() {
            self.workdir = Some(env::current_dir().unwrap());
        }

        let workdir = self.workdir.as_ref().unwrap();

        // 'dest' works as a fallback
        if self.dest_src.is_empty() {
            self.dest_src = self.dest.clone();
        }

        if self.dest_inc.is_empty() {
            self.dest_inc = self.dest.clone();
        }

        // override dest
        if let Some(path) = argv.value_of_lossy("output") {
            self.dest_src = validate_output_dir(workdir, &path)?;
            self.dest_inc = self.dest_src.clone();
        } else {
            // prefer arg, fallback to config file

            if argv.value_of_lossy("output-file-inc").is_some() ||
               argv.value_of_lossy("output-file-src").is_some() {
                self.single_output_file = true;
                if let Some(out_file) = argv.value_of_lossy("output-file-inc") {
                    self.dest_inc = out_file.to_string();
                }
                if let Some(out_file) = argv.value_of_lossy("output-file-src") {
                    self.dest_src = out_file.to_string();
                }
                return Ok(self)
            }

            if let Some(c_path) = argv.value_of_lossy("output-src") {
                self.dest_src = validate_output_dir(workdir, &c_path)?;
            } else {
                self.dest_src = validate_output_dir(workdir, &self.dest_src)?;
            }

            if let Some(h_path) = argv.value_of_lossy("output-inc") {
                self.dest_inc = validate_output_dir(workdir, &h_path)?;
            } else {
                self.dest_inc = validate_output_dir(workdir, &self.dest_inc)?;
            }
        }

        Ok(self)
    }
}

/// Check output dir, resolve WRT workdir
fn validate_output_dir(workdir: &Path, dir: &str) -> anyhow::Result<String> {
    if dir.is_empty() {
        anyhow::bail!("Missing output directory!");
    }

    let mut dest = PathBuf::from_str(dir)?;

    if !dest.is_absolute() {
        dest = workdir.join(dest).clean();
    }

    if !dest.is_dir() {
        anyhow::bail!("Output directory \"{}\" not valid!", dir);
    }

    Ok(dest.to_str().unwrap().to_owned())
}

/// Main entry point
fn main() {
    if let Err(e) = main_inner() {
        eprintln!(
            "---------------- Backtrace ---------------\n\
            {:?}\n\
            -------------------------------------------\n\
            \x1b[31;1mERROR:\x1b[m {}",
            e, e
        );
    }
}

const VERSION: &str = env!("CARGO_PKG_VERSION");

fn main_inner() -> anyhow::Result<()> {
    let config = Config::init(SOFTWARE_NAME, CONFIG_FILE, VERSION)?;

    let mut loader = ModuleLoader::new();

    let cwd_pb = config.workdir.as_ref().unwrap();

    let mut parse_context = SpecParseContext::new();
    parse_context.set_variables(config.variables.iter());

    loader.queue_modules_to_parse(&config.modules, Some(cwd_pb), &parse_context);
    let modules = loader.into_modules()?;

    trace!("Loaded modules: {:#?}", modules);

    let paths = (&config.dest_src, &config.dest_inc);

    if config.single_output_file && modules.len() > 1 {
        anyhow::bail!("Single file output expects single module, found {} modules!", modules.len());
    }

    for module in modules {
        codegen::generate_library_code(&config.codegen, module, paths, config.single_output_file)?;
    }

    if !config.single_output_file {
        codegen::generate_static_code(&config.codegen, paths)?;
    }

    info!("Codegen OK.");

    Ok(())
}

// Parsing is sequential using a shared context (it contains variable values to substitute where needed)
// - this avoids having to pass the context around inside the nom parser.
lazy_static::lazy_static! {
    pub(crate) static ref PARSE_CONTEXT : parking_lot::Mutex<SpecParseContext> = Default::default();
}

/// Load a file and convert it to a vec of payloads
fn file_to_payloads<P: AsRef<Path>>(
    path: P,
    parse_context: SpecParseContext,
) -> Result<Vec<PayloadSpec>, Error<'static>> {
    info!("Parsing: {}", path.as_ref().to_string_lossy());

    let mut p = File::open(&path)?;
    let mut buf = String::new();
    p.read_to_string(&mut buf)?;

    if buf.is_empty() {
        return Ok(vec![]);
    }

    *PARSE_CONTEXT.lock() = parse_context;

    let res = parser::parse(&buf);

    match res {
        Ok(vec) => Ok(vec),
        Err(Error::Message(m)) => {
            error!("{}", m);
            Err(Error::Message(m.to_string().into()))
        }
        Err(Error::SpecParse(m)) => {
            let msg = m.message.unwrap_or_else(|| "Unexpected syntax".into());

            let verbose_error = m.nom_error;

            eprintln!("\n\x1b[31;1m*** PARSE ERROR ***\x1b[m\n");

            //eprintln!("{:?}", verbose_error);

            eprint!("\x1b[31mError: {}\x1b[m\n\n", msg);

            eprintln!(
                "\x1b[90mError locations in {}\x1b[m\n\n{}",
                path.as_ref().display(),
                convert_error(buf.as_str(), verbose_error)
            );

            eprintln!("\x1b[31mParsing failed.\x1b[m\n");
            std::process::exit(1); // This looks better than a panic
        }
        Err(e) => {
            error!("{:#?}", e);
            Err(e.to_static())
        }
    }
}
