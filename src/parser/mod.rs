use std::convert::TryFrom;

use nom::branch::alt;
use nom::bytes::complete::{tag, take_until, take_while};
use nom::character::complete::{none_of, one_of};
use nom::combinator::{cut, map, opt, recognize, value};
use nom::error::{ErrorKind, ParseError, VerboseError};
use nom::multi::{many0, many1};
use nom::sequence::{delimited, pair, preceded, separated_pair, terminated, tuple};
use nom::IResult;

pub mod expr;
pub mod utils;

use crate::errors::{Error, SpecParseError};
use crate::parser::utils::{eof, identifier_extra};
use crate::spec::field::{
    self, BitfieldEntry, BitfieldEntryKind, BitfieldScalar, Field, FieldMeta, FieldMetaOption,
    Type, TypeValue,
};
use crate::spec::{
    field::BitfieldAlign, tag::CspPort, tag::MsgTag, Endian, PayloadOption, PayloadSpec,
};
use utils::{
    i64_literal, identifier, map_res2, non_whitespace, quoted_string, space_around0, space_around1,
    u64_literal, whitespace0, whitespace_to_eol,
};

fn option_format<'a>() -> impl Fn(&'a str) -> IResult<&'a str, (&'a str, String), SpecParseError> {
    |input: &str| {
        alt((
            tuple((
                preceded(space_around0(tag("+")), space_around1(identifier_extra(".[]()"))),
                terminated(
                    alt((
                        // quoted string with escape sequences
                        quoted_string(),
                        // anything within square brackets
                        delimited(
                            tag("["),
                            map(recognize(many1(none_of("]"))), |s: &'a str| s.into()),
                            tag("]"),
                        ),
                        // single token
                        map(non_whitespace(), |s| s.into()),
                    )),
                    whitespace_to_eol(),
                ),
            )),
            map(
                preceded(space_around0(tag("///")), take_while(|x| x != '\n')),
                |line| ("descr", line.to_owned()),
            ),
            map(
                terminated(
                    preceded(space_around0(tag("+")), identifier_extra(".[]()")),
                    whitespace_to_eol(),
                ),
                |ident| (ident, "".to_string()),
            ),
        ))(input)
    }
}

/// Parse the option directive:
///
/// +name value
///
/// whitespace and comments are discarded
fn payload_option<'a>() -> impl Fn(&'a str) -> IResult<&'a str, PayloadOption, SpecParseError> {
    |input: &str| {
        map_res2(option_format(), |(k, v)| {
            match k {
                "from" | "to" => {
                    if v.contains(':') {
                        let parts: Vec<&str> = v.split(':').collect();

                        let addr_s = parts[0];
                        let port_s = parts[1];

                        let addr = if addr_s.starts_with(|c: char| c.is_numeric()) {
                            u64_literal::<SpecParseError<'_>>()(addr_s)
                                .map_err(|_e| {
                                    nom_failure_msg!(input, "Error parsing address number")
                                })?
                                .1
                        } else {
                            // maybe a variable (or syntax error)
                            if let Some(val) = crate::PARSE_CONTEXT.lock().get_variable(addr_s) {
                                val
                            } else {
                                return Err(nom_failure_msg!(
                                    input,
                                    "Addr: Not a number, and no such variable defined"
                                ));
                            }
                        };

                        let addr_u8 = u8::try_from(addr).map_err(|_e| {
                            nom_failure_msg!(input, "Error converting address to u8")
                        })?;

                        let port = if port_s.starts_with(|c: char| c.is_numeric()) {
                            u64_literal::<SpecParseError<'_>>()(port_s)
                                .map_err(|_e| nom_failure_msg!(input, "Error parsing port number"))?
                                .1
                        } else {
                            // maybe a variable (or syntax error)
                            if let Some(val) = crate::PARSE_CONTEXT.lock().get_variable(port_s) {
                                val
                            } else {
                                return Err(nom_failure_msg!(
                                    input,
                                    "Port: Not a number, and no such variable defined"
                                ));
                            }
                        };

                        let port_u8 = u8::try_from(port)
                            .map_err(|_e| nom_failure_msg!(input, "Error converting port to u8"))?;

                        match k {
                            "from" => {
                                Ok(PayloadOption::Tag(MsgTag::From(addr_u8, CspPort(port_u8))))
                            }
                            "to" => Ok(PayloadOption::Tag(MsgTag::To(addr_u8, CspPort(port_u8)))),
                            _ => unreachable!(),
                        }
                    } else {
                        Err(nom_failure_msg!(input, "Expected format ADDR:PORT"))
                    }
                }
                "endian" => Ok(PayloadOption::Endian(match v.as_str() {
                    "big" => Endian::Big,
                    "little" => Endian::Little,
                    _ => {
                        return Err(nom_failure_msg!(
                            input,
                            "Endian must be \"big\" or \"little\"!"
                        ))
                    }
                })),
                "descr" => Ok(PayloadOption::Descr(v)),
                "recognize" => Ok(PayloadOption::Recognize(match v.as_str() {
                    "true" | "yes" | "y" | "1" => true,
                    "false" | "no" | "n" | "0" => false,
                    _ => return Err(nom_failure_msg!(input, "Bad value for +recognize!")),
                })),
                _ => Err(nom_failure_msg!(input, "Unknown option!")),
            }
        })(input)
    }
}

/// Parse the field meta option directive:
///
/// +name value
///
/// whitespace and comments are discarded
///
/// this is the same as the payload option, except a different set of options is supported
fn field_meta_option<'a>() -> impl Fn(&'a str) -> IResult<&'a str, FieldMetaOption, SpecParseError>
{
    |input: &str| {
        map_res2(option_format(), |(k, v)| {
            Ok(match k {
                "endian" => FieldMetaOption::Endian(match v.as_str() {
                    "big" => Endian::Big,
                    "little" => Endian::Little,
                    _ => {
                        return Err(nom_failure_msg!(
                            input,
                            "Endian must be \"big\" or \"little\"!"
                        ));
                    }
                }),
                "align" => FieldMetaOption::BitfieldAlign(match v.as_str() {
                    "left" | "high" => BitfieldAlign::Left,
                    "right" | "low" => BitfieldAlign::Right,
                    _ => {
                        return Err(nom_failure_msg!(
                            input,
                            "Bitfield Align must be \"left\" or \"right\"!"
                        ));
                    }
                }),
                "descr" => FieldMetaOption::Descr(v),
                "named" => FieldMetaOption::NamedConst(v),
                "unit" => FieldMetaOption::Unit(v),
                "repr" => FieldMetaOption::Repr(Type::try_from(v.as_str()).expect("Bad repr")),
                "fmt" => FieldMetaOption::JsonSprintf(v), // validating printf format is too complex, let's hope it is correct
                "transform" => FieldMetaOption::Transform(match expr::infix_to_ast(&v) {
                    Ok(n) => n,
                    Err(e) => {
                        parser_error!(input, "Expr parse error: {}", e.get_message());
                    }
                }),
                _ => {
                    return Err(nom_failure_msg!(input, "Unknown option!"));
                }
            })
        })(input)
    }
}

fn data_type<'a, E: ParseError<&'a str>>() -> impl Fn(&'a str) -> IResult<&'a str, Type, E> {
    |i: &str| {
        alt((
            value(Type::Bool, tag("bool")),
            value(Type::U8, alt((tag("u8"), tag("uint8_t")))),
            value(Type::U16, alt((tag("u16"), tag("uint16_t")))),
            value(Type::U24, alt((tag("u24"), tag("uint24_t")))),
            value(Type::U32, alt((tag("u32"), tag("uint32_t")))),
            value(Type::U64, alt((tag("u64"), tag("uint64_t")))),
            value(Type::I8, alt((tag("i8"), tag("int8_t"), tag("char")))),
            value(Type::I16, alt((tag("i16"), tag("int16_t")))),
            value(Type::I24, alt((tag("i24"), tag("int24_t")))),
            value(Type::I32, alt((tag("i32"), tag("int32_t")))),
            value(Type::I64, alt((tag("i64"), tag("int64_t")))),
            value(Type::F32, tag("float")),
            value(Type::F32, tag("f32")),
            value(Type::F64, tag("double")),
            value(Type::F64, tag("f64")),
        ))(i)
    }
}

/// Matches scalar fields and arrays
fn simple_field<'a>() -> impl Fn(&'a str) -> IResult<&'a str, field::Field, SpecParseError> {
    |input: &str| {
        map_res2(
            pair(
                many0(field_meta_option()),
                terminated(
                    preceded(
                        whitespace0(),
                        alt((
                            // type ident
                            // type ident[count]
                            // type =value for constants
                            // type _
                            // type _[count]
                            tuple((
                                space_around1(data_type()),
                                // ident or constant value
                                alt((
                                    recognize(preceded(space_around0(tag("=")), i64_literal())),
                                    space_around0(identifier()),
                                )),
                                opt(space_around0(delimited(
                                    tag("["),
                                    take_until("]"),
                                    tag("]"),
                                ))),
                            )),
                            // pad count
                            map(
                                preceded(space_around1(tag("pad")), recognize(u64_literal())),
                                |x| (Type::U8, "_", Some(x)),
                            ),
                        )),
                    ),
                    opt(space_around0(one_of(";,"))),
                ),
            ),
            |(metas, (dtype, name, opt_count))| {
                #[allow(clippy::manual_strip)]
                if name.starts_with('=') {
                    if opt_count.is_some() {
                        return Err(nom_failure_msg!(
                            input,
                            "repetition is not allowed for constants"
                        ));
                    }

                    // float and double constants not implemented
                    let (_, val) = match i64_literal::<VerboseError<_>>()(&name[1..]) {
                        Ok(v) => v,
                        Err(_) => {
                            return Err(nom_failure_msg!(name, "Invalid constant syntax"));
                        }
                    };

                    // DRY gods weep
                    return Ok(Field::Const {
                        value: match dtype {
                            Type::U8 => TypeValue::U8(match u8::try_from(val) {
                                Ok(v) => v,
                                Err(_) => {
                                    return Err(nom_failure_msg!(
                                        name,
                                        "Could not convert value to u8"
                                    ))
                                }
                            }),

                            Type::U16 => TypeValue::U16(match u16::try_from(val) {
                                Ok(v) => v,
                                Err(_) => {
                                    return Err(nom_failure_msg!(
                                        name,
                                        "Could not convert value to u16"
                                    ))
                                }
                            }),

                            Type::U24 => TypeValue::U24(match u32::try_from(val) {
                                Ok(v) => v,
                                Err(_) => {
                                    return Err(nom_failure_msg!(
                                        name,
                                        "Could not convert value to u32"
                                    ))
                                }
                            }),

                            Type::U32 => TypeValue::U32(match u32::try_from(val) {
                                Ok(v) => v,
                                Err(_) => {
                                    return Err(nom_failure_msg!(
                                        name,
                                        "Could not convert value to u32"
                                    ))
                                }
                            }),

                            Type::U64 => TypeValue::U64(match u64::try_from(val) {
                                Ok(v) => v,
                                Err(_) => {
                                    return Err(nom_failure_msg!(
                                        name,
                                        "Could not convert value to u64"
                                    ))
                                }
                            }),

                            Type::I8 => TypeValue::I8(match i8::try_from(val) {
                                Ok(v) => v,
                                Err(_) => {
                                    return Err(nom_failure_msg!(
                                        name,
                                        "Could not convert value to i8"
                                    ))
                                }
                            }),

                            Type::I16 => TypeValue::I16(match i16::try_from(val) {
                                Ok(v) => v,
                                Err(_) => {
                                    return Err(nom_failure_msg!(
                                        name,
                                        "Could not convert value to i16"
                                    ))
                                }
                            }),

                            Type::I32 => TypeValue::I32(match i32::try_from(val) {
                                Ok(v) => v,
                                Err(_) => {
                                    return Err(nom_failure_msg!(
                                        name,
                                        "Could not convert value to i32"
                                    ))
                                }
                            }),

                            Type::I24 => TypeValue::I24(match i32::try_from(val) {
                                Ok(v) => v,
                                Err(_) => {
                                    return Err(nom_failure_msg!(
                                        name,
                                        "Could not convert value to i32"
                                    ))
                                }
                            }), // TODO sign extend if given as hex or bin

                            Type::I64 => TypeValue::I64(val),

                            Type::Bool => TypeValue::Bool(val != 0),
                            _ => {
                                return Err(nom_failure_msg!(
                                    input,
                                    "such constants type not allowed"
                                ));
                            }
                        },
                        meta: FieldMeta::from_options(&metas),
                    });
                }

                for m in &metas {
                    if let FieldMetaOption::NamedConst(_) = m {
                        return Err(nom_failure_msg!(input, "Only constants can be +named"));
                    }
                }

                // fields named "_" become padding
                if name == "_" {
                    let mut len = dtype.byte_size() as u32;

                    if let Some(count_str) = opt_count {
                        len *= u64_literal()(count_str)?.1 as u32;
                    }
                    return Ok(Field::Pad(len as usize));
                }

                let field = Field::Simple {
                    name: name.into(),
                    dtype,
                    meta: FieldMeta::from_options(&metas),
                };
                match opt_count {
                    Some(count_str) => {
                        // error conversion hacks
                        match make_field_array(count_str, Box::new(field)) {
                            Ok(x) => Ok(x),
                            Err(e) => Err(nom::Err::Failure(e)),
                        }
                    }
                    None => Ok(field),
                }
            },
        )(input)
    }
}

/// Matches template use statement
fn use_field<'a>() -> impl Fn(&'a str) -> IResult<&'a str, field::Field, SpecParseError> {
    |input: &str| {
        map_res2(
            tuple((
                many0(field_meta_option()),
                terminated(
                    preceded(space_around1(tag("use")), space_around0(identifier_extra("/"))),
                    opt(space_around0(one_of(";,"))),
                ),
            )),
            |(metas, name)| {
                for m in &metas {
                    match m {
                        FieldMetaOption::Descr(_)
                        | FieldMetaOption::NamedConst(_)
                        | FieldMetaOption::Unit(_)
                        | FieldMetaOption::Repr(_)
                        | FieldMetaOption::BitfieldAlign(_)
                        | FieldMetaOption::Transform(_)
                        | FieldMetaOption::JsonSprintf(_) => {
                            return Err(nom_failure_msg!(
                                input,
                                "Invalid arguments for a Use statement"
                            ));
                        }
                        FieldMetaOption::Endian(_) => {}
                    }
                }

                let field = Field::Use {
                    template: name.into(),
                    meta: FieldMeta::from_options(&metas),
                };
                Ok(field)
            },
        )(input)
    }
}

/// Match a field of any type
fn a_field<'a>() -> impl Fn(&'a str) -> IResult<&'a str, Field, SpecParseError> {
    |input: &str| {
        space_around0(alt((
            simple_field(),
            struct_field(false),
            bitmap_field(),
            use_field(),
        )))(input)
    }
}

/// Matches named struct
fn struct_field<'a>(
    is_payload: bool,
) -> impl Fn(&'a str) -> IResult<&'a str, Field, SpecParseError> {
    move |input: &str| {
        map_res2(
            pair(
                many0(field_meta_option()),
                preceded(
                    whitespace0(),
                    terminated(
                        tuple((
                            // struct name
                            space_around1(if is_payload {
                                alt((tag("struct"), tag("template")))
                            } else {
                                // must have the same type, lol
                                alt((tag("struct"), tag("struct")))
                            }),
                            space_around0(identifier()),
                            // repetition brackets
                            opt(space_around0(delimited(
                                tag("["),
                                take_until("]"),
                                tag("]"),
                            ))),
                            // struct body
                            space_around0(preceded(
                                tag("{"),
                                cut(terminated(space_around0(many0(a_field())), tag("}"))),
                            )),
                        )),
                        opt(space_around0(one_of(";,"))),
                    ),
                ),
            ),
            move |(metas, (keyword, name, opt_count, fields))| {
                let mut meta = FieldMeta::from_options(&metas);
                if !meta.applies_to_struct() || meta.bitfield_align.is_some() {
                    return Err(nom_failure_msg!(input, "Invalid options for struct!"));
                }

                if !is_payload && fields.is_empty() {
                    return Err(nom_failure_msg!(input, "Struct must not be empty!"));
                }

                if keyword == "template" {
                    meta.is_template = true;
                }

                let field = Field::Struct {
                    name: name.into(),
                    fields,
                    meta,
                };

                match opt_count {
                    Some(count_str) => {
                        // error conversion hacks
                        match make_field_array(count_str, Box::new(field)) {
                            Ok(x) => Ok(x),
                            Err(e) => Err(nom::Err::Failure(e)),
                        }
                    }
                    None => Ok(field),
                }
            },
        )(input)
    }
}

/// Helper function to compose an array based on what's inside the square brackets
fn make_field_array(count_str: &str, field: Box<Field>) -> Result<Field, SpecParseError> {
    let count_str = count_str.trim();

    if count_str == "..." {
        return Ok(Field::TailArray { field });
    }

    if let Ok((_, val)) = u64_literal::<(&str, ErrorKind)>()(count_str) {
        Ok(Field::Array {
            count: val as usize,
            field,
        })
    } else {
        Err(SpecParseError::new(count_str, "Error parsing array index"))
    }
}

/// Convert parsed Field to a bitfield scalar
fn field2bitfield_scalar(field: Field) -> Result<field::BitfieldScalar, Field> {
    Ok(match field {
        Field::Simple { name, dtype, meta } => BitfieldScalar::Simple { name, dtype, meta },
        Field::Const { value, meta } => BitfieldScalar::Const { value, meta },
        Field::Pad { .. } => BitfieldScalar::Pad,
        _ => return Err(field),
    })
}

/// Simple bitfield member
fn bitfield_memb_simple<'a>() -> impl Fn(&'a str) -> IResult<&'a str, BitfieldEntry, SpecParseError>
{
    move |input: &str| {
        terminated(
            alt((
                // <type> <name> : <bits>
                map_res2(
                    separated_pair(
                        simple_field(),
                        space_around0(tag(":")),
                        space_around0(map(u64_literal(), |v| v as u32)),
                    ),
                    |(field, bits)| {
                        if bits == 0 {
                            return Err(nom_failure_msg!(input, "bitfield item size must be > 0"));
                        }

                        match field {
                            Field::Simple { .. } | Field::Const { .. } | Field::Pad(..) => {
                                let res = field2bitfield_scalar(field);

                                if res.is_err() {
                                    return Err(nom_failure_msg!(
                                        input,
                                        "not allowed inside bitfield"
                                    ));
                                }

                                Ok(BitfieldEntry {
                                    field: BitfieldEntryKind::Scalar(res.unwrap()),
                                    bits: bits as usize,
                                })
                            }
                            Field::Array {
                                count,
                                field: afield,
                            } => {
                                let res = field2bitfield_scalar(*afield);

                                if res.is_err() {
                                    return Err(nom_failure_msg!(
                                        input,
                                        "not allowed inside bitfield array"
                                    ));
                                }

                                let scalar = res.unwrap();
                                Ok(BitfieldEntry {
                                    field: field::BitfieldEntryKind::ScalarArray {
                                        count,
                                        field: scalar,
                                    },
                                    bits: (bits as usize * count),
                                })
                            }
                            _ => Err(nom_failure_msg!(input, "not allowed inside bitfield")),
                        }
                    },
                ),
                // bool <name>
                map(
                    pair(
                        many0(field_meta_option()),
                        preceded(space_around1(tag("bool")), space_around0(identifier())),
                    ),
                    |(options, name)| BitfieldEntry {
                        field: BitfieldEntryKind::Scalar(BitfieldScalar::Simple {
                            name: name.to_string(),
                            dtype: Type::Bool,
                            meta: FieldMeta::from_options(&options),
                        }),
                        bits: 1,
                    },
                ),
                // _ : <bits>
                map(
                    preceded(
                        pair(space_around0(tag("_")), space_around0(tag(":"))),
                        space_around0(u64_literal()),
                    ),
                    |bits| BitfieldEntry {
                        field: BitfieldEntryKind::Scalar(BitfieldScalar::Pad),
                        bits: bits as usize,
                    },
                ),
                // pad <bits>
                map(
                    preceded(space_around1(tag("pad")), space_around0(u64_literal())),
                    |bits| BitfieldEntry {
                        field: BitfieldEntryKind::Scalar(BitfieldScalar::Pad),
                        bits: bits as usize,
                    },
                ),
            )),
            opt(space_around0(one_of(";,"))),
        )(input)
    }
}

/// Matches bitfield
///
/// bits {
///     field : bits
///     _ : padbits
///     ...
/// }
fn bitmap_field<'a>() -> impl Fn(&'a str) -> IResult<&'a str, Field, SpecParseError> {
    |input: &str| {
        map_res2(
            pair(
                many0(field_meta_option()),
                terminated(
                    preceded(
                        whitespace0(),
                        pair(
                            preceded(
                                space_around1(tag("bits")),
                                opt(pair(
                                    space_around0(identifier()),
                                    opt(space_around0(delimited(
                                        tag("["),
                                        take_until("]"),
                                        tag("]"),
                                    ))),
                                )),
                            ),
                            space_around0(preceded(
                                tag("{"),
                                cut(terminated(
                                    many1(alt((bitfield_memb_simple(), bitmap_struct_field()))),
                                    tag("}"),
                                )),
                            )),
                        ),
                    ),
                    opt(space_around0(one_of(";,"))),
                ),
            ),
            |(metas, (nr_opt, mut items))| {
                let mut bf_name = None;

                if items.is_empty() {
                    return Err(nom_failure_msg!(input, "Bitfield must not be empty!"));
                }

                if let Some((name, repeat)) = nr_opt {
                    bf_name = Some(name.to_string());

                    if let Some(count_str) = repeat {
                        let bits = items.iter().fold(0, |acc, e| acc + e.bits);

                        let count = match i64_literal::<SpecParseError>()(count_str) {
                            Ok((_, v)) => v as usize,
                            Err(_) => {
                                if count_str == "..." {
                                    if bits % 8 == 0 {
                                        // Create a wrapping array with the bitfield inside
                                        return Ok(Field::TailArray {
                                            field: Box::new(Field::Bits {
                                                name: bf_name,
                                                items,
                                                meta: FieldMeta::from_options(&metas),
                                            }),
                                        });
                                    } else {
                                        return Err(nom_failure_msg!(count_str, "Element of bitfield tail array must be multiple of 8 bits long"));
                                    }
                                } else {
                                    return Err(nom_failure_msg!(
                                        count_str,
                                        "Invalid bitfield count"
                                    ));
                                }
                            }
                        };

                        items = vec![BitfieldEntry {
                            field: BitfieldEntryKind::StructArray {
                                name: bf_name.take().unwrap(),
                                count,
                                fields: items,
                                meta: FieldMeta::from_options(&metas),
                            },
                            bits: bits * count,
                        }];
                    }
                }

                Ok(Field::Bits {
                    name: bf_name,
                    items,
                    meta: FieldMeta::from_options(&metas),
                })
            },
        )(input)
    }
}

/// Named bitfield struct
fn bitmap_struct_field<'a>() -> impl Fn(&'a str) -> IResult<&'a str, BitfieldEntry, SpecParseError>
{
    |input: &str| {
        map_res2(
            pair(
                many0(field_meta_option()),
                terminated(
                    preceded(
                        whitespace0(),
                        tuple((
                            // struct name
                            space_around1(tag("struct")),
                            space_around0(identifier()),
                            // repetition brackets
                            opt(space_around0(delimited(
                                tag("["),
                                take_until("]"),
                                tag("]"),
                            ))),
                            // struct body
                            space_around0(cut(preceded(
                                tag("{"),
                                terminated(
                                    space_around0(many1(alt((
                                        bitfield_memb_simple(),
                                        bitmap_struct_field(),
                                    )))),
                                    tag("}"),
                                ),
                            ))),
                        )),
                    ),
                    opt(space_around0(one_of(";,"))),
                ),
            ),
            |(metas, (_, name, opt_count, fields))| {
                if fields.is_empty() {
                    return Err(nom_failure_msg!(
                        input,
                        "Bitfield struct must not be empty!"
                    ));
                }

                let bits = fields.iter().fold(0, |acc, e| acc + e.bits);

                let meta = FieldMeta::from_options(&metas);

                if !meta.applies_to_struct() || meta.bitfield_align.is_some() {
                    return Err(nom_failure_msg!(
                        input,
                        "Invalid options for bitfield struct!"
                    ));
                }

                Ok(if let Some(count_str) = opt_count {
                    //                    let count = u64_literal::<SpecParseError>()(count_str).unwrap().1 as u32;

                    let count = match i64_literal::<SpecParseError>()(count_str) {
                        Ok((_, v)) => v as usize,
                        Err(_) => {
                            return Err(nom_failure_msg!(count_str, "Invalid bitfield count"));
                        }
                    };

                    BitfieldEntry {
                        field: BitfieldEntryKind::StructArray {
                            name: name.to_string(),
                            count,
                            fields,
                            meta,
                        },
                        bits: bits as usize * count as usize,
                    }
                } else {
                    BitfieldEntry {
                        field: BitfieldEntryKind::Struct {
                            name: name.to_string(),
                            fields,
                            meta,
                        },
                        bits,
                    }
                })
            },
        )(input)
    }
}

/// Parse a payload specification
pub fn payload_spec<'a>() -> impl Fn(&'a str) -> IResult<&'a str, PayloadSpec, SpecParseError> {
    |input: &str| {
        map(
            pair(
                // this eats all options, so the struct field does not parse any meta by itself
                many0(space_around0(payload_option())),
                struct_field(true),
            ),
            |(opts, stru)| {
                let mut pld = PayloadSpec::from_options(&opts);

                if let Field::Struct { fields, name, meta } = stru {
                    pld.name = name;
                    pld.fields = fields;

                    if meta.is_template {
                        pld.tag = MsgTag::Template;
                    }
                } else {
                    unreachable!();
                }

                pld
            },
        )(input)
    }
}

/// Parse one or more payloads from a file
pub fn multiple_payloads<'a>(
) -> impl Fn(&'a str) -> IResult<&'a str, Vec<PayloadSpec>, SpecParseError> {
    |input: &str| terminated(many1(space_around0(payload_spec())), eof)(input)
}

/// Parse string to payloads
pub fn parse(input: &str) -> Result<Vec<PayloadSpec>, Error> {
    let res = multiple_payloads()(input);
    match res {
        Ok((remainder, vec)) => {
            if !remainder.is_empty() {
                Err(Error::from(format!(
                    "Text left over after parsing payloads spec: {}",
                    remainder
                )))
            } else {
                Ok(vec)
            }
        }
        Err(e) => Err(e.into()),
    }
}

#[cfg(test)]
mod tests {
    use nom::error::VerboseError;

    use crate::spec::field::{
        self, BitfieldEntry, BitfieldEntryKind, BitfieldScalar, Field, FieldMeta, FieldMetaOption,
        Type, TypeValue,
    };
    use crate::spec::PayloadOption;
    use crate::spec::{tag::CspPort, tag::MsgTag, Endian, PayloadSpec, DEFAULT_ENDIAN};

    use super::{
        bitmap_field, data_type, field_meta_option, multiple_payloads, payload_option,
        payload_spec, simple_field, struct_field,
    };

    #[test]
    fn test_field_meta_option() {
        let parser = field_meta_option();

        assert_eq!(
            Ok(("", FieldMetaOption::Endian(Endian::Big))),
            parser("+endian big")
        );

        assert_eq!(
            Ok(("", FieldMetaOption::JsonSprintf("%04x".to_string()))),
            parser("+fmt %04x")
        );

        // quotes are stripped
        assert_eq!(
            Ok(("", FieldMetaOption::JsonSprintf("%04x".to_string()))),
            parser("+fmt \"%04x\"")
        );

        // spaces and unicode allowed
        assert_eq!(
            Ok(("", FieldMetaOption::JsonSprintf("%04x °C".to_string()))),
            parser("+fmt \"%04x °C\"")
        );

        // TODO more
    }

    #[test]
    fn test_data_type() {
        let parser = data_type::<VerboseError<&str>>();

        assert_eq!(Ok(("", Type::Bool)), parser("bool"));
    }

    #[test]
    fn test_simple_field() {
        let parser = simple_field();

        assert_eq!(
            Ok((
                "",
                Field::Simple {
                    name: "field".into(),
                    dtype: Type::U8,
                    meta: FieldMeta::default(),
                }
            )),
            parser("u8 field")
        );
        assert_eq!(
            Ok((
                "",
                Field::Simple {
                    name: "field".into(),
                    dtype: Type::U16,
                    meta: FieldMeta::default(),
                }
            )),
            parser("u16 field")
        );
        assert_eq!(
            Ok((
                "",
                Field::Simple {
                    name: "field".into(),
                    dtype: Type::U24,
                    meta: FieldMeta::default(),
                }
            )),
            parser("u24 field")
        );
        assert_eq!(
            Ok((
                "",
                Field::Simple {
                    name: "field".into(),
                    dtype: Type::U32,
                    meta: FieldMeta::default(),
                }
            )),
            parser("u32 field")
        );
        assert_eq!(
            Ok((
                "",
                Field::Simple {
                    name: "field".into(),
                    dtype: Type::U64,
                    meta: FieldMeta::default(),
                }
            )),
            parser("u64 field")
        );

        assert_eq!(
            Ok((
                "",
                Field::Simple {
                    name: "field".into(),
                    dtype: Type::I8,
                    meta: FieldMeta::default(),
                }
            )),
            parser("i8 field")
        );
        assert_eq!(
            Ok((
                "",
                Field::Simple {
                    name: "field".into(),
                    dtype: Type::I16,
                    meta: FieldMeta::default(),
                }
            )),
            parser("i16 field")
        );
        assert_eq!(
            Ok((
                "",
                Field::Simple {
                    name: "field".into(),
                    dtype: Type::I32,
                    meta: FieldMeta::default(),
                }
            )),
            parser("i32 field")
        );
        assert_eq!(
            Ok((
                "",
                Field::Simple {
                    name: "field".into(),
                    dtype: Type::I24,
                    meta: FieldMeta::default(),
                }
            )),
            parser("i24 field")
        );
        assert_eq!(
            Ok((
                "",
                Field::Simple {
                    name: "field".into(),
                    dtype: Type::I64,
                    meta: FieldMeta::default(),
                }
            )),
            parser("i64 field")
        );

        assert_eq!(
            Ok((
                "",
                Field::Simple {
                    name: "field".into(),
                    dtype: Type::Bool,
                    meta: FieldMeta::default(),
                }
            )),
            parser("bool field")
        );

        assert_eq!(
            Ok((
                "",
                Field::Simple {
                    name: "field".into(),
                    dtype: Type::F32,
                    meta: FieldMeta {
                        endian: Some(Endian::Big),
                        descr: Some("banana deluxe".into()),
                        ..Default::default()
                    },
                }
            )),
            parser("+endian big\n+descr \"banana deluxe\"\nf32 field")
        );
        assert_eq!(
            Ok((
                "",
                Field::Simple {
                    name: "field".into(),
                    dtype: Type::F32,
                    meta: FieldMeta::default(),
                }
            )),
            parser("float field")
        );
        assert_eq!(
            Ok((
                "",
                Field::Simple {
                    name: "field".into(),
                    dtype: Type::F64,
                    meta: FieldMeta::default(),
                }
            )),
            parser("f64 field")
        );
        assert_eq!(
            Ok((
                "",
                Field::Simple {
                    name: "field".into(),
                    dtype: Type::F64,
                    meta: FieldMeta::default(),
                }
            )),
            parser("double field")
        );

        assert_eq!(Ok(("", Field::Pad(4))), parser("u32 _"));

        assert_eq!(Ok(("", Field::Pad(6))), parser("i16 _[3]"));

        assert_eq!(Ok(("", Field::Pad(6))), parser("pad 6"));

        assert_eq!(
            Ok((
                "",
                Field::Const {
                    value: TypeValue::I16(123),
                    meta: FieldMeta::default(),
                }
            )),
            parser("i16 =123")
        );

        assert_eq!(
            Ok((
                "",
                Field::Const {
                    value: TypeValue::U8(0xFF),
                    meta: FieldMeta::default(),
                }
            )),
            parser("u8 =0xFF")
        );

        assert_eq!(
            Ok((
                "",
                Field::Const {
                    value: TypeValue::I16(-255),
                    meta: FieldMeta::default(),
                }
            )),
            parser("i16 =-0xFF")
        );

        assert_eq!(
            Ok((
                "",
                Field::Array {
                    count: 1234,
                    field: Box::new(Field::Simple {
                        name: "simple".into(),
                        dtype: Type::U32,
                        meta: FieldMeta::default(),
                    }),
                }
            )),
            parser("u32 simple[1234]")
        );

        assert_eq!(
            Ok((
                "",
                Field::Array {
                    count: 0xabcd,
                    field: Box::new(Field::Simple {
                        name: "simple".into(),
                        dtype: Type::U32,
                        meta: FieldMeta::default(),
                    }),
                }
            )),
            parser("u32 simple[0xabcd]")
        );
    }

    #[test]
    fn test_struct_field() {
        assert_eq!(
            Ok((
                "",
                Field::Struct {
                    name: "mystruct".into(),
                    meta: FieldMeta {
                        descr: Some("AlfaOmega".to_string()),
                        unit: None,
                        constant_name: None,
                        endian: None,
                        bitfield_align: None,
                        transform: None,
                        repr: None,
                        json_sprintf: None,
                        is_template: false
                    },
                    fields: vec![
                        Field::Simple {
                            name: "simple".into(),
                            dtype: Type::U32,
                            meta: FieldMeta::default(),
                        },
                        Field::Simple {
                            name: "small".into(),
                            dtype: Type::U16,
                            meta: FieldMeta {
                                endian: Some(Endian::Little),
                                ..Default::default()
                            },
                        },
                        Field::Simple {
                            name: "uhhh".into(),
                            dtype: Type::Bool,
                            meta: FieldMeta::default(),
                        },
                        Field::TailArray {
                            field: Box::new(Field::Simple {
                                name: "ar".into(),
                                dtype: Type::U8,
                                meta: FieldMeta::default(),
                            })
                        },
                    ]
                }
            )),
            struct_field(false)(
                r#"
                +descr AlfaOmega
                struct mystruct {
                    u32 simple
                    +endian little
                    u16 small bool uhhh
                    // comment ...
                    u8 ar[...]
                }//??
                "#
            )
        );
    }

    #[test]
    fn test_payload_option() {
        let parser = payload_option();

        assert_eq!(
            Ok(("", PayloadOption::Endian(Endian::Big))),
            parser("+endian big"),
            "endian 1"
        );
        assert_eq!(
            Ok(("aaa", PayloadOption::Endian(Endian::Big))),
            parser("+endian big\naaa"),
            "endian 2"
        );
        assert_eq!(
            Ok(("", PayloadOption::Endian(Endian::Big))),
            parser("+endian big   \n"),
            "endian 3"
        );
        assert_eq!(
            Ok(("", PayloadOption::Endian(Endian::Big))),
            parser("   +   \t endian  big   \n"),
            "endian 4"
        );

        assert_eq!(
            Ok(("", PayloadOption::Endian(Endian::Little))),
            parser("+endian little"),
            "endian little"
        );

        assert_eq!(
            Ok(("", PayloadOption::Tag(MsgTag::From(123, CspPort(45))))),
            parser("+from 123:45"),
            "from dec"
        );
        assert_eq!(
            Ok(("", PayloadOption::Tag(MsgTag::To(123, CspPort(0))))),
            parser("+to 123:0"),
            "to dec"
        );

        // check other numeric formats support

        assert_eq!(
            Ok(("xxx", PayloadOption::Tag(MsgTag::From(16, CspPort(12))))),
            parser("+from     1_6:1_2\nxxx"),
            "from dec 2 _"
        );
        assert_eq!(
            Ok(("", PayloadOption::Tag(MsgTag::From(0xFF, CspPort(0xA))))),
            parser("+from 0xff:0xa"),
            "from hexa"
        );

        assert_eq!(
            Ok(("", PayloadOption::Tag(MsgTag::From(0xf0, CspPort(1))))),
            parser("+from 0xf0:1 //comment here"),
            "from with comment"
        );
        assert_eq!(
            Ok(("aa", PayloadOption::Tag(MsgTag::From(0xf0, CspPort(5))))),
            parser("+from 0xf_0:5 //comment here\naa"),
            "from with comment 2"
        );
        assert_eq!(
            Ok(("", PayloadOption::Tag(MsgTag::From(0xff, CspPort(5))))),
            parser("+from 0xff:5//comment here"),
            "from with comment, no space"
        );
        assert_eq!(
            Ok(("aa", PayloadOption::Tag(MsgTag::From(0xf0, CspPort(0xf))))),
            parser("+from 0xf_0:0x_f//comment here\naa"),
            "from with comment 2, no space"
        );
        assert_eq!(
            Ok(("aa", PayloadOption::Tag(MsgTag::From(0xf0, CspPort(3))))),
            parser("+from 0xf0:3 /* x\nxx */ // fufufu \naa"),
            "from with comment 3"
        );

        assert_eq!(
            Ok(("", PayloadOption::Tag(MsgTag::From(0b111, CspPort(1))))),
            parser("+from 0b111:0b01"),
            "from bin"
        );

        assert_eq!(
            Ok((
                "",
                PayloadOption::Descr("hello dolly \" lalal \\aaa\\nline2".to_string())
            )),
            parser(r#"+descr [hello dolly " lalal \aaa\nline2]"#)
        );

        assert_eq!(
            Ok((
                "",
                PayloadOption::Descr("hello dolly \" lalal \\aaa\nline2".to_string())
            )),
            parser(r#"+descr "hello dolly \" lalal \\aaa\nline2""#)
        );

        assert_eq!(
            Ok((
                "                aa",
                PayloadOption::Descr("hello dolly \" lalal \\aaa\nline2".to_string())
            )),
            parser(
                r#"+descr "hello dolly \" lalal \\aaa\nline2"//bebebe comment here ...
                aa"#
            )
        );
        assert_eq!(
            Ok((
                "                aa",
                PayloadOption::Descr("hello dolly \" lalal \\aaa\nline2".to_string())
            )),
            parser(
                r#"+descr "hello dolly \" lalal \\aaa\nline2"/*
                multiline
                comment
                */
                aa"#
            )
        );
    }

    #[test]
    fn test_many_payloads() {
        assert_eq!(
            Ok((
                "",
                vec![
                    PayloadSpec {
                        name: "GsNpw_GET_HK_2_WDT_Resp".into(),
                        descr: None,
                        tag: MsgTag::From(0x31, CspPort(8)),
                        endian: DEFAULT_ENDIAN,
                        recognize: true,
                        fields: vec![
                            Field::Simple {
                                dtype: Type::U32,
                                name: "wdt_i2c_time_left".into(),
                                meta: FieldMeta::default(),
                            },
                            Field::Array {
                                count: 2,
                                field: Box::new(Field::Simple {
                                    dtype: Type::U8,
                                    name: "wdt_csp_pings_left".into(),
                                    meta: FieldMeta::default(),
                                })
                            },
                        ],
                    },
                    PayloadSpec {
                        name: "GsNpw_GET_HK_2_Basic_Req".into(),
                        descr: Some("lalala lelele".into()),
                        tag: MsgTag::To(0x31, CspPort(15)),
                        endian: Endian::Little,
                        fields: vec![Field::Const {
                            value: TypeValue::U8(3),
                            meta: FieldMeta::default(),
                        }],
                        recognize: true,
                    },
                    PayloadSpec {
                        name: "Empty".into(),
                        descr: None,
                        tag: MsgTag::To(0x31, CspPort(8)),
                        endian: DEFAULT_ENDIAN,
                        fields: vec![],
                        recognize: true,
                    },
                    PayloadSpec {
                        name: "Empty2".into(),
                        descr: None,
                        tag: MsgTag::To(0x31, CspPort(8)),
                        endian: DEFAULT_ENDIAN,
                        fields: vec![],
                        recognize: true,
                    },
                ]
            )),
            multiple_payloads()(
                r#"
                +from 0x31:8
                struct GsNpw_GET_HK_2_WDT_Resp {
                    u32 wdt_i2c_time_left
                    u8 wdt_csp_pings_left[2]
                }

                +to 0x31:15
                +endian little
                +descr "lalala lelele"
                struct GsNpw_GET_HK_2_Basic_Req {
                    u8 =3
                }

                +to 0x31:8
                struct Empty {
                }

                +to 0x31:8
                struct Empty2 {
                }
                "#
            )
        )
    }

    #[test]
    fn test_payload_spec() {
        assert_eq!(
            Ok((
                "",
                PayloadSpec {
                    name: "myPayload".into(),
                    descr: Some("aaa bb ccccc".into()),
                    tag: MsgTag::From(12, CspPort(27)),
                    endian: Endian::Little,
                    recognize: true,
                    fields: vec![
                        Field::Simple {
                            dtype: Type::U8,
                            name: "first".into(),
                            meta: FieldMeta::default(),
                        },
                        Field::Pad(3),
                        Field::Simple {
                            dtype: Type::I64,
                            name: "second".into(),
                            meta: FieldMeta::default(),
                        }
                    ],
                }
            )),
            payload_spec()(
                r#"
                +from 12:27
                +endian little
                +descr "aaa bb ccccc"

                struct myPayload {
                    u8 first
                    pad 3
                    i64 second
                }
                "#
            )
        )
    }

    #[test]
    fn test_bitmap_field() {
        // various items including padding
        assert_eq!(
            Ok((
                "",
                Field::Bits {
                    name: None,
                    items: vec![
                        BitfieldEntry {
                            field: BitfieldEntryKind::Scalar(BitfieldScalar::Simple {
                                dtype: Type::U8,
                                name: "aaa".into(),
                                meta: FieldMeta::default(),
                            }),
                            bits: 4
                        },
                        BitfieldEntry {
                            field: BitfieldEntryKind::Scalar(BitfieldScalar::Pad),
                            bits: 4
                        },
                        BitfieldEntry {
                            field: BitfieldEntryKind::Scalar(BitfieldScalar::Const {
                                value: TypeValue::U32(15),
                                meta: FieldMeta::default(),
                            }),
                            bits: 7
                        },
                        BitfieldEntry {
                            field: BitfieldEntryKind::Scalar(BitfieldScalar::Pad),
                            bits: 16
                        },
                        BitfieldEntry {
                            field: BitfieldEntryKind::Struct {
                                meta: FieldMeta::default(),
                                fields: vec![
                                    BitfieldEntry {
                                        field: BitfieldEntryKind::Scalar(BitfieldScalar::Simple {
                                            dtype: Type::U8,
                                            name: "num".into(),
                                            meta: FieldMeta::default(),
                                        }),
                                        bits: 7
                                    },
                                    BitfieldEntry {
                                        field: BitfieldEntryKind::Scalar(BitfieldScalar::Simple {
                                            dtype: Type::Bool,
                                            name: "pullup".into(),
                                            meta: FieldMeta::default(),
                                        }),
                                        bits: 1
                                    },
                                ],
                                name: "port".to_string()
                            },
                            bits: 8,
                        },
                        BitfieldEntry {
                            field: BitfieldEntryKind::StructArray {
                                meta: FieldMeta::default(),
                                fields: vec![
                                    BitfieldEntry {
                                        field: BitfieldEntryKind::Scalar(BitfieldScalar::Simple {
                                            dtype: Type::U8,
                                            name: "num".into(),
                                            meta: FieldMeta::default(),
                                        }),
                                        bits: 7
                                    },
                                    BitfieldEntry {
                                        field: BitfieldEntryKind::Scalar(BitfieldScalar::Simple {
                                            dtype: Type::Bool,
                                            name: "pullup".into(),
                                            meta: FieldMeta::default(),
                                        }),
                                        bits: 1
                                    },
                                ],
                                name: "ports".to_string(),
                                count: 6,
                            },
                            bits: 48,
                        }
                    ],
                    meta: Default::default(),
                }
            )),
            bitmap_field()(
                r#"
                bits {
                    u8 aaa : 4
                    u8 _ : 4
                    u32 =15 : 7
                    _ : 16 // alternate simpler padding syntax
                    struct port {
                        u8 num : 7
                        bool pullup
                    }
                    struct ports[6] {
                        u8 num : 7
                        bool pullup
                    }
                }
                "#
            )
        );

        // bool with implicit size
        assert_eq!(
            Ok((
                "",
                Field::Bits {
                    name: None,
                    items: vec![
                        field::BitfieldEntry {
                            field: field::BitfieldEntryKind::Scalar(
                                field::BitfieldScalar::Simple {
                                    dtype: Type::U8,
                                    name: "aaa".into(),
                                    meta: FieldMeta::default(),
                                }
                            ),
                            bits: 4
                        },
                        field::BitfieldEntry {
                            field: field::BitfieldEntryKind::Scalar(
                                field::BitfieldScalar::Simple {
                                    dtype: Type::Bool,
                                    name: "xxx".into(),
                                    meta: FieldMeta::default(),
                                }
                            ),
                            bits: 1
                        },
                    ],
                    meta: Default::default(),
                }
            )),
            bitmap_field()(
                r#"
                bits {
                    u8 aaa : 4
                    bool xxx
                }
                "#
            )
        );

        // test a named bitfield
        assert_eq!(
            Ok((
                "",
                Field::Bits {
                    name: Some("alalala".to_string()),
                    items: vec![
                        field::BitfieldEntry {
                            field: field::BitfieldEntryKind::Scalar(
                                field::BitfieldScalar::Simple {
                                    dtype: Type::U8,
                                    name: "aaa".into(),
                                    meta: FieldMeta::default(),
                                }
                            ),
                            bits: 4
                        },
                        field::BitfieldEntry {
                            field: field::BitfieldEntryKind::Scalar(
                                field::BitfieldScalar::Simple {
                                    dtype: Type::Bool,
                                    name: "xxx".into(),
                                    meta: FieldMeta::default(),
                                }
                            ),
                            bits: 1
                        },
                    ],
                    meta: Default::default(),
                }
            )),
            bitmap_field()(
                r#"
                bits alalala {
                    u8 aaa : 4
                    bool xxx
                }
                "#
            )
        );

        // bool with explicit size
        assert_eq!(
            Ok((
                "",
                Field::Bits {
                    name: None,
                    items: vec![
                        field::BitfieldEntry {
                            field: field::BitfieldEntryKind::Scalar(
                                field::BitfieldScalar::Simple {
                                    dtype: Type::U8,
                                    name: "aaa".into(),
                                    meta: FieldMeta::default(),
                                }
                            ),
                            bits: 4
                        },
                        field::BitfieldEntry {
                            field: field::BitfieldEntryKind::Scalar(
                                field::BitfieldScalar::Simple {
                                    dtype: Type::Bool,
                                    name: "xxx".into(),
                                    meta: FieldMeta::default(),
                                }
                            ),
                            bits: 4
                        },
                    ],
                    meta: Default::default(),
                }
            )),
            bitmap_field()(
                r#"
                bits {
                    u8 aaa : 4
                    bool xxx : 4
                }
                "#
            )
        );

        // bool with implicit size at a internal position
        assert_eq!(
            Ok((
                "",
                Field::Bits {
                    name: None,
                    items: vec![
                        field::BitfieldEntry {
                            field: field::BitfieldEntryKind::Scalar(
                                field::BitfieldScalar::Simple {
                                    dtype: Type::U8,
                                    name: "aaa".into(),
                                    meta: FieldMeta::default(),
                                }
                            ),
                            bits: 4
                        },
                        field::BitfieldEntry {
                            field: field::BitfieldEntryKind::Scalar(
                                field::BitfieldScalar::Simple {
                                    dtype: Type::Bool,
                                    name: "xxx".into(),
                                    meta: FieldMeta::default(),
                                }
                            ),
                            bits: 1
                        },
                        field::BitfieldEntry {
                            field: field::BitfieldEntryKind::Scalar(
                                field::BitfieldScalar::Simple {
                                    dtype: Type::U8,
                                    name: "bbb".into(),
                                    meta: FieldMeta::default(),
                                }
                            ),
                            bits: 3
                        },
                    ],
                    meta: Default::default(),
                }
            )),
            bitmap_field()(
                r#"
                bits {
                    u8 aaa : 4
                    bool xxx
                    u8 bbb : 3
                }
                "#
            )
        );

        // bool with implicit size spiked with comments
        assert_eq!(
            Ok((
                "",
                Field::Bits {
                    name: None,
                    items: vec![
                        field::BitfieldEntry {
                            field: field::BitfieldEntryKind::Scalar(
                                field::BitfieldScalar::Simple {
                                    dtype: Type::U8,
                                    name: "aaa".into(),
                                    meta: FieldMeta::default(),
                                }
                            ),
                            bits: 4
                        },
                        field::BitfieldEntry {
                            field: field::BitfieldEntryKind::Scalar(
                                field::BitfieldScalar::Simple {
                                    dtype: Type::Bool,
                                    name: "xxx".into(),
                                    meta: FieldMeta::default(),
                                }
                            ),
                            bits: 1
                        },
                        field::BitfieldEntry {
                            field: field::BitfieldEntryKind::Scalar(
                                field::BitfieldScalar::Simple {
                                    dtype: Type::U8,
                                    name: "bbb".into(),
                                    meta: FieldMeta::default(),
                                }
                            ),
                            bits: 3
                        },
                    ],
                    meta: Default::default(),
                }
            )),
            bitmap_field()(
                r#"
                bits {
                    u8 aaa : 4
                    bool/* aaa */xxx// comment
                    u8 bbb : 3
                }
                "#
            )
        );
    }
}
