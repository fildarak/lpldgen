//! Arithmetic expressions with simple equation solving
#![allow(unused)]

pub mod ast;
pub mod shyard;
pub mod tokenize;

use crate::errors::Error;
use shyard::{infix_to_postfix, postfix_to_ast};
use tokenize::{tokenize, unary_minus_fix};

pub use ast::Node;

pub fn infix_to_ast(infix: &str) -> Result<ast::Node, Error> {
    let infix = unary_minus_fix(tokenize(infix)?)?;
    let postfix = infix_to_postfix(infix)?;
    postfix_to_ast(postfix)
}
