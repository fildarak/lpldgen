//! AST for arithmetic expressions, including some manipulation methods

use std::fmt::{self, Display, Formatter, Write};

use Node::*;

use crate::errors::Error;

/// Arithmetic node
#[derive(Debug, PartialEq, Clone)]
pub enum Node {
    /// Numeric constant
    Literal(f64),
    /// Named variable
    Variable(String),
    /// Add two nodes (A + B)
    Add(Box<Node>, Box<Node>),
    /// Subtract two nodes (A - B)
    Sub(Box<Node>, Box<Node>),
    /// Multiply two nodes (A * B)
    Mul(Box<Node>, Box<Node>),
    /// Divide two nodes (A / B)
    Div(Box<Node>, Box<Node>),
    /// Modulo (A % B)
    Mod(Box<Node>, Box<Node>),
    /// Round function
    Round(Box<Node>),
    /// Ceil function
    Ceil(Box<Node>),
    /// Floor function
    Floor(Box<Node>),
    /// sin()
    Sin(Box<Node>),
    /// asin()
    Asin(Box<Node>),
    /// sin()
    Cos(Box<Node>),
    /// asin()
    Acos(Box<Node>),
    /// tan()
    Tan(Box<Node>),
    /// atan()
    Atan(Box<Node>),
}

impl Node {
    /// Reverse this node
    ///
    /// This is one step in simple equation solving
    ///
    /// The original left-hand side and the node we want to separate on the right hand side
    /// are given. Returns the new left- and right-hand side formulas.
    pub fn reverse(self, lhs: Node, want: &Node) -> Result<(Node, Node), Error<'static>> {
        Ok(match self {
            Literal(_) | Variable(_) => (Literal(0f64), Add(Box::new(lhs), Box::new(self))),

            Add(mut a, mut b) => {
                if b.contains(want) && !a.contains(want) {
                    std::mem::swap(&mut a, &mut b);
                }

                (*a, Sub(Box::new(lhs), b))
            }
            Sub(a, b) => {
                if b.contains(want) && !a.contains(want) {
                    (*b, Sub(a, Box::new(lhs)))
                } else {
                    (*a, Add(Box::new(lhs), b))
                }
            }
            Mul(mut a, mut b) => {
                if b.contains(want) && !a.contains(want) {
                    std::mem::swap(&mut a, &mut b);
                }

                (*a, Div(Box::new(lhs), b))
            }
            Div(a, b) => {
                if b.contains(want) && !a.contains(want) {
                    (*b, Div(a, Box::new(lhs)))
                } else {
                    (*a, Mul(Box::new(lhs), b))
                }
            }
            Mod(_a, _b) => {
                return Err(Error::from("Cannot reverse modulo"));
            }
            Round(a) | Ceil(a) | Floor(a) => (*a, lhs),
            Sin(a) => (*a, Asin(Box::new(lhs))),
            Asin(a) => (*a, Sin(Box::new(lhs))),
            Cos(a) => (*a, Acos(Box::new(lhs))),
            Acos(a) => (*a, Cos(Box::new(lhs))),
            Tan(a) => (*a, Atan(Box::new(lhs))),
            Atan(a) => (*a, Tan(Box::new(lhs))),
        })
    }

    /// Check if a variable or the other node given equals this node, or is contained
    /// in its sub-expression.
    pub fn contains(&self, other: &Node) -> bool {
        match self {
            Literal(_) | Variable(_) => self == other,
            Add(a, b) | Sub(a, b) | Mul(a, b) | Div(a, b) | Mod(a, b) => {
                a.contains(other) || b.contains(other)
            }
            Round(a) | Ceil(a) | Floor(a) | Sin(a) | Asin(a) | Cos(a) | Acos(a) | Tan(a)
            | Atan(a) => a.contains(other),
        }
    }

    pub fn substitute_var(&mut self, from: &str, to: &str) {
        match self {
            Literal(_) => {}
            Variable(s) => {
                if s == from {
                    s.clear();
                    s.push_str(to);
                }
            }
            Add(a, b) | Sub(a, b) | Mul(a, b) | Div(a, b) | Mod(a, b) => {
                a.substitute_var(from, to);
                b.substitute_var(from, to);
            }
            Round(a) | Ceil(a) | Floor(a) | Sin(a) | Asin(a) | Cos(a) | Acos(a) | Tan(a)
            | Atan(a) => {
                a.substitute_var(from, to);
            }
        }
    }

    /// Extract a variable or other node.
    ///
    /// The extracted variable and the left-hand side nodes are given as arguments
    ///
    /// lhs=formula(variable)
    ///
    /// the result is the new left-hand side after the right hand side became the variable alone.
    ///
    /// This will not work for formulas that lead to polynomials, square roots etc.
    pub fn extract(self, want: &Node, mut lhs: Node) -> Result<Node, Error<'static>> {
        let mut rhs = self;
        let mut iters: u32 = 0;

        let as_str = format!(
            "y = {}, solving for {}",
            rhs.render_c(FloatPrecision::Debug, false),
            want.render_c(FloatPrecision::Debug, false)
        );

        loop {
            if &rhs == want {
                if lhs.contains(want) {
                    return Err(Error::from(format!(
                        "Failed to reverse a transform formula: {}",
                        as_str
                    )));
                }
                return Ok(lhs);
            }

            let (a, b) = rhs.reverse(lhs, want)?;
            lhs = b;
            rhs = a;

            iters += 1;

            if iters > 100 {
                return Err(Error::from(format!(
                    "Extract too deep in transform formula: {}",
                    as_str
                )));
            }
        }
    }

    pub fn render_c(&self, floats: FloatPrecision, round_outer: bool) -> String {
        format!(
            "{}",
            Fmt(|fmt| { self.render_c_fmt(fmt, floats, round_outer) })
        )
    }

    pub fn render_c_fmt(
        &self,
        fmt: &mut Formatter<'_>,
        floats: FloatPrecision,
        round_outer: bool,
    ) -> fmt::Result {
        let add_round = if round_outer {
            match self {
                Round(_) | Ceil(_) | Floor(_) => false,
                _ => {
                    if floats == FloatPrecision::Float {
                        write!(fmt, "roundf(")?;
                    } else {
                        write!(fmt, "round(")?;
                    }
                    true
                }
            }
        } else {
            false
        };

        self.render_c_inner(fmt, floats)?;

        if add_round {
            fmt.write_char(')')?;
        }

        Ok(())
    }

    fn render_c_inner(&self, fmt: &mut Formatter<'_>, floats: FloatPrecision) -> fmt::Result {
        match self {
            Literal(v) => {
                write!(fmt, "{}", v)?;
                if (*v - v.round()).abs() < 0.000_001 {
                    fmt.write_str(".0")?;
                }

                if floats == FloatPrecision::Float {
                    fmt.write_char('f')?;
                }
            }
            Variable(s) => {
                // XXX ugly hack to detect cast embedded in the variable
                if !s.starts_with('(') {
                    match floats {
                        FloatPrecision::Float => fmt.write_str("(float)")?,
                        FloatPrecision::Double => fmt.write_str("(double)")?,
                        FloatPrecision::Debug => {}
                    };
                }

                write!(fmt, "{}", s)?;
            }
            Add(a, b) => {
                fmt.write_char('(')?;
                a.render_c_inner(fmt, floats)?;
                fmt.write_char('+')?;
                b.render_c_inner(fmt, floats)?;
                fmt.write_char(')')?;
            }
            Sub(a, b) => {
                fmt.write_char('(')?;
                a.render_c_inner(fmt, floats)?;
                fmt.write_char('-')?;
                b.render_c_inner(fmt, floats)?;
                fmt.write_char(')')?;
            }
            Mul(a, b) => {
                fmt.write_char('(')?;
                a.render_c_inner(fmt, floats)?;
                fmt.write_char('*')?;
                b.render_c_inner(fmt, floats)?;
                fmt.write_char(')')?;
            }
            Div(a, b) => {
                fmt.write_char('(')?;
                a.render_c_inner(fmt, floats)?;
                fmt.write_char('/')?;
                b.render_c_inner(fmt, floats)?;
                fmt.write_char(')')?;
            }
            Mod(a, b) => {
                fmt.write_char('(')?;
                a.render_c_inner(fmt, floats)?;
                fmt.write_char('%')?;
                b.render_c_inner(fmt, floats)?;
                fmt.write_char(')')?;
            }
            Round(a) => {
                if floats == FloatPrecision::Float {
                    fmt.write_str("roundf(")?;
                } else {
                    fmt.write_str("round(")?;
                }
                a.render_c_inner(fmt, floats)?;
                fmt.write_char(')')?;
            }
            Ceil(a) => {
                if floats == FloatPrecision::Float {
                    fmt.write_str("ceilf(")?;
                } else {
                    fmt.write_str("ceil(")?;
                }
                a.render_c_inner(fmt, floats)?;
                fmt.write_char(')')?;
            }
            Floor(a) => {
                if floats == FloatPrecision::Float {
                    fmt.write_str("floorf(")?;
                } else {
                    fmt.write_str("floor(")?;
                }
                a.render_c_inner(fmt, floats)?;
                fmt.write_char(')')?;
            }
            Sin(a) => {
                if floats == FloatPrecision::Float {
                    fmt.write_str("sinf(")?;
                } else {
                    fmt.write_str("sin(")?;
                }
                a.render_c_inner(fmt, floats)?;
                fmt.write_char(')')?;
            }
            Asin(a) => {
                if floats == FloatPrecision::Float {
                    fmt.write_str("asinf(")?;
                } else {
                    fmt.write_str("asin(")?;
                }
                a.render_c_inner(fmt, floats)?;
                fmt.write_char(')')?;
            }
            Cos(a) => {
                if floats == FloatPrecision::Float {
                    fmt.write_str("cosf(")?;
                } else {
                    fmt.write_str("cos(")?;
                }
                a.render_c_inner(fmt, floats)?;
                fmt.write_char(')')?;
            }
            Acos(a) => {
                if floats == FloatPrecision::Float {
                    fmt.write_str("acosf(")?;
                } else {
                    fmt.write_str("acos(")?;
                }
                a.render_c_inner(fmt, floats)?;
                fmt.write_char(')')?;
            }
            Tan(a) => {
                if floats == FloatPrecision::Float {
                    fmt.write_str("tanf(")?;
                } else {
                    fmt.write_str("tan(")?;
                }
                a.render_c_inner(fmt, floats)?;
                fmt.write_char(')')?;
            }
            Atan(a) => {
                if floats == FloatPrecision::Float {
                    fmt.write_str("atanf(")?;
                } else {
                    fmt.write_str("atan(")?;
                }
                a.render_c_inner(fmt, floats)?;
                fmt.write_char(')')?;
            }
        }

        Ok(())
    }
}

// this is a hack to allow obtaining formatter
struct Fmt<T>(T)
where
    T: Fn(&mut Formatter<'_>) -> fmt::Result;

impl<T> Display for Fmt<T>
where
    T: Fn(&mut Formatter<'_>) -> fmt::Result,
{
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        self.0(f)
    }
}

#[derive(Debug, Eq, PartialEq, Clone, Copy, Hash)]
pub enum FloatPrecision {
    Float,
    Double,
    /// Same as double, but without the casts
    Debug,
}

impl Display for Node {
    fn fmt(&self, fmt: &mut Formatter) -> fmt::Result {
        fmt.write_str(&self.render_c(FloatPrecision::Double, false))
    }
}

#[cfg(test)]
mod tests {
    use crate::parser::expr::ast::FloatPrecision;

    use super::Node::*;

    #[test]
    fn test_reverse() {
        let want = Variable("f".into());

        let left = Variable("u".into());
        let right = Round(Box::new(Mul(
            Box::new(Add(
                Box::new(Variable("f".into())),
                Box::new(Literal(30f64)),
            )),
            Box::new(Literal(100f64)),
        )));

        let (right, left) = right.reverse(left, &want).unwrap();

        assert_eq!(Variable("u".into()), left);
        assert_eq!(
            Mul(
                Box::new(Add(
                    Box::new(Variable("f".into())),
                    Box::new(Literal(30f64)),
                )),
                Box::new(Literal(100f64)),
            ),
            right
        );

        let (right, left) = right.reverse(left, &want).unwrap();

        assert_eq!(
            Div(Box::new(Variable("u".into())), Box::new(Literal(100f64))),
            left
        );
        assert_eq!(
            Add(Box::new(Variable("f".into())), Box::new(Literal(30f64))),
            right
        );

        let (right, left) = right.reverse(left, &want).unwrap();

        assert_eq!(
            Sub(
                Box::new(Div(
                    Box::new(Variable("u".into())),
                    Box::new(Literal(100f64)),
                )),
                Box::new(Literal(30f64)),
            ),
            left
        );
        assert_eq!(Variable("f".into()), right);
    }

    #[test]
    fn test_to_str() {
        let right = Round(Box::new(Mul(
            Box::new(Add(
                Box::new(Variable("f".into())),
                Box::new(Literal(30f64)),
            )),
            Box::new(Literal(100f64)),
        )));

        assert_eq!(
            "round((((double)f+30.0)*100.0))".to_string(),
            format!("{}", right)
        );

        // "alternate" format produces more c-like syntax
        assert_eq!(
            "round((((double)f+30.0)*100.0))".to_string(),
            format!("{:#}", right)
        );

        assert_eq!("1.234".to_string(), format!("{:#}", Literal(1.234)));
    }

    #[test]
    fn test_reverse_by_wanted() {
        let want = Variable("f".into());

        let left = Variable("u".into());
        let right = Mul(
            Box::new(Literal(100f64)),
            Box::new(Add(
                Box::new(Literal(30f64)),
                Box::new(Variable("f".into())),
            )),
        );

        assert_eq!(
            "(double)u=(100.0*(30.0+(double)f))",
            format!("{}={}", left, right)
        );
        let (right, left) = right.reverse(left, &want).unwrap();
        assert_eq!(
            "((double)u/100.0)=(30.0+(double)f)",
            format!("{}={}", left, right)
        );
        let (right, left) = right.reverse(left, &want).unwrap();
        assert_eq!(
            "(((double)u/100.0)-30.0)=(double)f",
            format!("{}={}", left, right)
        );
    }

    #[test]
    fn test_reverse_by_wanted_div() {
        let want = Variable("f".into());

        let left = Variable("u".into());
        let right = Div(Box::new(Literal(100f64)), Box::new(Variable("f".into())));

        assert_eq!("(double)u=(100.0/(double)f)", format!("{}={}", left, right));
        let (right, left) = right.reverse(left, &want).unwrap();
        assert_eq!("(100.0/(double)u)=(double)f", format!("{}={}", left, right));
    }

    #[test]
    fn test_reverse_by_wanted_sub() {
        let want = Variable("f".into());

        let left = Variable("u".into());
        let right = Sub(Box::new(Literal(100f64)), Box::new(Variable("f".into())));

        assert_eq!("(double)u=(100.0-(double)f)", format!("{}={}", left, right));
        let (right, left) = right.reverse(left, &want).unwrap();
        assert_eq!("(100.0-(double)u)=(double)f", format!("{}={}", left, right));
    }

    #[test]
    fn test_ohms_law() {
        let want = Variable("r".into());

        let left = Variable("i".into());
        let right = Div(
            Box::new(Variable("u".into())),
            Box::new(Variable("r".into())),
        );

        assert_eq!(
            "(double)i=((double)u/(double)r)",
            format!("{}={}", left, right)
        );
        let (right, left) = right.reverse(left, &want).unwrap();
        assert_eq!(
            "((double)u/(double)i)=(double)r",
            format!("{}={}", left, right)
        );
    }

    #[test]
    fn test_extract() {
        let want = Variable("r".into());

        let left = Variable("i".into());
        let right = Div(
            Box::new(Variable("u".into())),
            Box::new(Variable("r".into())),
        );

        let result = right.extract(&want, left).unwrap();
        assert_eq!("((double)u/(double)i)", format!("{}", result));
    }

    #[test]
    fn test_extract2() {
        let want = Variable("f".into());

        let left = Variable("u".into());
        let right = Mul(
            Box::new(Literal(100f64)),
            Box::new(Add(
                Box::new(Literal(30f64)),
                Box::new(Variable("f".into())),
            )),
        );
        assert_eq!("(100.0*(30.0+(double)f))", format!("{}", right));

        let result = right.extract(&want, left).unwrap();
        assert_eq!("(((double)u/100.0)-30.0)", format!("{}", result));

        let left = want;
        let want = Variable("u".into());

        let result = result.extract(&want, left).unwrap();
        assert_eq!("(((double)f+30.0)*100.0)", format!("{}", result));

        // and a round trip to check the orders remain unchanged
        let left = want;
        let want = Variable("f".into());

        let result = result.extract(&want, left).unwrap();
        assert_eq!("(((double)u/100.0)-30.0)", format!("{}", result));
    }

    #[test]
    fn test_deep_extract() {
        let want = Variable("b".into());

        let left = Variable("a".into());
        let right = Div(
            Box::new(Variable("x".into())),
            Box::new(Div(
                Box::new(Variable("y".into())),
                Box::new(Div(
                    Box::new(Variable("z".into())),
                    Box::new(Variable("b".into())),
                )),
            )),
        );

        let result = right.extract(&want, left).unwrap();
        //        assert_eq!("(z/(y/(x/a)))", result.render_c(FloatPrecision::Double, false));
        assert_eq!(
            "((double)z/((double)y/((double)x/(double)a)))",
            result.to_string()
        );
    }
}
