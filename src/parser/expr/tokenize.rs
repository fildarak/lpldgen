//! Tokenization and basic parsing of infix expressions

use std::fmt::{self, Display};

use nom::branch::alt;
use nom::bytes::complete::tag;
use nom::combinator::{map, peek, value};
use nom::IResult;
use nom::multi::many1;
use nom::sequence::terminated;

use Op::*;
use Token::*;

use crate::errors::Error;
use crate::errors::SpecParseError;
use crate::parser::utils::{f64_literal, identifier, space_around0};

/// Operator kind
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum Op {
    LParen,
    RParen,
    Add,
    Sub,
    Mul,
    Div,
    Mod,
    Comma, // comma is used in functions to separate arguments
}

/// Operator associativity (used for infix parsing)
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum Associativity {
    Left,
    Right,
}

impl Op {
    /// Get operator precedence, higher value is higher precedence
    pub fn precedence(self) -> u32 {
        match self {
            Mul | Div => 10,
            Add | Sub => 5,
            _ => 0,
        }
    }

    /// Get operator associativity
    pub fn associativity(self) -> Associativity {
        match self {
            Add | Sub => Associativity::Left,
            Mul | Div => Associativity::Left,
            _ => Associativity::Left,
        }
    }
}

impl Display for Op {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        match self {
            LParen => fmt.write_str("("),
            RParen => fmt.write_str(")"),
            Add => fmt.write_str("+"),
            Sub => fmt.write_str("-"),
            Mul => fmt.write_str("*"),
            Div => fmt.write_str("/"),
            Mod => fmt.write_str("%"),
            Comma => fmt.write_str(","),
        }
    }
}

/// Token parsed from a string
#[derive(Debug, Clone, PartialEq)]
pub enum Token {
    /// Varible
    Variable(String),
    /// Literal value; all values are internally converted to double
    Literal(f64),
    /// Operator
    Operator(Op),
    /// Function; variables are not recognizable from functions, so their parsing is based
    /// on a function whitelist.
    ///
    /// TODO better function parsing could look for the following opening parenthesis
    Function(String),
}

impl Display for Token {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Variable(s) => fmt.write_str(s),
            Literal(f) => write!(fmt, "{}", f),
            Operator(op) => op.fmt(fmt),
            Function(s) => fmt.write_str(s),
        }
    }
}

/// Convert a string to a token stream
pub fn tokenize(input: &str) -> Result<Vec<Token>, SpecParseError> {
    let r: IResult<&str, Vec<Token>, SpecParseError> = many1(space_around0(alt((
        value(Operator(Add), tag("+")),
        value(Operator(Sub), tag("-")),
        value(Operator(Mul), tag("*")),
        value(Operator(Div), tag("/")),
        value(Operator(Mod), tag("%")),
        value(Operator(Comma), tag(",")),
        value(Operator(LParen), tag("(")),
        value(Operator(RParen), tag(")")),
        map(
            terminated(identifier(), peek(space_around0(tag("(")))),
            |n| Function(n.to_string()),
        ),
        map(identifier(), |n| Variable(n.to_string())),
        #[allow(clippy::redundant_closure)] // wtf
            map(f64_literal(), |n| Literal(n)),
    ))))(input);

    // turn nom error into a normal error
    match r {
        Ok((_i, v)) => Ok(v),
        Err(nom::Err::Error(e)) | Err(nom::Err::Failure(e)) => Err(e),
        _ => panic!(),
    }
}

pub fn token_vec_display(vec: &[Token]) -> String {
    let v: Vec<String> = vec.iter().map(ToString::to_string).collect();

    v.join(" ").trim().to_string()
}

#[test]
fn test_vec_display() {
    assert_eq!(
        "1 + 1",
        &token_vec_display(&[Literal(1f64), Operator(Add), Literal(1f64)])
    );
    assert_eq!(
        "( 1 + -1 )",
        &token_vec_display(&[
            Operator(LParen),
            Literal(1f64),
            Operator(Op::Add),
            Literal(-1f64),
            Operator(RParen)
        ])
    );
}

#[test]
fn test_tokenize() {
    assert_eq!(
        Ok(vec![Literal(1f64), Operator(Op::Add), Literal(1f64)]),
        tokenize("1+1")
    );
    assert_eq!(
        Ok(vec![
            Operator(Op::LParen),
            Literal(12.34f64),
            Operator(Op::Add),
            Variable("abc".to_string()),
            Operator(Op::Mul),
            Variable("abc_def123".to_string()),
            Operator(Op::Div),
            Operator(Op::LParen),
            Literal(0f64),
            Operator(Op::RParen),
            Operator(Op::RParen),
            Operator(Op::Sub),
            Literal(123f64),
            Operator(Op::Sub),
            Operator(Op::Sub),
            Literal(456f64),
            Operator(Op::Add),
            Operator(Op::LParen),
            Operator(Op::Sub),
            Literal(62f64),
            Operator(Op::RParen),
            Operator(Op::Add),
            Function("foo".into()),
            Operator(Op::LParen),
            Variable("bar".to_string()),
            Operator(Op::RParen),
        ]),
        tokenize("(12.34+abc*abc_def123 / (0))-123--456+(-62)+foo(bar)")
    );
}

pub fn unary_minus_fix<'a>(vec: Vec<Token>) -> Result<Vec<Token>, Error<'a>> {
    #[derive(PartialEq)]
    enum TokState {
        Start,
        Leaf,
        Op,
        OpSub,
        StartSub,
        Function,
    }

    let mut cleaned = vec![];
    let mut state = TokState::Start;

    for item in vec.into_iter() {
        match state {
            TokState::Start => match item {
                Operator(Sub) => {
                    state = TokState::StartSub;
                }
                Operator(LParen) => {
                    state = TokState::Start;
                    cleaned.push(item);
                }
                Operator(_) => {
                    state = TokState::Op;
                    cleaned.push(item);
                }
                Function(_) => {
                    state = TokState::Function;
                    cleaned.push(item);
                }
                _ => {
                    state = TokState::Leaf;
                    cleaned.push(item);
                }
            },
            TokState::Leaf => match item {
                Operator(_) => {
                    state = TokState::Op;
                    cleaned.push(item);
                }
                _ => {
                    return Err(Error::from(format!("Leaf expected op, got {}", item)));
                }
            },
            TokState::Function => match item {
                Operator(LParen) => {
                    state = TokState::Start;
                    cleaned.push(item);
                }
                _ => {
                    return Err(Error::from(format!("Func expected lparen, got {}", item)));
                }
            },
            TokState::Op => match item {
                Operator(Sub) => {
                    state = TokState::OpSub;
                }
                Operator(LParen) => {
                    state = TokState::Start;
                    cleaned.push(item);
                }
                Operator(_) => {
                    state = TokState::Op;
                    cleaned.push(item);
                }
                Function(_) => {
                    state = TokState::Function;
                    cleaned.push(item);
                }
                _ => {
                    state = TokState::Leaf;
                    cleaned.push(item);
                }
            },
            TokState::OpSub => match item {
                Operator(Sub) => {
                    cleaned.push(Operator(Add));
                    state = TokState::Start;
                }
                Operator(LParen) => {
                    cleaned.push(item);
                    state = TokState::Start;
                }
                Operator(_) => {
                    return Err(Error::from(format!("Op- followed by unexpected {}", item)));
                }
                Function(_) => {
                    state = TokState::Function;
                    cleaned.push(item);
                }
                Literal(x) => {
                    state = TokState::Leaf;
                    cleaned.push(Literal(-x));
                }
                Variable(_) => {
                    state = TokState::Leaf;
                    cleaned.push(Literal(0f64));
                    cleaned.push(Operator(Sub));
                    cleaned.push(item);
                }
            },
            TokState::StartSub => match item {
                Operator(Sub) => {
                    state = TokState::Start;
                }
                Operator(LParen) => {
                    state = TokState::Start;
                    cleaned.push(Literal(0f64));
                    cleaned.push(Operator(Sub));
                    cleaned.push(item);
                }
                Literal(x) => {
                    state = TokState::Leaf;
                    cleaned.push(Literal(-x));
                }
                Variable(_) | Function(_) => {
                    state = TokState::Leaf;
                    cleaned.push(Literal(0f64));
                    cleaned.push(Operator(Sub));
                    cleaned.push(item);
                }
                Operator(_) => {
                    return Err(Error::from(format!("Unexpected {}", item)));
                }
            },
        }
    }

    Ok(cleaned)
}

#[test]
fn test_cleanup_tokenize() {
    assert_eq!(
        vec![Literal(1f64), Operator(Op::Add), Literal(1f64)],
        unary_minus_fix(vec![Literal(1f64), Operator(Op::Add), Literal(1f64)]).unwrap()
    );

    assert_eq!(
        "1 + 1",
        token_vec_display(&unary_minus_fix(tokenize("1+1").unwrap()).unwrap())
    );

    assert_eq!(
        "-1 + 1",
        token_vec_display(&unary_minus_fix(tokenize("-1+1").unwrap()).unwrap())
    );

    assert_eq!(
        "1",
        token_vec_display(&unary_minus_fix(tokenize("--1").unwrap()).unwrap())
    );

    assert_eq!(
        "2 - -1",
        token_vec_display(&unary_minus_fix(tokenize("2--1").unwrap()).unwrap())
    );

    assert_eq!(
        "1 - 1",
        token_vec_display(&unary_minus_fix(tokenize("1 - 1").unwrap()).unwrap())
    );

    assert_eq!(
        "-1",
        token_vec_display(&unary_minus_fix(tokenize("- 1").unwrap()).unwrap())
    );

    assert_eq!(
        "0 - a", // unary minus cant be used with variables, turn it into binary minus
        token_vec_display(&unary_minus_fix(tokenize("-a").unwrap()).unwrap())
    );

    assert_eq!(
        "( -1 )",
        token_vec_display(&unary_minus_fix(tokenize("(-1)").unwrap()).unwrap())
    );

    assert_eq!(
        "( 0 - a )",
        token_vec_display(&unary_minus_fix(tokenize("(-a)").unwrap()).unwrap())
    );

    assert_eq!(
        "round ( 0 - a )",
        token_vec_display(&unary_minus_fix(tokenize("round(-a)").unwrap()).unwrap())
    );

    assert_eq!(
        "( -30 + ( t + 30 ) ) * 100",
        token_vec_display(&unary_minus_fix(tokenize("(-30+(t+30))*100").unwrap()).unwrap())
    );

    assert_eq!(
        "floor ( 1 , 2 , 3 , 4 ) / 5", // this doesnt make sense, but we dont check arity at this level, so it's ok as a test for the comma operator
        token_vec_display(&unary_minus_fix(tokenize("floor(1,2,3,4)/5").unwrap()).unwrap())
    );

    let infix = unary_minus_fix(tokenize("3 + 4 * 2 / ( 1 - 5 )").unwrap()).unwrap();
    assert_eq!("3 + 4 * 2 / ( 1 - 5 )", token_vec_display(&infix));
}
