# Changelog

## 1.4.4
- Added option to generate single header/source file without common files (payloads_internal etc.) - used for services.

## 1.4.3
- Fixed memory leak in realloc_clear

## 1.4.2
- `+fmt` now transforms `%d,%i,%o,%x,%X` (even in more complex form, such as `0x%08x`) 
  to platform-independent format using print macros from `<inttypes.h>`.

## 1.4.1
- fix wrong indentation in growable struct parser code

## 1.4.0
- added templates for payload re-use

## 1.3.0

- Add field attribute `+fmt`, allowing to convert the value to string when exporting 
  to JSON. Syntax: `+fmt %02x` or quoted `+fmt "hex value is 0x%04x"`. Only one numeric
  substitution can be used (it can be float/double).
- Changed JSON export to use strings literal references instead of `strdup`-ing everything.
- Add simple trig transforms

## 1.2.0

- Improve parser generation to avoid GCC warnings about unused values
- Remove git hash from the generated file headers, version is sufficient
- Constant fields in a payload can be annotated with `+named TAG` ("TAG" is the name)
  to generate a corresponding constant in the output file (MODULE_PAYLOAD_TAG)

## 1.1.3

- Updated dependencies to fix build with rust 1.48
- Fix false positives in duplicate payload detection

## 1.1.2
- Add new ASCII byte constant syntax: `='A'`
