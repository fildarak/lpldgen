#include "../test.h"

bool test_u16(void) {
    uint8_t *packed;
    size_t len;
    pld_error_t ret;

    /* Source data */
    struct TestPld_test_u16 pld = {
        .u16b = 0xCAFE,
        .u16l = 0xBABE,
    };

    /* Pack */
    ret = TestPld_test_u16_build(&pld, malloc, &packed, &len);
    check(ret == PLD_OK);
    check_eq_ul(len, 4ul);

    /* Check packed format */
    const uint8_t expected[] = {
        // big
        0xCA, 0xFE,
        // little
        0xBE, 0xBA
    };
    check_array(packed, expected);

    /* Unpack */
    struct TestPld_test_u16 *pld2 = NULL;
    ret = TestPld_test_u16_parse(packed, len, malloc, &pld2);
    check(ret == PLD_OK);
    check(pld2 != NULL);

    /* Compare with original */
    check_eq(pld2->u16b, pld.u16b);
    check_eq(pld2->u16l, pld.u16l);

    /* Clean up */
    free(pld2);
    free(packed);
    return true;
}
