#include "../test.h"

bool test_f64(void) {
    uint8_t *packed;
    size_t len;
    pld_error_t ret;

    /* Source data */
    struct TestPld_test_f64 pld = {
        .f64b = 0.1, // 0x3fb9_9999_9999_999a
        .f64l = 0.1,
    };

    /* Pack */
    ret = TestPld_test_f64_build(&pld, malloc, &packed, &len);
    check(ret == PLD_OK);
    check_eq_ul(len, 16ul);

    /* Check packed format */
    const uint8_t expected[] = {
        // big
        0x3f, 0xb9, 0x99, 0x99, 0x99, 0x99, 0x99, 0x9a,
        // little
        0x9a, 0x99, 0x99, 0x99, 0x99, 0x99, 0xb9, 0x3f
    };
    check_array(packed, expected);

    /* Unpack */
    struct TestPld_test_f64 *pld2 = NULL;
    ret = TestPld_test_f64_parse(packed, len, malloc, &pld2);
    check(ret == PLD_OK);
    check(pld2 != NULL);

    /* Compare with original */
    check_eq(pld2->f64b, pld.f64b);
    check_eq(pld2->f64l, pld.f64l);

    /* Clean up */
    free(pld2);
    free(packed);
    return true;
}
