#include "../test.h"

// this is an example of chained modules
static const pld_module_t * modules[] = PLD_MODULES(&Payloads_TestPld, &Payloads_TestPld2);

bool test_recog_const_first(void)
{
    uint8_t *packed;
    size_t len;
    pld_error_t ret;
    void *vpld = NULL;
    uint32_t kind;

    /* r_const_a, r_const_c */
    {
        /* Source data */
        struct TestPld2_r_const_a pld = {.val = 123};

        /* Pack */
        ret = TestPld2_r_const_a_build(&pld, malloc, &packed, &len);
        check_eq(ret, PLD_OK);

        /* Parse as generic payload */
        ret = pld_parse(modules, (struct pld_address) {8, 15, 30, 30}, packed, len, malloc, &vpld, &kind);
        check_eq(ret, PLD_OK);

        /* Compare with original */
        check_eq(kind, TESTPLD2_R_CONST_A_ID);
        struct TestPld2_r_const_a *pld2 = vpld;

        check_eq(pld2->val, pld.val);

        /* Clean up */
        free(pld2);

        // try with port 16 - that is r_const_c
        ret = pld_recognize(modules, (struct pld_address) {8, 16, 30, 30}, packed, len, &kind);
        check_eq(ret, PLD_OK);
        check_eq(kind, TESTPLD2_R_CONST_C_ID);

        // try with 'to' - that is r_const_c2
        ret = pld_recognize(modules, (struct pld_address) {30, 30, 8, 15}, packed, len, &kind);
        check_eq(ret, PLD_OK);
        check_eq(kind, TESTPLD2_R_CONST_C2_ID);

        // try with addr 7 - r_const_c3
        ret = pld_recognize(modules, (struct pld_address) {7, 15, 30, 30}, packed, len, &kind);
        check_eq(ret, PLD_OK);
        check_eq(kind, TESTPLD2_R_CONST_C3_ID);

        free(packed);
    }

    /* r_const_b - differs only in the constant */
    {
        /* Source data */
        struct TestPld2_r_const_b pld = {.val = 123};

        /* Pack */
        ret = TestPld2_r_const_b_build(&pld, malloc, &packed, &len);
        check_eq(ret, PLD_OK);

        /* Parse as generic payload */
        ret = pld_parse(modules, (struct pld_address) {8, 15, 30, 30}, packed, len, malloc, &vpld, &kind);
        check_eq(ret, PLD_OK);

        /* Compare with original */
        check_eq(kind, TESTPLD2_R_CONST_B_ID);
        struct TestPld2_r_const_b *pld2 = vpld;

        check_eq(pld2->val, pld.val);

        /* Clean up */
        free(vpld);

        /* try changing the constant to parse as r_const_a */
        packed[0] = 0xFF;

        // using the recognize function manually to avoid needless parsing
        ret = pld_recognize(modules, (struct pld_address) {8, 15, 30, 30}, packed, len, &kind);
        check_eq(ret, PLD_OK);
        check_eq(kind, TESTPLD2_R_CONST_A_ID);

        // finish parsing (now without recognition - we already have 'kind')
        ret = pld_parse_as(modules, kind, packed, len, malloc, &vpld);
        check_eq(ret, PLD_OK);
        struct TestPld2_r_const_a *pld3 = vpld;
        check_eq(pld3->val, pld.val); // check that parsing worked

        free(packed);
    }

    return true;
}

bool test_recog_const_last(void)
{
    pld_error_t ret;
    uint32_t kind;

    // here testing with constant byte arrays for brevity
    const uint8_t pck[] = {15, 0xFF, 0xF0};
    ret = pld_recognize(modules, (struct pld_address) {8, 2, 30, 30}, pck, 3, &kind);
    check_eq(ret, PLD_OK);
    check_eq(kind, TESTPLD2_R_CONST_D_ID);

    const uint8_t pck3[] = {15, 0xF0, 0xFF};
    ret = pld_recognize(modules, (struct pld_address) {8, 2, 30, 30}, pck3, 3, &kind);
    check_eq(ret, PLD_OK);
    check_eq(kind, TESTPLD2_R_CONST_D_LITTLE_ID);

    const uint8_t pck2[] = {15, 0xFF, 0xF1};
    ret = pld_recognize(modules, (struct pld_address) {8, 2, 30, 30}, pck2, 3, &kind);
    check_eq(ret, PLD_OK);
    check_eq(kind, TESTPLD2_R_CONST_E_ID);

    return true;
}

bool test_recog_const_stru(void)
{
    pld_error_t ret;
    void *vpld = NULL;
    uint32_t kind;

    /* constant in struct */
    uint8_t pck[] = {127, 1, 66, 00, 55};
    ret = pld_recognize(modules, (struct pld_address) {8, 3, 30, 30}, pck, 5, &kind);
    check_eq(ret, PLD_OK);
    check_eq(kind, TESTPLD2_R_CONST_IN_STRUCT_ID);

    // here the constant is zero
    pck[1] = 0;
    ret = pld_parse(modules, (struct pld_address) {8, 3, 30, 30}, pck, 5, malloc, &vpld, &kind);
    check_eq(ret, PLD_OK);
    check_eq(kind, TESTPLD2_R_CONST_IN_STRUCT2_ID);
    struct TestPld2_r_const_in_struct2 *pld2 = vpld;

    // check it's parsed right
    check_eq(pld2->foo, 127);
    check_eq(pld2->inner.x, 66);
    check_eq(pld2->bar, 55);

    free(vpld);

    return true;
}

bool test_recog_const_multiple(void) {
    uint8_t *packed;
    size_t len;
    pld_error_t ret;
    void *vpld = NULL;
    uint32_t kind;

    // two constants
    {
        struct TestPld2_r_two_consts pld = { .val = 123 };

        ret = TestPld2_r_two_consts_build(&pld, malloc, &packed, &len);
        check_eq(ret, PLD_OK);

        uint8_t expected[] = {12, 123, 15};
        check_array(packed, expected);

        ret = pld_parse(modules, (struct pld_address) {8, 3, 30, 30}, packed, len, malloc, &vpld, &kind);
        check_eq(ret, PLD_OK);
        check_eq(kind, TESTPLD2_R_TWO_CONSTS_ID);

        struct TestPld2_r_two_consts *pld2 = vpld;
        check_eq(pld2->val, pld.val);
        free(vpld);
        free(packed);
    }

    {
        struct TestPld2_r_two_consts2 pld = { .val = 123 };

        ret = TestPld2_r_two_consts2_build(&pld, malloc, &packed, &len);
        check_eq(ret, PLD_OK);

        uint8_t expected[] = {13, 123, 15};
        check_array(packed, expected);

        ret = pld_parse(modules, (struct pld_address) {8, 3, 30, 30}, packed, len, malloc, &vpld, &kind);
        check_eq(ret, PLD_OK);
        check_eq(kind, TESTPLD2_R_TWO_CONSTS2_ID);

        struct TestPld2_r_two_consts2 *pld2 = vpld;
        check_eq(pld2->val, pld.val);
        free(vpld);

        packed[0] = 12;
        packed[2] = 16;
        ret = pld_recognize(modules, (struct pld_address) {8, 3, 30, 30}, packed, len, &kind);
        check_eq(ret, PLD_OK);
        check_eq(kind, TESTPLD2_R_TWO_CONSTS3_ID);

        packed[0] = 13;
        packed[2] = 16;
        ret = pld_recognize(modules, (struct pld_address) {8, 3, 30, 30}, packed, len, &kind);
        check_eq(ret, PLD_OK);
        check_eq(kind, TESTPLD2_R_TWO_CONSTS4_ID);

        free(packed);
    }

    return true;
}

bool test_recog_const_bf_single(void) {
    uint8_t *packed;
    size_t len;
    pld_error_t ret;
    void *vpld = NULL;
    uint32_t kind;

    {
        struct TestPld2_r_bfconst_a1 pld = { .x = 22, .y = 19 };

        ret = TestPld2_r_bfconst_a1_build(&pld, malloc, &packed, &len);
        check_eq(ret, PLD_OK);

        uint8_t expected[] = {0xB6, 0x73};
        check_array(packed, expected);

        ret = pld_parse(modules, (struct pld_address) {8, 4, 30, 30}, packed, len, malloc, &vpld, &kind);
        check_eq(ret, PLD_OK);
        check_eq(kind, TESTPLD2_R_BFCONST_A1_ID);

        struct TestPld2_r_bfconst_a1 *pld2 = vpld;
        check_eq(pld2->x, pld.x);
        check_eq(pld2->y, pld.y);

        free(vpld);
        free(packed);
    }

    {
        struct TestPld2_r_bfconst_a2 pld = { .x = 22, .y = 19 };

        ret = TestPld2_r_bfconst_a2_build(&pld, malloc, &packed, &len);
        check_eq(ret, PLD_OK);

        uint8_t expected[] = {0xB6, 0x53};
        check_array(packed, expected);

        ret = pld_parse(modules, (struct pld_address) {8, 4, 30, 30}, packed, len, malloc, &vpld, &kind);
        check_eq(ret, PLD_OK);
        check_eq(kind, TESTPLD2_R_BFCONST_A2_ID);

        struct TestPld2_r_bfconst_a1 *pld2 = vpld;
        check_eq(pld2->x, pld.x);
        check_eq(pld2->y, pld.y);

        free(vpld);
        free(packed);
    }

    return true;
}

bool test_recog_const_bf_multiple(void) {
    uint8_t *packed;
    size_t len;
    pld_error_t ret;
    void *vpld = NULL;
    uint32_t kind;

    {
        struct TestPld2_r_bfconst_b1 pld = { .x = 0xCE, .y = 9 };

        ret = TestPld2_r_bfconst_b1_build(&pld, malloc, &packed, &len);
        check_eq(ret, PLD_OK);

        uint8_t expected[] = {0xCE, 0x93};
        check_array(packed, expected);

        ret = pld_parse(modules, (struct pld_address) {8, 5, 30, 30}, packed, len, malloc, &vpld, &kind);
        check_eq(ret, PLD_OK);
        check_eq(kind, TESTPLD2_R_BFCONST_B1_ID);

        struct TestPld2_r_bfconst_a2 *pld2 = vpld;
        check_eq(pld2->x, pld.x);
        check_eq(pld2->y, pld.y);

        free(vpld);

        // test that padding is ignored
        packed[1] = 0xF3;
        ret = pld_recognize(modules, (struct pld_address) {8, 5, 30, 30}, packed, len, &kind);
        check_eq(ret, PLD_OK);
        check_eq(kind, TESTPLD2_R_BFCONST_B1_ID);

        free(packed);
    }

    {
        struct TestPld2_r_bfconst_b2 pld = { .x = 0xCE, .y = 9 };

        ret = TestPld2_r_bfconst_b2_build(&pld, malloc, &packed, &len);
        check_eq(ret, PLD_OK);

        uint8_t expected[] = {0xCE, 0x92};
        check_array(packed, expected);

        ret = pld_parse(modules, (struct pld_address) {8, 5, 30, 30}, packed, len, malloc, &vpld, &kind);
        check_eq(ret, PLD_OK);
        check_eq(kind, TESTPLD2_R_BFCONST_B2_ID);

        struct TestPld2_r_bfconst_a2 *pld2 = vpld;
        check_eq(pld2->x, pld.x);
        check_eq(pld2->y, pld.y);

        free(vpld);

        // test that padding is ignored
        packed[1] = 0xF2;
        ret = pld_recognize(modules, (struct pld_address) {8, 5, 30, 30}, packed, len, &kind);
        check_eq(ret, PLD_OK);
        check_eq(kind, TESTPLD2_R_BFCONST_B2_ID);

        free(packed);
    }

    return true;
}


bool test_recog_const_padded(void)
{
    pld_error_t ret;
    uint32_t kind;

    /* constant in struct */
    uint8_t pck[] = {0, 0xFF, 0, 0, 55, 0};
    ret = pld_recognize(modules, (struct pld_address) {8, 6, 30, 30}, pck, 6, &kind);
    check_eq(ret, PLD_OK);
    check_eq(kind, TESTPLD2_R_CONSTPAD1_ID);

    // padding is changed, it still recognizes
    pck[0] = 250;
    pck[2] = 99;
    pck[3] = 199;
    pck[5] = 123;
    ret = pld_recognize(modules, (struct pld_address) {8, 6, 30, 30}, pck, 6, &kind);
    check_eq(ret, PLD_OK);
    check_eq(kind, TESTPLD2_R_CONSTPAD1_ID);

    // the constant changed, it's no longer recognizable
    pck[1] = 0x8F;
    ret = pld_recognize(modules, (struct pld_address) {8, 6, 30, 30}, pck, 6, &kind);
    check_eq(ret, PLD_ERR_UNKNOWN_PAYLOAD);

    return true;
}


bool test_recog_const_string(void)
{
    pld_error_t ret;
    void *vpld = NULL;
    uint32_t kind;

    /* constant in struct */
    uint8_t pck[] = {'A', 'H', 'O', 'J', 78};
    ret = pld_recognize(modules, (struct pld_address) {8, 12, 30, 30}, pck, 5, &kind);
    check_eq(ret, PLD_OK);
    check_eq(kind, TESTPLD2_R_CALLSIGN_ID);

    // here the constant is zero
    pck[1] = 'X'; // the rest is a variable length payload
    ret = pld_parse(modules, (struct pld_address) {8, 12, 30, 30}, pck, 5, malloc, &vpld, &kind);
    check_eq(ret, PLD_OK);
    check_eq(kind, TESTPLD2_R_CALLSIGN2_ID);
    struct TestPld2_r_callsign2 *pld2 = vpld;

    // check it's parsed right
    check_eq(pld2->vals_len, 3);
    check_eq(pld2->vals[0], 'O');
    check_eq(pld2->vals[1], 'J');
    check_eq(pld2->vals[2], 78);

    free(vpld);

    return true;
}
