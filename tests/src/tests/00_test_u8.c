#include "../test.h"

bool test_u8(void) {
    uint8_t *packed = NULL;
    size_t len = 0;
    pld_error_t ret = 0;

    /* Source data */
    struct TestPld_test_u8 pld = {
        .u8b = 0xF0,
        .u8l = 0x1F,
    };

    /* Pack */
    ret = TestPld_test_u8_build(&pld, malloc, &packed, &len);
    check(ret == PLD_OK);
    check_eq_ul(len, 2ul);

    /* Check packed format */
    const uint8_t expected[] = { 0xF0, 0x1F };
    check_array(packed, expected);

    /* Unpack */
    struct TestPld_test_u8 *pld2 = NULL;
    ret = TestPld_test_u8_parse(packed, len, malloc, &pld2);
    check(ret == PLD_OK);
    check(pld2 != NULL);

    /* Compare with original */
    check_eq(pld2->u8b, pld.u8b);
    check_eq(pld2->u8l, pld.u8l);

    /* Clean up */
    free(pld2);
    free(packed);
    return true;
}

bool test_u8_generic_build(void) {
    uint8_t *packed = NULL;
    size_t len = 0;
    pld_error_t ret = 0;

    const pld_module_t *modules[] = PLD_MODULES(&Payloads_TestPld);

    /* Source data */
    struct TestPld_test_u8 pld = {
        .pld_id = TESTPLD_TEST_U8_ID,
        .u8b = 0xF0,
        .u8l = 0x1F,
    };

    /* Pack */
    ret = pld_build(modules, &pld, malloc, &packed, &len);
    check(ret == PLD_OK);
    check_eq_ul(len, 2ul);

    /* Check packed format */
    const uint8_t expected[] = { 0xF0, 0x1F };
    check_array(packed, expected);

    /* Unpack */
    struct TestPld_test_u8 *pld2 = NULL;
    ret = pld_parse_as(modules, TESTPLD_TEST_U8_ID, packed, len, malloc, (void**) &pld2);
    check(ret == PLD_OK);
    check(pld2 != NULL);
    check_eq(pld2->pld_id, TESTPLD_TEST_U8_ID);

    /* Compare with original */
    check_eq(pld2->u8b, pld.u8b);
    check_eq(pld2->u8l, pld.u8l);

    /* Clean up */
    free(pld2);
    free(packed);
    return true;
}


bool test_u8_static(void) {
    pld_error_t ret = 0;

    /* Source data */
    struct TestPld_test_u8 pld = {
        .u8b = 0xF0,
        .u8l = 0x1F,
    };

    uint8_t packed[2];

    /* Pack */
    ret = TestPld_test_u8_build_s(&pld, packed, 2);
    check(ret == PLD_OK);

    /* Check packed format */
    const uint8_t expected[] = { 0xF0, 0x1F };
    check_array(packed, expected);

    /* Unpack */
    struct TestPld_test_u8 pld2;
    ret = TestPld_test_u8_parse_s(packed, 2, &pld2);
    check(ret == PLD_OK);

    /* Compare with original */
    check_eq(pld2.u8b, pld.u8b);
    check_eq(pld2.u8l, pld.u8l);
    return true;
}
