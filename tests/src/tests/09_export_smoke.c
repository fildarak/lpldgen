#include "../test.h"
#include "../generated/payloads.h"
#include "../generated/cJSON.h"
#include <string.h>

#define smoketest(stru, size, parse_fn, export_fn, annotated, basic, dataonly) do { \
    uint8_t buf[size] = {}; \
    struct stru dest = {}; \
    bool matches; \
    pld_error_t rv = parse_fn(buf, size, &dest); \
    check_eq(rv, PLD_OK); \
    \
    cJSON* json = NULL; \
    rv = export_fn(&dest, EXPORT_JSON_ANNOTATED, &json); \
    check_eq(rv, PLD_OK); \
    char *str = cJSON_PrintUnformatted(json); \
    check(str != NULL); \
    matches = (0 == strcmp(str, annotated)); \
    if (!matches) { \
        printf("Annotated: %s\n", str); \
        printf("Expecting: %s\n", annotated); \
        check(matches); \
    } \
    \
    json = NULL; \
    rv = export_fn(&dest, EXPORT_JSON_BASIC, &json); \
    check_eq(rv, PLD_OK); \
    str = cJSON_PrintUnformatted(json); \
    check(str != NULL); \
    matches = (0 == strcmp(str, basic)); \
    if (!matches) { \
        printf("Basic: %s\n", str); \
        printf("Expecting: %s\n", basic); \
        check(matches); \
    } \
    \
    json = NULL; \
    rv = export_fn(&dest, EXPORT_JSON_DATAONLY, &json); \
    check_eq(rv, PLD_OK); \
    str = cJSON_PrintUnformatted(json); \
    check(str != NULL); \
    matches = (0 == strcmp(str, dataonly)); \
    if (!matches) { \
        printf("Dataonly: %s\n", str); \
        printf("Expecting: %s\n", dataonly); \
        check(matches); \
    } \
} while(0)


bool test_export_smoke(void)
{
    // test some exports

    smoketest(
            Exp_GET_HK_1_Req,
            EXP_GET_HK_1_REQ_BIN_SIZE,
            Exp_GET_HK_1_Req_parse_s,
            Exp_GET_HK_1_Req_to_json,
            "{\"name\":\"Exp_GET_HK_1_Req\",\"data\":{}}",
            "{\"name\":\"Exp_GET_HK_1_Req\",\"data\":{}}",
            "{}"
    );

    smoketest(
            Exp_GET_HK_1_Resp,
            EXP_GET_HK_1_RESP_BIN_SIZE,
            Exp_GET_HK_1_Resp_parse_s,
            Exp_GET_HK_1_Resp_to_json,
            "{\"name\":\"Exp_GET_HK_1_Resp\",\"descr\":\"first line\\nwith newline\\nsecond line\",\"data\":{\"pv\":{\"type\":\"u16[]\",\"value\":[0,0,0]},\"pc\":{\"descr\":\"°C\",\"type\":\"u16\",\"value\":0},\"bv\":{\"descr\":\"first line\\nwith newline\\nsecond line\",\"type\":\"u16\",\"value\":0},\"sc\":{\"type\":\"u16\",\"value\":0},\"temp\":{\"type\":\"u16[]\",\"value\":[0,0,0,0]},\"bat_temp\":{\"descr\":\"triple slash descr\",\"type\":\"u16[]\",\"value\":[0,0,0,0]},\"latchup\":{\"type\":\"u16[]\",\"value\":[0,0,0,0,0,0]},\"reset\":{\"descr\":\"aaa\\nbbb\",\"type\":\"u8\",\"value\":0},\"bootcount\":{\"type\":\"u16\",\"value\":0},\"sw_errors\":{\"type\":\"u16\",\"value\":\"0x0000\"},\"ppt_mode\":{\"type\":\"u8\",\"value\":0},\"qh\":{\"type\":\"bool\",\"value\":false},\"qs\":{\"type\":\"bool\",\"value\":false},\"v33_3\":{\"type\":\"bool\",\"value\":false},\"v33_2\":{\"type\":\"bool\",\"value\":false},\"v33_1\":{\"type\":\"bool\",\"value\":false},\"v5_3\":{\"type\":\"bool\",\"value\":false},\"v5_2\":{\"type\":\"bool\",\"value\":false},\"v5_1\":{\"type\":\"bool\",\"value\":false}}}",
            "{\"name\":\"Exp_GET_HK_1_Resp\",\"data\":{\"pv\":[0,0,0],\"pc\":0,\"bv\":0,\"sc\":0,\"temp\":[0,0,0,0],\"bat_temp\":[0,0,0,0],\"latchup\":[0,0,0,0,0,0],\"reset\":0,\"bootcount\":0,\"sw_errors\":\"0x0000\",\"ppt_mode\":0,\"qh\":false,\"qs\":false,\"v33_3\":false,\"v33_2\":false,\"v33_1\":false,\"v5_3\":false,\"v5_2\":false,\"v5_1\":false}}",
            "{\"pv\":[0,0,0],\"pc\":0,\"bv\":0,\"sc\":0,\"temp\":[0,0,0,0],\"bat_temp\":[0,0,0,0],\"latchup\":[0,0,0,0,0,0],\"reset\":0,\"bootcount\":0,\"sw_errors\":\"0x0000\",\"ppt_mode\":0,\"qh\":false,\"qs\":false,\"v33_3\":false,\"v33_2\":false,\"v33_1\":false,\"v5_3\":false,\"v5_2\":false,\"v5_1\":false}"
    );

    smoketest(
            Exp_GET_HK_2_Basic_Req,
            EXP_GET_HK_2_BASIC_REQ_BIN_SIZE,
            Exp_GET_HK_2_Basic_Req_parse_s,
            Exp_GET_HK_2_Basic_Req_to_json,
            "{\"name\":\"Exp_GET_HK_2_Basic_Req\",\"data\":{}}",
            "{\"name\":\"Exp_GET_HK_2_Basic_Req\",\"data\":{}}",
            "{}"
    );

    smoketest(
            Exp_GET_HK_2_Basic_Resp,
            EXP_GET_HK_2_BASIC_RESP_BIN_SIZE,
            Exp_GET_HK_2_Basic_Resp_parse_s,
            Exp_GET_HK_2_Basic_Resp_to_json,
            "{\"name\":\"Exp_GET_HK_2_Basic_Resp\",\"data\":{\"counter_boot\":{\"type\":\"u32\",\"value\":0},\"temp\":{\"type\":\"i16[]\",\"value\":[0,0,0,0,0,0]},\"bootcause\":{\"type\":\"u8\",\"value\":0},\"battmode\":{\"type\":\"u8\",\"value\":0},\"pptmode\":{\"type\":\"u8\",\"value\":0}}}",
            "{\"name\":\"Exp_GET_HK_2_Basic_Resp\",\"data\":{\"counter_boot\":0,\"temp\":[0,0,0,0,0,0],\"bootcause\":0,\"battmode\":0,\"pptmode\":0}}",
            "{\"counter_boot\":0,\"temp\":[0,0,0,0,0,0],\"bootcause\":0,\"battmode\":0,\"pptmode\":0}"
    );

    return true;
}
