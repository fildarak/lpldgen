#include "../test.h"

bool test_bitfield_nested(void) {
    uint8_t *packed;
    size_t len;
    pld_error_t ret;

    /* Source data */
    struct TestPld_bitf_nested pld = {
        .outer = {
            .inner = {
                .three = 0b110,
                .threebits = {
                    .b1 = 1,
                    .b2 = 0,
                    .b3 = 1,
                },
                .onebit = 0,
            },
            .onebit = 1,
        },
    };

    /* Pack */
    ret = TestPld_bitf_nested_build(&pld, malloc, &packed, &len);
    check(ret == PLD_OK);
    check_eq_ul(len, 1ul);

    /* Check packed format */
    const uint8_t expected[] = {
        0b11010101
    };
    check_array(packed, expected);

    /* Unpack */
    struct TestPld_bitf_nested *pld2 = NULL;
    ret = TestPld_bitf_nested_parse(packed, len, malloc, &pld2);
    check(ret == PLD_OK);
    check(pld2 != NULL);

    /* Compare with original */
    check_eq(pld2->outer.onebit, pld.outer.onebit);
    check_eq(pld2->outer.inner.onebit, pld.outer.inner.onebit);
    check_eq(pld2->outer.inner.three, pld.outer.inner.three);
    check_eq(pld2->outer.inner.threebits.b1, pld.outer.inner.threebits.b1);
    check_eq(pld2->outer.inner.threebits.b2, pld.outer.inner.threebits.b2);
    check_eq(pld2->outer.inner.threebits.b3, pld.outer.inner.threebits.b3);

    /* Clean up */
    free(pld2);
    free(packed);
    return true;
}
