#include "../test.h"

bool test_i24(void) {
    uint8_t *packed;
    size_t len;
    pld_error_t ret;

    /* Source data */
    struct TestPld_test_i24 pld = {
        .i24b = -8388608, // 0x80_0000
        .i24l = -2024900, // 0xE1_1A3C
        .i24b2 = 8388607, // 0x7F_FFFF
        .i24l2 = 7369538, // 0x70_7342
    };

    /* Pack */
    ret = TestPld_test_i24_build(&pld, malloc, &packed, &len);
    check(ret == PLD_OK);
    check_eq_ul(len, 12ul);

    /* Check packed format */
    const uint8_t expected[] = {
        // big
        0x80, 0x00, 0x00,
        // little
        0x3C, 0x1A, 0xE1,
        // big
        0x7F, 0xFF, 0xFF,
        // little
        0x42, 0x73, 0x70,
    };
    check_array(packed, expected);

    /* Unpack */
    struct TestPld_test_i24 *pld2 = NULL;
    ret = TestPld_test_i24_parse(packed, len, malloc, &pld2);
    check(ret == PLD_OK);
    check(pld2 != NULL);

    /* Compare with original */
    check_eq(pld2->i24b, pld.i24b);
    check_eq(pld2->i24l, pld.i24l);
    check_eq(pld2->i24b2, pld.i24b2);
    check_eq(pld2->i24l2, pld.i24l2);

    /* Clean up */
    free(pld2);
    free(packed);
    return true;
}
