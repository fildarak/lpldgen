#include "../test.h"

bool test_bitfield_arrays(void) {
    uint8_t *packed;
    size_t len;
    pld_error_t ret;

    /* Source data */
    struct TestPld_bitf_bitarrays pld = {
        .repeated = {
            { .inside = 0x9 },
            { .inside = 0xC },
        },
        .repeated_outer = {
            { .inside = 0xA },
            { .inside = 0xB },
        },
    };

    /* Pack */
    ret = TestPld_bitf_bitarrays_build(&pld, malloc, &packed, &len);
    check(ret == PLD_OK);
    check_eq_ul(len, 2ul);

    /* Check packed format */
    const uint8_t expected[] = {
        0x9C,
        0xBA, // it's right aligned
    };
    check_array(packed, expected);

    /* Unpack */
    struct TestPld_bitf_bitarrays *pld2 = NULL;
    ret = TestPld_bitf_bitarrays_parse(packed, len, malloc, &pld2);
    check(ret == PLD_OK);
    check(pld2 != NULL);

    /* Compare with original */
    check_eq(pld2->repeated[0].inside, pld.repeated[0].inside);
    check_eq(pld2->repeated[1].inside, pld.repeated[1].inside);

    check_eq(pld2->repeated_outer[0].inside, pld.repeated_outer[0].inside);
    check_eq(pld2->repeated_outer[1].inside, pld.repeated_outer[1].inside);

    /* Clean up */
    free(pld2);
    free(packed);
    return true;
}
