#include "../test.h"

static bool check_array_pass() {
    {
        uint8_t a[] = {1, 2, 3, 4, 5, 6};
        uint8_t b[] = {1, 2, 3, 4, 5, 6};
        check_array(a, b);
    }

    uint8_t a[] = {};
    uint8_t b[] = {};
    check_array(a, b);

    return true;
}

static bool check_array_fail1() {
    uint8_t a[] = {1,2,3,4,5,6};
    uint8_t b[] = {0,2,3,4,5,6};
    check_array(a, b);

    printf("check_array_fail1 passed, should fail!\n");
    return true;
}

static bool check_array_fail2() {
    uint8_t a[] = {1,2,3,4,5,0};
    uint8_t b[] = {1,2,3,4,5,6};
    check_array(a, b);

    printf("check_array_fail2 passed, should fail!\n");
    return true;
}

static bool check_pass() {
    check(true);
    return true;
}

static bool check_fail() {
    check(false);
    printf("check_fail passed, should fail!\n");
    return true;
}

static bool check_eq_fail() {
    check_eq(1, 2);
    printf("check_eq_fail passed, should fail!\n");
    return true;
}

static bool check_eq_ul_fail() {
    check_eq_ul(1ul, 2ul);
    printf("check_eq_ul_fail passed, should fail!\n");
    return true;
}

static bool check_eq_l_fail() {
    check_eq_l(1l, 2l);
    printf("check_eq_l_fail passed, should fail!\n");
    return true;
}

static bool check_eq_f_fail1() {
    check_eq_f(10.0f, 11.0f);
    printf("check_eq_f_fail1 passed, should fail!\n");
    return true;
}

static bool check_eq_f_fail2() {
    #include <math.h>
    check_eq_f(10.0f, NAN);
    printf("check_eq_f_fail2 passed, should fail!\n");
    return true;
}

bool test_asserts(void) {
    if (!check_array_pass()) return false;
    if (!check_pass()) return false;
    check_eq(1, 1);
    check_eq_ul(1ul, 1ul);
    check_eq_l(1l, 1l);
    check_eq_f(1.0f, 1.0f);

    printf("-- the following asserts should fail:\n");
    if (check_array_fail1()) return false;
    if (check_array_fail2()) return false;
    if (check_fail()) return false;
    if (check_eq_fail()) return false;
    if (check_eq_ul_fail()) return false;
    if (check_eq_l_fail()) return false;
    if (check_eq_f_fail1()) return false;
    if (check_eq_f_fail2()) return false;
    printf("-- end of assert tests\n\n");

    return true;
}

