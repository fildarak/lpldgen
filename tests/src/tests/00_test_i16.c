#include "../test.h"

bool test_i16(void) {
    uint8_t *packed;
    size_t len;
    pld_error_t ret;

    /* Source data */
    struct TestPld_test_i16 pld = {
        .i16b = -32768, // 0x8000
        .i16l = -12345, // 0xCFC7
        .i16b2 = 32767, // 0x7FFF
        .i16l2 = 12345, // 0x3039
    };

    /* Pack */
    ret = TestPld_test_i16_build(&pld, malloc, &packed, &len);
    check(ret == PLD_OK);
    check_eq_ul(len, 8ul);

    /* Check packed format */
    const uint8_t expected[] = {
        // big
        0x80, 0x00,
        // little
        0xC7, 0xCF,
        // big
        0x7F, 0xFF,
        // little
        0x39, 0x30,
    };
    check_array(packed, expected);

    /* Unpack */
    struct TestPld_test_i16 *pld2 = NULL;
    ret = TestPld_test_i16_parse(packed, len, malloc, &pld2);
    check(ret == PLD_OK);
    check(pld2 != NULL);

    /* Compare with original */
    check_eq(pld2->i16b, pld.i16b);
    check_eq(pld2->i16l, pld.i16l);
    check_eq(pld2->i16b2, pld.i16b2);
    check_eq(pld2->i16l2, pld.i16l2);

    /* Clean up */
    free(pld2);
    free(packed);
    return true;
}
