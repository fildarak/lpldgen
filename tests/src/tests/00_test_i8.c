#include "../test.h"

bool test_i8(void) {
    uint8_t *packed;
    size_t len;
    pld_error_t ret;

    /* Source data */
    struct TestPld_test_i8 pld = {
        .i8b = -128,
        .i8l = -99,
        .i8b2 = 127,
        .i8l2 = 99,
    };

    /* Pack */
    ret = TestPld_test_i8_build(&pld, malloc, &packed, &len);
    check(ret == PLD_OK);
    check_eq_ul(len, 4ul);

    /* Check packed format */
    const uint8_t expected[] = { 0x80, 0x9D, 0x7F, 0x63 };
    check_array(packed, expected);

    /* Unpack */
    struct TestPld_test_i8 *pld2 = NULL;
    ret = TestPld_test_i8_parse(packed, len, malloc, &pld2);
    check(ret == PLD_OK);
    check(pld2 != NULL);

    /* Compare with original */
    check_eq(pld2->i8b, pld.i8b);
    check_eq(pld2->i8l, pld.i8l);
    check_eq(pld2->i8b2, pld.i8b2);
    check_eq(pld2->i8l2, pld.i8l2);

    /* Clean up */
    free(pld2);
    free(packed);
    return true;
}
