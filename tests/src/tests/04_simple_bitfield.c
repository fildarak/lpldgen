#include "../test.h"

bool test_simple_bitfield(void) {
    uint8_t *packed;
    size_t len;
    pld_error_t ret;

    /* Source data */
    struct TestPld_bitf_simple pld = {
        // 1
        .bits = {
            .upper = 0x7,
            .lower = 0x3,
        },
        // 2
        .upper = 0xA,
        .middle = 0x58,
        // 6
        .elittle = {
            .upper = 0x3,
            .lower = 0x2F7,
            .little16 = 0xabcd,
            .big16 = 0x1234,
        },
        // 2
        .ebig = {
            .upper = 0x3,
            .lower = 0x2F7,
        },
        // 2
        .ahigh = {
            .upper = 0x6,
            .lower = 0xAB,
            .b = 1,
        },
        // 2
        .aleft = {
            .upper = 0x6,
            .lower = 0xAB,
            .b = 1,
        },
        // 2
        .alow = {
            .upper = 0x6,
            .lower = 0xAB,
            .b = 1,
        },
        // 2
        .aright = {
            .upper = 0x6,
            .lower = 0xAB,
            .b = 1,
        },
    };

    /* Pack */
    ret = TestPld_bitf_simple_build(&pld, malloc, &packed, &len);
    check(ret == PLD_OK);
    check_eq_ul(len, 19ul);

    /* Check packed format */
    const uint8_t expected[] = {
        // bits
        0x73,
        // flattened
        0xA5, 0x80,
        // elittle
        0x3F, 0x72, 0xcd, 0xab, 0x12, 0x34,
        // ebig
        0x32, 0xF7,
        //ahigh
        0x6A, 0xB1,
        // aleft
        0x6A, 0xB1,
        // alow
        0xB6, 0x8A,
        // aright
        0xB6, 0x8A,
    };
    check_array(packed, expected);

    /* Unpack */
    struct TestPld_bitf_simple *pld2 = NULL;
    ret = TestPld_bitf_simple_parse(packed, len, malloc, &pld2);
    check(ret == PLD_OK);
    check(pld2 != NULL);

    /* Compare with original */
    check_eq(pld2->bits.upper, pld.bits.upper);
    check_eq(pld2->bits.lower, pld.bits.lower);
    check_eq(pld2->upper, pld.upper);
    check_eq(pld2->middle, pld.middle);

    check_eq(pld2->elittle.upper, pld.elittle.upper);
    check_eq(pld2->elittle.lower, pld.elittle.lower);
    check_eq(pld2->elittle.little16, pld.elittle.little16);
    check_eq(pld2->elittle.big16, pld.elittle.big16);

    check_eq(pld2->ebig.upper, pld.ebig.upper);
    check_eq(pld2->ebig.lower, pld.ebig.lower);

    check_eq(pld2->ahigh.upper, pld.ahigh.upper);
    check_eq(pld2->ahigh.lower, pld.ahigh.lower);
    check_eq(pld2->ahigh.b, pld.ahigh.b);
    check_eq(pld2->aleft.upper, pld.aleft.upper);
    check_eq(pld2->aleft.lower, pld.aleft.lower);
    check_eq(pld2->aleft.b, pld.aleft.b);

    check_eq(pld2->alow.upper, pld.alow.upper);
    check_eq(pld2->alow.lower, pld.alow.lower);
    check_eq(pld2->alow.b, pld.alow.b);
    check_eq(pld2->aright.upper, pld.aright.upper);
    check_eq(pld2->aright.lower, pld.aright.lower);
    check_eq(pld2->aright.b, pld.aright.b);

    /* Clean up */
    free(pld2);
    free(packed);
    return true;
}
