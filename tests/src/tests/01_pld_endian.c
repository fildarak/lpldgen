#include "../test.h"

bool test_pld_endian_little(void) {
    uint8_t *packed;
    size_t len;
    pld_error_t ret;

    /* Source data */
    struct TestPld_pld_endian_little pld = {
        .u16 = 0xABCD,
    };

    /* Pack */
    ret = TestPld_pld_endian_little_build(&pld, malloc, &packed, &len);
    check(ret == PLD_OK);
    check_eq_ul(len, 2ul);

    /* Check packed format */
    const uint8_t expected[] = { 0xCD, 0xAB };
    check_array(packed, expected);

    /* Unpack */
    struct TestPld_pld_endian_little *pld2 = NULL;
    ret = TestPld_pld_endian_little_parse(packed, len, malloc, &pld2);
    check(ret == PLD_OK);
    check(pld2 != NULL);

    /* Compare with original */
    check_eq(pld2->u16, pld.u16);

    /* Clean up */
    free(pld2);
    free(packed);
    return true;
}

bool test_pld_endian_big(void) {
    uint8_t *packed;
    size_t len;
    pld_error_t ret;

    /* Source data */
    struct TestPld_pld_endian_big pld = {
        .u16 = 0xABCD,
    };

    /* Pack */
    ret = TestPld_pld_endian_big_build(&pld, malloc, &packed, &len);
    check(ret == PLD_OK);
    check_eq_ul(len, 2ul);

    /* Check packed format */
    const uint8_t expected[] = { 0xAB, 0xCD };
    check_array(packed, expected);

    /* Unpack */
    struct TestPld_pld_endian_big *pld2 = NULL;
    ret = TestPld_pld_endian_big_parse(packed, len, malloc, &pld2);
    check(ret == PLD_OK);
    check(pld2 != NULL);

    /* Compare with original */
    check_eq(pld2->u16, pld.u16);

    /* Clean up */
    free(pld2);
    free(packed);
    return true;
}
