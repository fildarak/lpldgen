#include "../test.h"

bool test_simple_struct(void) {
    uint8_t *packed;
    size_t len;
    pld_error_t ret;

    /* Source data */
    struct TestPld_simple_struct pld = {
        .x = {
            .a = 0xAB, // 8
            .b = 0xCDEF, // 16
            .c = 0x012345, // 24
            .d = 0x6789ABCD, // 32
            .e = 0xEF0123456789ABCD, // 64
        },
    };

    /* Pack */
    ret = TestPld_simple_struct_build(&pld, malloc, &packed, &len);
    check(ret == PLD_OK);
    check_eq_ul(len, 18ul);

    /* Check packed format */
    const uint8_t expected[] = {
        0xab,
        0xcd, 0xef,
        0x01, 0x23, 0x45,
        0x67, 0x89, 0xab, 0xcd,
        0xef, 0x01, 0x23, 0x45, 0x67, 0x89, 0xab, 0xcd
    };
    check_array(packed, expected);

    /* Unpack */
    struct TestPld_simple_struct *pld2 = NULL;
    ret = TestPld_simple_struct_parse(packed, len, malloc, &pld2);
    check(ret == PLD_OK);
    check(pld2 != NULL);

    /* Compare with original */
    check_eq(pld2->x.a, pld.x.a);
    check_eq(pld2->x.b, pld.x.b);
    check_eq(pld2->x.c, pld.x.c);
    check_eq(pld2->x.d, pld.x.d);
    check_eq_ul(pld2->x.e, pld.x.e);

    /* Clean up */
    free(pld2);
    free(packed);
    return true;
}
