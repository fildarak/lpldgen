#include "../test.h"

bool test_u64(void) {
    uint8_t *packed;
    size_t len;
    pld_error_t ret;

    /* Source data */
    struct TestPld_test_u64 pld = {
        .u64b = 0xABCDEF0123456789ul,
        .u64l = 0xABCDEF0123456789ul,
    };

    /* Pack */
    ret = TestPld_test_u64_build(&pld, malloc, &packed, &len);
    check(ret == PLD_OK);
    check_eq_ul(len, 16ul);

    /* Check packed format */
    const uint8_t expected[] = {
        // big
        0xAB, 0xCD, 0xEF, 0x01, 0x23, 0x45, 0x67, 0x89,
        // little
        0x89, 0x67, 0x45, 0x23, 0x01, 0xEF, 0xCD, 0xAB
    };
    check_array(packed, expected);

    /* Unpack */
    struct TestPld_test_u64 *pld2 = NULL;
    ret = TestPld_test_u64_parse(packed, len, malloc, &pld2);
    check(ret == PLD_OK);
    check(pld2 != NULL);

    /* Compare with original */
    check_eq_ul(pld2->u64b, pld.u64b);
    check_eq_ul(pld2->u64l, pld.u64l);

    /* Clean up */
    free(pld2);
    free(packed);
    return true;
}
