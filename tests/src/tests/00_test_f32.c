#include "../test.h"

bool test_f32(void) {
    uint8_t *packed;
    size_t len;
    pld_error_t ret;

    /* Source data */
    struct TestPld_test_f32 pld = {
        .f32b = 0.1f, // 0x3dcc_cccd
        .f32l = 0.1f,
    };

    /* Pack */
    ret = TestPld_test_f32_build(&pld, malloc, &packed, &len);
    check(ret == PLD_OK);
    check_eq_ul(len, 8ul);

    /* Check packed format */
    const uint8_t expected[] = {
        // big
        0x3d, 0xcc, 0xcc, 0xcd,
        // little
        0xcd, 0xcc, 0xcc, 0x3d,
    };
    check_array(packed, expected);

    /* Unpack */
    struct TestPld_test_f32 *pld2 = NULL;
    ret = TestPld_test_f32_parse(packed, len, malloc, &pld2);
    check(ret == PLD_OK);
    check(pld2 != NULL);

    /* Compare with original */
    check_eq_f(pld2->f32b, pld.f32b);
    check_eq_f(pld2->f32l, pld.f32l);

    /* Clean up */
    free(pld2);
    free(packed);
    return true;
}
