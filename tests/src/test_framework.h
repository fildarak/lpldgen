/**
 * shared defines and utils for tests
 *
 * Created on 2020/05/12.
 */

#ifndef TESTS_TEST_FRAMEWORK_H
#define TESTS_TEST_FRAMEWORK_H

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <math.h>

#define XSTR(s) STR(s)
#define STR(s) #s

typedef bool(*testfn_t)(void);

struct Test {
    const char *name;
    testfn_t func;
};

void run_tests(struct Test *tests, const char *module_name);

// _good must be an array, not a pointer
#define check_array(_var, _good) \
    do { \
        for (int i = 0; i < (int)sizeof(_good); i++) { \
            if (_var[i] != _good[i]) { \
                printf("%s:%d \x1b[31mAssert failed\x1b[m: arrays differ\n\x1b[37;1m  n# exp  act\x1b[m\n", __func__, __LINE__); \
                for (i = 0; i < (int)sizeof(_good); i++) { \
                    if (_var[i] == _good[i]) { \
                        printf("  %2d - 0x%02x -\n", i, _var[i]); \
                    } else { \
                        printf("  %2d \x1b[32m0x%02x \x1b[31m0x%02x\x1b[m\n", i, _good[i], _var[i]); \
                    } \
                } \
                return false; \
            } \
        } \
    } while(0)

#define check(_cond) \
    do { \
        if (!(_cond)) { \
            printf("%s:%d \x1b[31mAssert failed\x1b[m: %s\n", __func__, __LINE__, STR(_cond)); \
            return false; \
        } \
    } while(0)

#define check_eq(_var, _good) \
    do { \
        if ((_var) != (_good)) { \
            printf("%s:%d \x1b[31mAssert failed\x1b[m: %s == %s\n", __func__, __LINE__, STR(_var), STR(_good)); \
            printf("  \x1b[31m%d (0x%02x)\x1b[m != \x1b[32m%d (0x%02x)\x1b[m\n", _var, _var, _good, _good); \
            return false; \
        } \
    } while(0)

#define check_eq_ul(_var, _good) \
    do { \
        if ((_var) != (_good)) { \
            printf("%s:%d \x1b[31mAssert failed\x1b[m: %s == %s\n", __func__, __LINE__, STR(_var), STR(_good)); \
            printf("  \x1b[31m%ld (0x%02lx)\x1b[m != \x1b[32m%ld (0x%02lx)\x1b[m\n", _var, _var, _good, _good); \
            return false; \
        } \
    } while(0)

#define check_eq_l(_var, _good) \
    do { \
        if ((_var) != (_good)) { \
            printf("%s:%d \x1b[31mAssert failed\x1b[m: %s == %s\n", __func__, __LINE__, STR(_var), STR(_good)); \
            printf("  \x1b[31m%ld\x1b[m != \x1b[32m%ld\x1b[m\n", _var, _good); \
            return false; \
        } \
    } while(0)

#define check_eq_f(_var, _good) \
    do { \
        if ((_var) != (_good)) { \
            printf("%s:%d \x1b[31mAssert failed\x1b[m: %s == %s\n", __func__, __LINE__, STR(_var), STR(_good)); \
            printf("  \x1b[31m%f\x1b[m != \x1b[32m%f\x1b[m\n", _var, _good); \
            return false; \
        } \
    } while(0)

#define check_eq_f_safe(_var, _good, thr) \
    do { \
        if (fabs((double)((_var) - (_good))) > thr) { \
            printf("%s:%d \x1b[31mAssert failed\x1b[m: %s == %s\n", __func__, __LINE__, STR(_var), STR(_good)); \
            printf("  \x1b[31m%f\x1b[m != \x1b[32m%f\x1b[m\n", _var, _good); \
            return false; \
        } \
    } while(0)


#endif //TESTS_TEST_FRAMEWORK_H
