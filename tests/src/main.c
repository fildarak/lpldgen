#include <stdio.h>
#include <payloads.h>
#include "test.h"

#define X(name) extern bool name(void);
XTESTS
#undef X

static struct Test tests[] = {
#define X(name) { STR(name), name },
XTESTS
#undef X
    {}
};

#if PLD_HAVE_LOGGING
#define MAX_VERBOSITY PLD_LOG_WARN

#include <stdarg.h>
void __attribute__((format (printf, 2, 3)))
pld_log_write(enum pld_log_level level, const char *format, ...) {
    va_list args;
    va_start(args, format);

    if (level < MAX_VERBOSITY) {
        return;
    }

    const char *levels[] = {
        [PLD_LOG_TRACE] = "\x1b[90m[TRACE]",
        [PLD_LOG_DEBUG] = "\x1b[39m[DEBUG]",
        [PLD_LOG_INFO] = "\x1b[32;1m[INFO]",
        [PLD_LOG_WARN] = "\x1b[33;1m[WARN]",
        [PLD_LOG_ERROR] = "\x1b[31;1m[ERROR]",
    };

    printf("pld: %s ", levels[level]);
    vprintf(format, args);
    printf("\x1b[0m\n");

    va_end(args);
}
#endif

int main() {
    run_tests(tests, "main");
    return 0;
}
