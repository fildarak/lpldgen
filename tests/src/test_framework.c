#include <stdio.h>
#include "test_framework.h"

void run_tests(struct Test *tests, const char *module_name) {
    printf("Running tests module \"%s\"...\n\n", module_name);

    struct Test *t = &tests[0];
    int passed = 0, failed = 0;
    do {
        printf("Running %s...\n", t->name);

        if (t->func()) {
            printf("%s \x1b[32mPASSED\x1b[m\n", t->name);
            passed++;
        } else {
            printf("%s \x1b[31mFAILED\x1b[m\n", t->name);
            failed++;
        }
    } while((++t)->name != NULL);

    printf("\nTests module \"%s\" done. %d passed, %d failed.\n", module_name, passed, failed);
}
