#ifndef LPLDGEN_CONF_PAYLOADS_H
#define LPLDGEN_CONF_PAYLOADS_H

// these are set by compile flags in the test build

// #define PLD_HAVE_EXPORT
// #define PLD_HAVE_ALLOC
// #define PLD_HAVE_LOGGING

#define PLD_BUILD_FUNCS_CHECK_ID 2

#endif //LPLDGEN_CONF_PAYLOADS_H
