+from 2:0
struct simple_struct {
    struct x {
        u8 a
        u16 b
        u24 c
        u32 d
        u64 e
    }
}

// Two adjacent structs with the same member names.
// Test that endian works in a struct.
+from 2:1
struct two_structs {
    struct x {
        u8 a
        u8 b
    }
    struct y {
        +endian little
        u16 a
        i8 b
    }
}

// Structs can be nested
// Multiple levels of nesting, with regular fields at all levels
// Same names at different levels
+from 2:2
struct nested_struct {
    struct x {
        u8 a
        struct y {
            u8 a
            struct y {
                u8 a
                i8 b
            }
            u8 c
        }
        u8 c
    }
    u8 c
}
