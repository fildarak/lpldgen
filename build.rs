use std::process::Command;
use std::str;

fn main() {
    let desc_c = Command::new("git")
        .args(&["describe", "--all", "--long"])
        .output()
        .unwrap();

    let desc = unsafe { str::from_utf8_unchecked(&desc_c.stdout) };

    println!("cargo:rustc-env=GIT_REV={}", desc);
}
