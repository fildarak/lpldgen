project('pld', 'c', version : '0.1')

pld_src = files(
  'out/payloads.c',
)

if get_option('have_export')
  pld_src += files(
    'out/cJSON.c',
    'out/payloads_pretty_print.c',
  )
endif

pld_deps = []
pld_inc = include_directories('out/include')

lpldgen_exe = meson.current_build_dir() / 'rust/release/lpldgen'
fs = import('fs')
if not fs.exists(lpldgen_exe)
  message('Building lpldgen...')
  run_command(
    'cargo', 'build', '--release',
    check : true,
    env : {'CARGO_TARGET_DIR' : meson.current_build_dir() / 'rust'},
  )
endif
if not meson.is_subproject()
  install_data(lpldgen_exe, install_dir : get_option('bindir'))
endif

pld_lib = library(
  'pld',
  pld_src,
  dependencies : pld_deps,
  include_directories : pld_inc,
  install : true
)

pld_dep = declare_dependency(
  include_directories : pld_inc,
  link_with : pld_lib,
  variables : {'lpldgen' : lpldgen_exe },
)
meson.override_dependency('pld', pld_dep)

pkgconfig = import('pkgconfig')
pkgconfig.generate(pld_lib, variables : ['lpldgen=${prefix}/bin/lpldgen'])

pld_config = configuration_data()
pld_config.set10('PLD_HAVE_EXPORT', get_option('have_export'))
pld_config.set10('PLD_HAVE_ALLOC', get_option('have_alloc'))
pld_config.set10('PLD_HAVE_LOGGING', get_option('have_logging'))
pld_config.set('PLD_BUILD_FUNCS_CHECK_ID', get_option('build_funcs_check_id'))

subdir('out/include/pld')

install_subdir('out/include/pld', install_dir : get_option('includedir'), exclude_files : 'meson.build')
install_headers('out/include/cJSON.h')
