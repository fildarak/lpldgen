# libpayload

This project generates a C library to build, recognize, and parse CSP payloads, based on
structure specification files.

## Usage

This application generates C code to link into other projects. The payloads to build and
parse are described in text files using syntax detailed in [SYNTAX.md](SYNTAX.md).

1. Write the payload templates
2. Run this application to build C source and header files
3. Use the built files in an external C project, or build library archive and link that.

The generated header file contains Doxygen comments for each of the structs and functions.

## Command line options

```
USAGE:
    lpldgen [FLAGS] [OPTIONS]

FLAGS:
        --default-config    Print the default config JSON for reference (or to be piped to a file)
        --dump-config       Print the loaded config struct
    -h, --help              Prints help information
    -V, --version           Prints version information
    -v, --verbose           Increase logging verbosity (repeat to increase)

OPTIONS:
    -c, --config <FILE>        Sets a custom config file (JSON5)
        --log <LEVEL>          Set logging verbosity (error,warning,info,debug,trace)
    -o, --dest <DIR>           Sets a destination folder for both source and header files, absolute or relative to
                               working directory
        --output-inc <FILE>    Sets an output header file to be generated for single module
        --output-src <FILE>    Sets an output source file to be generated for single module
        --dest-inc <DIR>       Sets a destination folder for generated header files, absolute or relative to working
                               directory
        --dest-src <DIR>       Sets a destination folder for generated source files, absolute or relative to working
                               directory
```

## Compiling

The Rust Programming Language is needed to compile `lpldgen`. The best way how to get it is:

```bash
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
```

And then:

```bash
git submodule init
git submodule update
cargo build --release

# Link or copy the binary where you want it...
ln -sfr target/release/lpldgen ~/bin/
```

## Using as shared library

Some VZLU tools depend on lpldgen being available as shared library. To do this, run the following:

```
meson build
cd build && ninja install
```
